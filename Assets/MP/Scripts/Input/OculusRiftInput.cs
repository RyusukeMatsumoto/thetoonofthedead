﻿using UnityEngine;
using System.Collections;

namespace TheToonOfTheDead.Input
{
    //==============================================================================
    //------------------------------------------------------------------------------
    // OculusGoの入力.
    public class OculusRiftInput : GameInputBase
    {


        //------------------------------------------------------------------------------
        public OculusRiftInput() { }


        //------------------------------------------------------------------------------
        // キー入力情報の更新.
        public override void UpdateInput(
                ref InputKey _key,
                ref Vector2 _lAxis,
                ref Vector2 _rAxis)
        {
            _key = InputKey.None;
            _lAxis = Vector2.zero;
            _rAxis = Vector2.zero;

            var _activeController = OVRInput.GetActiveController();
            if (_activeController == OVRInput.Controller.LTrackedRemote)
            {
                _key |= OVRInput.GetDown(OVRInput.Button.Back) ? InputKey.BackButtonDown : InputKey.None;
                _key |= OVRInput.Get(OVRInput.Button.Back) ? InputKey.BackButton : InputKey.None;
                _key |= OVRInput.GetUp(OVRInput.Button.Back) ? InputKey.BackButtonUp : InputKey.None;
                _key |= OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger) ? InputKey.LTriggerDown : InputKey.None;
                _key |= OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger) ? InputKey.LTrigger : InputKey.None;
                _key |= OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger) ? InputKey.LTriggerUp : InputKey.None;

                _key |= InputKey.LGrip;
            }
            else
            {
                _key |= OVRInput.GetDown(OVRInput.Button.Back) ? InputKey.BackButtonDown : InputKey.None;
                _key |= OVRInput.Get(OVRInput.Button.Back) ? InputKey.BackButton : InputKey.None;
                _key |= OVRInput.GetUp(OVRInput.Button.Back) ? InputKey.BackButtonUp : InputKey.None;
                _key |= OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger) ? InputKey.RTriggerDown : InputKey.None;
                _key |= OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger) ? InputKey.RTrigger : InputKey.None;
                _key |= OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger) ? InputKey.RTriggerUp : InputKey.None;

                _key |= InputKey.RGrip;
            }
            _rAxis = _lAxis = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad);
        }
    }
}
