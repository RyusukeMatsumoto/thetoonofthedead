﻿using UnityEngine;
using System.Collections;

namespace TheToonOfTheDead.Input
{
    //==============================================================================
    //------------------------------------------------------------------------------
    // 今ゲームの入力周りの処理を行わせる抽象クラス,入力周りの処理を行うものはこのクラスを継承する.
    public abstract class GameInputBase
    {

        [System.Flags]
        public enum InputKey
        {
            None = 0,
            BackButtonDown = 1,       // バックボタン押下.
            BackButton     = 1 << 1,  // バックボタン押し.
            BackButtonUp   = 1 << 2,  // バックボタン離し.
            LTriggerDown   = 1 << 3,  // 左トリガ押下.
            LTrigger       = 1 << 4,  // 左トリガ.
            LTriggerUp     = 1 << 5,  // 左トリガ離し.
            RTriggerDown   = 1 << 6,  // 右トリガ押下.
            RTrigger       = 1 << 7,  // 右トリガ.
            RTriggerUp     = 1 << 8,  // 右トリガ離し.
            LGripDown      = 1 << 9,  // 左グリップ押下.
            LGrip          = 1 << 10, // 左グリップ.
            LGripUp        = 1 << 11, // 左グリップ離し.
            RGripDown      = 1 << 12, // 右グリップ押下.
            RGrip          = 1 << 13, // 右グリップ.
            RGripUp        = 1 << 14, // 右グリップ離し.
        }


        //------------------------------------------------------------------------------
        // キーの更新処理.
        public abstract void UpdateInput( ref InputKey _key, ref Vector2 _lAxis, ref Vector2 _rAxis );

        static public bool IsValidateKey( InputKey _key, InputKey _targetKey )
        {
            return ( _key & _targetKey ) != InputKey.None;
        }

    }
}


