﻿using System;
using UnityEngine;

namespace TheToonOfTheDead.Input
{
    //==============================================================================
    //------------------------------------------------------------------------------
    // 手振れ補正.
    public class ShakeCorrection : MonoBehaviour
    {
        //位置
        //補正をするか
        public bool posEnable = true;
        //二捨三入にするか
        public bool posDetailedAccuracy = false;
        //どの桁以上を出力するか
        public int posThreshold = 1;

        //角度
        //補正をするか
        public bool rotEnable = true;
        //二捨三入にするか
        public bool rotDetailedAccuracy = false;
        //どの桁以上を出力するか
        public int rotThreshold = 1;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            //位置を取得
            Vector3 pos = transform.position;
            //角度を取得
            Vector3 rot = transform.eulerAngles;

            //位置
            if (posEnable)
            {
                //二捨三入の準備
                if (posDetailedAccuracy)
                {
                    pos *= 2;
                }

                //四捨五入
                pos.x = (float)Math.Round(pos.x, posThreshold, MidpointRounding.AwayFromZero);
                pos.y = (float)Math.Round(pos.y, posThreshold, MidpointRounding.AwayFromZero);
                pos.z = (float)Math.Round(pos.z, posThreshold, MidpointRounding.AwayFromZero);

                //二捨三入へ変換
                if (posDetailedAccuracy)
                {
                    pos /= 2;
                }

                //結果を入力
                transform.position = pos;
            }


            //角度
            if (rotEnable)
            {
                //二捨三入の準備
                if (rotDetailedAccuracy)
                {
                    rot *= 2;
                }

                //四捨五入
                rot.x = (float)Math.Round(rot.x, posThreshold, MidpointRounding.AwayFromZero);
                rot.y = (float)Math.Round(rot.y, posThreshold, MidpointRounding.AwayFromZero);
                rot.z = (float)Math.Round(rot.z, posThreshold, MidpointRounding.AwayFromZero);

                //二捨三入へ変換
                if (rotDetailedAccuracy)
                {
                    rot /= 2;
                }

                //結果を入力
                transform.eulerAngles = rot;
            }
        }
    }}
