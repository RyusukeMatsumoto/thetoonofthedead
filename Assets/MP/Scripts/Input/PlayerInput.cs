﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;


using TheToonOfTheDead.Weapon;

namespace TheToonOfTheDead.Input
{
    //==============================================================================
    //------------------------------------------------------------------------------
    // コントローラ入力のカプセルクラス.
    public class PlayerInput : MonoBehaviour
    {

        [SerializeField] GameObject weaponObj;
        IWeapon weapon;

        GameInputBase input;
        public GameInputBase.InputKey inputKey;
        Vector2 lAxis = Vector2.zero;
        Vector2 rAxis = Vector2.zero;
        


        //------------------------------------------------------------------------------
        void Start()
        {
            input = this.GetInputClass();
            if (weaponObj != null)
                weapon = weaponObj.GetComponent<WeaponBase>() as IWeapon;
        }


        //------------------------------------------------------------------------------
        void Update()
        {
            input.UpdateInput(ref inputKey, ref lAxis, ref rAxis);
            if ( GameInputBase.IsValidateKey(inputKey,GameInputBase.InputKey.LTrigger) || GameInputBase.IsValidateKey(inputKey,GameInputBase.InputKey.RTrigger ) )
            {
                if (weapon.IsCoolTimeComplete())
                {
                    if ( 0 < weapon.GetRemainingBullets() )
                        weapon.Fire();
                    else
                        weapon.Reload();
                }
            }
        }


        //------------------------------------------------------------------------------
        // ゲームの入力処理を担うクラスを取得.
        GameInputBase GetInputClass()
        {
            //return new OculusGOInput();
            return new OculusRiftInput();
        }


    }
}

