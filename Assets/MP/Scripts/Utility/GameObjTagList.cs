﻿using UnityEngine;
using System.Collections;


namespace TheToonOfTheDead.Utility
{
    public static class GameObjTagList 
    {
        public static readonly string Player = "Player";
        public static readonly string Zombie = "Zombie";
        public static readonly string ZombieCommander = "ZombieCommander";
        public static readonly string JPCommander = "JPCommander";
        public static readonly string USCommander = "USCommander";
        public static readonly string UKCommander = "UKCommander";
        public static readonly string GCommander = "GCommander";
        public static readonly string SCommander = "SCommander";
        public static readonly string JPSoldier = "JPSoldier";
        public static readonly string USSoldier = "USSoldier";
        public static readonly string UKSoldier = "UKSoldier";
        public static readonly string GSoldier = "GSoldier";
        public static readonly string SSoldier = "JPSoldier";
    }
}
