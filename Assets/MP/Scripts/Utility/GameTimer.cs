﻿using UnityEngine;
using System.Collections;


namespace TheToonOfTheDead.Utility
{
    //==============================================================================
    //------------------------------------------------------------------------------
    // 一つのタイマーで1種類の時間のみ計測できるようにする.
    // コンストラクタ以外で測る時間を変更できなくすることである程度はセーフティ.
    public class GameTimer
    {

        float prevTime = -1f;
        public readonly float CompleteTime = 0f;


        //------------------------------------------------------------------------------
        // コンストラクタ.
        public GameTimer(){}
        public GameTimer(
                float _completeTime )
        {
            CompleteTime = _completeTime;
        }


        //------------------------------------------------------------------------------
        void ResetPrevTime()=> prevTime = -1f;


        //------------------------------------------------------------------------------
        /// <summary> タイマーのスタート. </summary>
        public void Start()=> prevTime = Time.time;


        //------------------------------------------------------------------------------
        public void Restart()
        {
            this.ResetPrevTime();
            this.Start();
        }


        //------------------------------------------------------------------------------
        /// <summary> 指定した時間を超えたか否か. </summary>
        /// <param name="_timerStop"> 自動リスタートするか否か. </param>
        /// <returns></returns>
        public bool IsComplete(
                bool _autoRestart = false )
        {
            if ( prevTime < 0 ) return false;
            bool _ret = CompleteTime <= ( Time.time - prevTime );
            if ( _ret && _autoRestart ) this.Restart();
            return _ret;
        }

    }
}

