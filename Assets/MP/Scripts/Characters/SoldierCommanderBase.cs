﻿using UnityEngine;
using System.Collections;


using TheToonOfTheDead.Utility;

namespace TheToonOfTheDead.Characters
{
    //==============================================================================
    //------------------------------------------------------------------------------
    // 各種兵士の司令塔クラスのベース.
    public class SoldierCommanderBase : MonoBehaviour
    {

        [SerializeField] protected Transform[] soldierList;
        [SerializeField] protected Transform[] battleStations;
        protected ICommander zombieCommander;


        //------------------------------------------------------------------------------
        protected void Start()
        {
            zombieCommander = GameObject.FindGameObjectWithTag(GameObjTagList.ZombieCommander).GetComponent<ZombieCommander>() as ICommander;
            if ( zombieCommander == null )
                Debug.LogError("zombieCommander 取得失敗");
        }


        //------------------------------------------------------------------------------
        /// <summary> 兵士の司令塔のインターフェース取得. </summary>
        /// <returns> 各兵士の司令塔インターフェース. </returns>
        public virtual ICommander GetInterface() => null;

    }
}

