﻿using UnityEngine;
using System.Collections;


namespace TheToonOfTheDead.Characters
{
    //==============================================================================
    //------------------------------------------------------------------------------
    // 各兵士のインターフェース.
    public interface ISoldier 
    {
        void FireSoldierSkill();
    }
}

