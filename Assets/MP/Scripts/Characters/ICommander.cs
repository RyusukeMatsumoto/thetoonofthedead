﻿using UnityEngine;
using System.Collections;


namespace TheToonOfTheDead.Characters
{
    public interface ICommander
    {
        ICommander GetInterface();
        Transform GetNextMovePoint();    // 次の移動先座標を取得.
        Transform GetPlayer();           // プレイヤーのTransformを取得.
        Transform GetRandom();           // 兵士のTransformを取得.
        Transform[] GetAll();            // すべて取得.
        void RemoveElem(Transform _elem); // 指定したターゲットを削除.
        Transform GetNeary(Transform _target); // 指定したオブジェクトに近いものを取得.
    }
}

