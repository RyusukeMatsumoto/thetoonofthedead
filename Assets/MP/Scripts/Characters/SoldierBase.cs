﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


namespace TheToonOfTheDead.Characters
{

    //==============================================================================
    //------------------------------------------------------------------------------
    // 各兵士のベースクラス.
    [RequireComponent(typeof(AudioSource), typeof(NavMeshAgent))]
    public class SoldierBase : CharacterBase
    {

        [SerializeField] protected SimpleAnimation simpleAnim;
        [SerializeField] protected AudioSource audioSource;
        [SerializeField] protected NavMeshAgent agent;



        // hp.
        protected float hp;
        public float Hp { get { return hp; } }


        //------------------------------------------------------------------------------
        void Awake()
        {
            audioSource = GetComponent<AudioSource>();
            agent = GetComponent<NavMeshAgent>();
        }


        //------------------------------------------------------------------------------
        // 初期化.
        public virtual void Init()
        {
        }

    }
}

