﻿using UnityEngine;
using System.Collections;

namespace TheToonOfTheDead.Characters
{
    //==============================================================================
    //------------------------------------------------------------------------------
    // 各アニメーションのキー定義クラス.
    public class SoldierAnimKey
    {
        public static readonly string Idle = "Default";
        public static readonly string Walk = "Walk";
        public static readonly string Run = "Run";
        public static readonly string Reload = "Reload";
        public static readonly string Shoot = "Shoot";
        public static readonly string Bolt = "Bolt";
        public static readonly string Bayonet = "Bayonet";
        public static readonly string Damage = "Damage";
        public static readonly string Death = "Death";

        public static readonly string CrouchIdle = "CrouchIdle";
        public static readonly string CrouchShoot = "CrouchShoot";
        public static readonly string CrouchReload = "CrouchReload";
        public static readonly string CrouchBolt = "CrouchBolt";
    }
}

