﻿using UnityEngine;



namespace TheToonOfTheDead.Characters
{
    //==============================================================================
    //------------------------------------------------------------------------------
    // 各ゾンビアニメーションキー.
    public class ZombieAnimKey
    {
        public static readonly string Idle = "Default";
        public static readonly string Walk = "Walk";
        public static readonly string Run = "Run";
        public static readonly string AttackL = "AttackL";
        public static readonly string AttackR = "AttackR";
        public static readonly string AttackBite = "AttackBite";
        public static readonly string DamageL = "DamageL";
        public static readonly string DamageR = "DamageR";
        public static readonly string DeathA = "DeathA";
        public static readonly string DeathB = "DeathB";
        public static readonly string DeathC = "DeathC";
        public static readonly string DeathD = "DeathD";
    }
}