﻿using UnityEngine;
using System.Collections;


using TheToonOfTheDead.Settings;

namespace TheToonOfTheDead.Characters
{
    //==============================================================================
    //------------------------------------------------------------------------------
    // ゾンビ,兵士双方に共通する処理はこちらで定義する.
    public interface ICharacter 
    {
        void SetDamage(WeaponAttackEffectData _attackEffect);
    }
}

