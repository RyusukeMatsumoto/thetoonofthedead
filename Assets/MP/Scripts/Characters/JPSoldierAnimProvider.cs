﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

namespace TheToonOfTheDead.Characters
{
    //==============================================================================
    //------------------------------------------------------------------------------
    // 日本兵のアニメーション管理スクリプト.
    public class JPSoldierAnimProvider : SoldierAnimProviderBase, ISoldierAnimProvider
    {



        //------------------------------------------------------------------------------
        void Start()
        {
            base.Init();
        }


        //------------------------------------------------------------------------------
        public void Idle(
                UnityAction _listener, UnityAction<string> _anyEvent = null)
        {
            if ( base.AnimationPlayable(SoldierAnimKey.Idle) )
                base.CrossFade(SoldierAnimKey.Idle, 0.5f, _listener, _anyEvent);
        }


        //------------------------------------------------------------------------------
        public void Walk(
                UnityAction _listener, UnityAction<string> _anyEvent = null)
        {
            if ( base.AnimationPlayable(SoldierAnimKey.Walk) )
                base.CrossFade(SoldierAnimKey.Walk, 0.5f, _listener, _anyEvent);
        }


        //------------------------------------------------------------------------------
        public void Run(
                UnityAction _listener, UnityAction<string> _anyEvent = null)
        {
            if ( base.AnimationPlayable(SoldierAnimKey.Run) )
                base.CrossFade(SoldierAnimKey.Run, 0.5f, _listener, _anyEvent);
        }


        //------------------------------------------------------------------------------
        public void Shoot(
                UnityAction _listener, UnityAction<string> _anyEvent = null)
        {
            if ( base.AnimationPlayable(SoldierAnimKey.Shoot) )
                base.CrossFade(SoldierAnimKey.Shoot, 0f, _listener, _anyEvent);
        }


        //------------------------------------------------------------------------------
        public void Bolt(
                UnityAction _listener, UnityAction<string> _anyEvent = null)
        {
            if ( base.AnimationPlayable(SoldierAnimKey.Bolt) )
                base.CrossFade(SoldierAnimKey.Bolt, 0f, _listener, _anyEvent);
        }

        
        //------------------------------------------------------------------------------
        public void Reload(
                UnityAction _listener, UnityAction<string> _anyEvent = null)
        {
            if ( base.AnimationPlayable(SoldierAnimKey.Reload) )
                base.CrossFade(SoldierAnimKey.Reload, 0.5f, _listener, _anyEvent);
        }
        

        //------------------------------------------------------------------------------
        public void CrouchIdle(
                UnityAction _listener, UnityAction<string> _anyEvent = null)
        {
            if ( base.AnimationPlayable(SoldierAnimKey.CrouchIdle) )
                base.CrossFade(SoldierAnimKey.CrouchIdle, 0.5f, _listener, _anyEvent);
        }


        //------------------------------------------------------------------------------
        public void CrouchShoot(
                UnityAction _listener, UnityAction<string> _anyEvent = null)
        {
            if ( base.AnimationPlayable(SoldierAnimKey.CrouchShoot) )
                base.CrossFade(SoldierAnimKey.CrouchShoot, 0.5f, _listener, _anyEvent);
        }


        //------------------------------------------------------------------------------
        public void CrouchBolt(
                UnityAction _listener, UnityAction<string> _anyEvent = null)
        {
            if ( base.AnimationPlayable(SoldierAnimKey.CrouchBolt) )
                base.CrossFade(SoldierAnimKey.CrouchBolt, 0.5f, _listener, _anyEvent);
        }


        //------------------------------------------------------------------------------
        public void CrouchReload(
                UnityAction _listener, UnityAction<string> _anyEvent = null)
        {
            if ( base.AnimationPlayable(SoldierAnimKey.CrouchReload) )
                base.CrossFade(SoldierAnimKey.CrouchReload, 0.5f, _listener, _anyEvent);
        }


        //------------------------------------------------------------------------------
        public void Bayonet(
                UnityAction _listener, UnityAction<string> _anyEvent = null)
        {
            if ( base.AnimationPlayable(SoldierAnimKey.Bayonet) )
                base.CrossFade(SoldierAnimKey.Bayonet, 0.5f, _listener, _anyEvent);
        }


        //------------------------------------------------------------------------------
        public void Damage(
                UnityAction _listener, UnityAction<string> _anyEvent = null)
        {
            if ( base.AnimationPlayable(SoldierAnimKey.Damage) )
                base.CrossFade(SoldierAnimKey.Damage, 0.5f, _listener, _anyEvent);
        }


        //------------------------------------------------------------------------------
        public void Death(
                UnityAction _listener, UnityAction<string> _anyEvent = null)
        {
            if ( base.AnimationPlayable(SoldierAnimKey.Death) )
                base.CrossFade(SoldierAnimKey.Death, 0f, _listener, _anyEvent);
        }


    }
}


