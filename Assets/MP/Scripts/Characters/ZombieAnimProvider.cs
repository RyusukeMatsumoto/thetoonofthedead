﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

namespace TheToonOfTheDead.Characters
{
    //==============================================================================
    //------------------------------------------------------------------------------
    // 通常のゾンビのアニメーション.
    public class ZombieAnimProvider : ZombieAnimProviderBase, IZombieAnimProvider
    {

        //------------------------------------------------------------------------------
        void Start()
        {
            base.Init();
        }


        //------------------------------------------------------------------------------
        public void AttackBite(
                UnityAction _listener)
        {
            if ( base.AnimationPlayable(ZombieAnimKey.AttackBite) )
                base.CrossFade(ZombieAnimKey.AttackBite, 0f, _listener);
        }


        //------------------------------------------------------------------------------
        public void AttackL(
                UnityAction _listener)
        {
            if ( base.AnimationPlayable(ZombieAnimKey.AttackL) )
                base.CrossFade(ZombieAnimKey.AttackL, 0f, _listener);
        }


        //------------------------------------------------------------------------------
        public void AttackR(
                UnityAction _listener)
        {
            if ( base.AnimationPlayable(ZombieAnimKey.AttackR) )
                base.CrossFade(ZombieAnimKey.AttackR, 1f, _listener);
        }


        //------------------------------------------------------------------------------
        public void DamageL(
                UnityAction _listener)
        {
            if ( base.AnimationPlayable(ZombieAnimKey.DamageL) )
                base.CrossFade(ZombieAnimKey.DamageL, 0f, _listener);
        }


        //------------------------------------------------------------------------------
        public void DamageR(
                UnityAction _listener)
        {
            if ( base.AnimationPlayable(ZombieAnimKey.DamageR) )
                base.CrossFade(ZombieAnimKey.DamageR, 0f, _listener);
        }


        //------------------------------------------------------------------------------
        public void DeathA(
                UnityAction _listener)
        {
            if ( base.AnimationPlayable(ZombieAnimKey.DeathA) )
                base.CrossFade(ZombieAnimKey.DeathA, 0f, _listener);
        }

        //------------------------------------------------------------------------------
        public void DeathB(
                UnityAction _listener)
        {
            if ( base.AnimationPlayable(ZombieAnimKey.DeathB) )
                base.CrossFade(ZombieAnimKey.DeathB, 0f, _listener);
        }


        //------------------------------------------------------------------------------
        public void DeathC(
                UnityAction _listener)
        {
            if ( base.AnimationPlayable(ZombieAnimKey.DeathC) )
                base.CrossFade(ZombieAnimKey.DeathC, 0f, _listener);
        }


        //------------------------------------------------------------------------------
        public void DeathD(
                UnityAction _listener)
        {
            if ( base.AnimationPlayable(ZombieAnimKey.DeathD) )
                base.CrossFade(ZombieAnimKey.DeathD, 0f, _listener);
        }


        //------------------------------------------------------------------------------
        public void Idle(
                UnityAction _listener)
        {
            if ( base.AnimationPlayable(ZombieAnimKey.Idle) )
                base.CrossFade(ZombieAnimKey.Idle, 0.1f, _listener);
        }

        //------------------------------------------------------------------------------
        public void Run(
                UnityAction _listener)
        {
            if ( base.AnimationPlayable(ZombieAnimKey.Run) )
                base.CrossFade(ZombieAnimKey.Run, 0.1f, _listener);
        }


        //------------------------------------------------------------------------------
        public void Walk(
                UnityAction _listener)
        {
            if ( base.AnimationPlayable(ZombieAnimKey.Walk) )
                base.CrossFade(ZombieAnimKey.Walk, 0.1f, _listener);
        }


    }
}

