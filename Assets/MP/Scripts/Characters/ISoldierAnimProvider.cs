﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

namespace TheToonOfTheDead.Characters
{
    //==============================================================================
    //------------------------------------------------------------------------------
    // 兵士のアニメーションプロバイダインタフェース.
    public interface ISoldierAnimProvider
    {

        // 立ちアニメーション.
        void Idle(UnityAction _listener = null, UnityAction<string> _anyEvent = null);
        void Walk(UnityAction _listener = null, UnityAction<string> _anyEvent = null);
        void Run(UnityAction _listener = null, UnityAction<string> _anyEvent = null);
        void Reload(UnityAction _listener = null, UnityAction<string> _anyEvent = null);
        void Shoot(UnityAction _listener = null, UnityAction<string> _anyEvent = null);
        void Bolt(UnityAction _listener = null, UnityAction<string> _anyEvent = null);
        void Bayonet(UnityAction _listener = null, UnityAction<string> _anyEvent = null);
        void Damage(UnityAction _listener = null, UnityAction<string> _anyEvent = null);
        void Death(UnityAction _listener = null, UnityAction<string> _anyEvent = null);

        // しゃがみアニメーション.
        void CrouchIdle(UnityAction _listener = null, UnityAction<string> _anyEvent = null);
        void CrouchShoot(UnityAction _listener = null, UnityAction<string> _anyEvent = null);
        void CrouchReload(UnityAction _listener = null, UnityAction<string> _anyEvent = null);
        void CrouchBolt(UnityAction _listener = null, UnityAction<string> _anyEvent = null);

    }
}

