﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace TheToonOfTheDead.Characters
{ 
    //==============================================================================
    //------------------------------------------------------------------------------
    public class ZombieAnimProviderBase : AnimProviderBase
    {


        //------------------------------------------------------------------------------
        void Awake()
        {
            this.Init();
        }


        //------------------------------------------------------------------------------
        /// <summary> ディクショナリの初期化など( 必ず行う ). </summary>
        protected override void Init()
        {
            base.Init();
            if ( simpleAnim == null ) simpleAnim = GetComponent<SimpleAnimation>();

            // ディクショナリを登録.
            animCompleteListenerDic = new Dictionary<string, AnimationExeData>()
            {
                {ZombieAnimKey.Idle, new AnimationExeData(true) },
                {ZombieAnimKey.Walk, new AnimationExeData(true) },
                {ZombieAnimKey.Run, new AnimationExeData(true) },
                {ZombieAnimKey.AttackL, new AnimationExeData() },
                {ZombieAnimKey.AttackR, new AnimationExeData() },
                {ZombieAnimKey.AttackBite, new AnimationExeData() },
                {ZombieAnimKey.DamageL, new AnimationExeData() },
                {ZombieAnimKey.DamageR, new AnimationExeData() },
                {ZombieAnimKey.DeathA, new AnimationExeData() },
                {ZombieAnimKey.DeathB, new AnimationExeData() },
                {ZombieAnimKey.DeathC, new AnimationExeData() },
                {ZombieAnimKey.DeathD, new AnimationExeData() },
            };
        }

    }
}

