﻿using UnityEngine;
using System.Collections;


using TheToonOfTheDead.Characters.State;
using TheToonOfTheDead.Settings;

namespace TheToonOfTheDead.Characters
{
    //==============================================================================
    //------------------------------------------------------------------------------
    public class NormalZombie : ZombieBase, IZombie, ICharacter
    {

        IZombieAnimProvider animProv;
        StateBase state;
        CharacterParam param;
        


        //------------------------------------------------------------------------------
        void Awake()
        {
        }


        //------------------------------------------------------------------------------
        void Start()
        {
            this.Init();
        }


        //------------------------------------------------------------------------------
        void Update()
        {
            state = state.Update(param);
        }


        //------------------------------------------------------------------------------
        public override void Init()
        {
            base.Init();
            animProv = GetComponent<ZombieAnimProvider>() as IZombieAnimProvider;
            param = new CharacterParam(gameObject, null, transform, agent, initHp);
            param.SetDeathListener(this.DeathListener);
            state = new ZombieMovementState(param);
        }


        //------------------------------------------------------------------------------
        void DeathListener()
        {
            state = new ZombieDeathState(param);
        }


        //------------------------------------------------------------------------------
        /// <summary> ダメージのセット. </summary>
        /// <param name="_damage"> ダメージ量. </param>
        public void SetDamage(
                 WeaponAttackEffectData _attackEffect )
        {
            param.SetDamage(_attackEffect.EffectPower);
        }


    }
}


