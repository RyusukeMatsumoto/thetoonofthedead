﻿using UnityEngine;
using System.Collections;


using TheToonOfTheDead.Utility;

namespace TheToonOfTheDead.Characters
{
    //==============================================================================
    //------------------------------------------------------------------------------
    // 日本兵の司令塔.
    public class JPCommander : SoldierCommanderBase, ICommander
    {


        //------------------------------------------------------------------------------
        void Start()
        {
            base.Start();

        }


        //------------------------------------------------------------------------------
        void Update()
        {

        }

        
        //------------------------------------------------------------------------------
        /// <summary> 必要なインターフェースを返す. <summary>
        /// <returns> インターフェース. </returns>
        public override ICommander GetInterface() => this;


        //------------------------------------------------------------------------------
        public Transform GetPlayer()
        {
            return GameObject.FindGameObjectWithTag(GameObjTagList.Player).transform;
        }

        
        //------------------------------------------------------------------------------
        public Transform GetNextMovePoint()
        {
            // 今は適当な位置を渡してる.
            if ( battleStations == null )
            {
                GameObject _obj = new GameObject("dummyBattleStation");
                _obj.transform.position = Vector3.zero;
                _obj.transform.SetParent(transform);
                battleStations = new Transform[1];
                battleStations[0] = _obj.transform;
                return battleStations[0];
            }
            return battleStations[Random.Range(0, battleStations.Length)];
        }


        //------------------------------------------------------------------------------
        public Transform GetRandom()
        {
            if ( soldierList == null ) return null;
            return soldierList[Random.Range(0, soldierList.Length)];
        }


        //------------------------------------------------------------------------------
        public Transform[] GetAll()=> soldierList;


        //------------------------------------------------------------------------------
        public void RemoveElem( Transform _elem ){}

        
        //------------------------------------------------------------------------------
        public Transform GetNeary(
                Transform _target )
        {
            Transform _ret = null;
            float _distance = 0f;
            foreach ( var _zombie in zombieCommander.GetAll() )
            {
                if (_zombie == null ) break;
                if ( _ret == null )
                {
                    _ret = _zombie;
                    _distance = Vector3.Distance(_target.position, _zombie.position);
                    continue;
                }
                else
                {
                    float _tempDistance = Vector3.Distance(_target.position, _zombie.position);
                    if ( _tempDistance < _distance )
                    {
                        _ret = _zombie;
                        _distance = _tempDistance;
                    }
                }
            }
            return _ret;
        }



    }
}

