﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;

using TheToonOfTheDead.Characters.State;

namespace TheToonOfTheDead.Characters
{
    //==============================================================================
    //------------------------------------------------------------------------------
    // 各ゾンビクラスのベース.
    [RequireComponent(typeof(Rigidbody))]
    public class ZombieBase : CharacterBase
    {

        [SerializeField] protected SimpleAnimation simpleAnim;
        [SerializeField] protected AudioSource audioSource;
        [SerializeField] protected NavMeshAgent agent;
        [SerializeField] protected float initHp;
        

        //------------------------------------------------------------------------------
        // 初期化.
        public virtual void Init()
        {
            GetComponent<Rigidbody>().isKinematic = true;
            audioSource = GetComponent<AudioSource>();
            simpleAnim = GetComponent<SimpleAnimation>();
            agent = GetComponent<NavMeshAgent>();
        }

    }
}


