﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using System;

namespace TheToonOfTheDead.Characters
{
    //==============================================================================
    //------------------------------------------------------------------------------
    // アニメーション管理用クラスのベースクラス.
    // SimpleAnimation前提.
    public class SoldierAnimProviderBase : AnimProviderBase
    {

        //------------------------------------------------------------------------------
        /// <summary> ディクショナリの初期化など( 必ず行う ). </summary>
        protected override void Init()
        {
            if ( simpleAnim == null ) simpleAnim = GetComponent<SimpleAnimation>();

            // ディクショナリを登録.
            animCompleteListenerDic = new Dictionary<string, AnimationExeData>()
            {
                {SoldierAnimKey.Idle, new AnimationExeData(true)},
                {SoldierAnimKey.Walk, new AnimationExeData(true)},
                {SoldierAnimKey.Run, new AnimationExeData(true)},
                {SoldierAnimKey.Reload, new AnimationExeData()},
                {SoldierAnimKey.Shoot, new AnimationExeData()},
                {SoldierAnimKey.Bolt, new AnimationExeData()},
                {SoldierAnimKey.Bayonet, new AnimationExeData()},
                {SoldierAnimKey.Damage, new AnimationExeData()},
                {SoldierAnimKey.Death, new AnimationExeData()},

                {SoldierAnimKey.CrouchIdle, new AnimationExeData(true)},
                {SoldierAnimKey.CrouchShoot, new AnimationExeData()},
                {SoldierAnimKey.CrouchBolt, new AnimationExeData()},
                {SoldierAnimKey.CrouchReload, new AnimationExeData()},
            };
        }

        

    }
}

