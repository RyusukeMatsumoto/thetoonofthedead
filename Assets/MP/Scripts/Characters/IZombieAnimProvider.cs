﻿using UnityEngine;
using UnityEngine.Events;


namespace TheToonOfTheDead.Characters
{
    //==============================================================================
    //------------------------------------------------------------------------------
    public interface IZombieAnimProvider
    {
        void Idle(UnityAction _listener = null);
        void Walk(UnityAction _listener = null);
        void Run(UnityAction _listener = null);
        void AttackL(UnityAction _listener = null);
        void AttackR(UnityAction _listener = null);
        void AttackBite(UnityAction _listener = null);
        void DamageL(UnityAction _listener = null);
        void DamageR(UnityAction _listener = null);
        void DeathA(UnityAction _listener = null);
        void DeathB(UnityAction _listener = null);
        void DeathC(UnityAction _listener = null);
        void DeathD(UnityAction _listener = null);
    }
}
