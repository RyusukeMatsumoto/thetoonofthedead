﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using System.Collections;
using System;

using TheToonOfTheDead.Weapon;

namespace TheToonOfTheDead.Characters.State
{
    //==============================================================================
    //------------------------------------------------------------------------------
    public class CharacterParam : IDisposable
    {

        public GameObject Obj { get; private set; }
        public Transform Trans{ get; private set; }
        public NavMeshAgent Agent { get; private set; }
        public float MaxHp { get; private set; }
        public bool IsDeath { get; private set; } = false;
        public IWeapon Weapon { get; private set; }

        UnityAction deathListener;
        float hp;
        bool isDeath = false;


        //------------------------------------------------------------------------------
        // コンストラクタ.
        public CharacterParam(
                GameObject _obj,
                IWeapon _weapon,
                Transform _trans,
                NavMeshAgent _agent,
                float _hp )
        {
            Obj = _obj;
            Weapon = _weapon;
            Trans = _trans;
            Agent = _agent;
            MaxHp = hp = _hp;
        }


        //------------------------------------------------------------------------------
        /// <summary> ダメージをセット, hpが0以下になったら死亡リスナを起動. </summary>
        /// <param name="_damage"> ダメージ量. </param>
        public void SetDamage(
                float _damage )
        {
            hp -= _damage;
            if ( hp <= 0f && !isDeath )
            {
                deathListener?.Invoke();
                isDeath = true;
            }
        }


        //------------------------------------------------------------------------------
        /// <summary> Hpを回復. </summary>
        /// <param name="_val"> 回復量. </param>
        public void RecoveryHp(
                float _val )
        {
            if ( hp <= 0f ) return;
            hp += _val;
        }


        //------------------------------------------------------------------------------
        /// <summary> 死亡時リスナをセット. </summary>
        /// <param name="_listener"> リスナ. </param>
        public void SetDeathListener( UnityAction _listener )=> deathListener += _listener;


        //------------------------------------------------------------------------------
        public void Dispose()
        {
            deathListener = null;
        }


    }
}
