﻿using UnityEngine;
using System.Collections;

using TheToonOfTheDead.Utility;

namespace TheToonOfTheDead.Characters.State
{
    //==============================================================================
    //------------------------------------------------------------------------------
    public class JPSoldierMovementState : StateBase
    {

        ICommander commander;
        ISoldierAnimProvider animProv;
        Transform nextMovePos;


        //------------------------------------------------------------------------------
        public JPSoldierMovementState(
                CharacterParam _data)
        {
            commander = GameObject.FindGameObjectWithTag(GameObjTagList.JPCommander).GetComponent<JPCommander>().GetInterface();
            animProv = _data.Obj.GetComponent<JPSoldierAnimProvider>() as ISoldierAnimProvider;
            nextMovePos = commander != null ? commander.GetNextMovePoint() : _data.Trans;

            animProv.Run();
            _data.Agent.SetDestination(nextMovePos.position);
        }


        //------------------------------------------------------------------------------
        public override StateBase Update(
                CharacterParam _data )
        {
            if ( Vector3.Distance(_data.Trans.position, nextMovePos.position) < 1f )
            {
                //return new JPSoldierMovementState(_data);
                return new JPSoldierAttackState(_data);
            }
            return this;
        }


    }
}


