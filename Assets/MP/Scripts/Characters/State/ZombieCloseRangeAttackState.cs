﻿using UnityEngine;
using System.Collections;


namespace TheToonOfTheDead.Characters.State
{
    //==============================================================================
    //------------------------------------------------------------------------------
    // 近接攻撃ステート.
    public class ZombieCloseRangeAttackState : StateBase
    {

        bool isEnd = false;
        IZombieAnimProvider animProv;



        //------------------------------------------------------------------------------
        // コンストラクタ.
        public ZombieCloseRangeAttackState(
                CharacterParam _data )
        {
            animProv = _data.Obj.GetComponent<ZombieAnimProvider>() as IZombieAnimProvider;
            _data.Agent.isStopped = true;

            animProv.AttackL(this.AttackAnimEndListener);

        }


        //------------------------------------------------------------------------------
        public override StateBase Update(
                CharacterParam _data)
        {
            return ( isEnd ) ? new ZombieMovementState(_data) : this as StateBase;
        }


        //------------------------------------------------------------------------------
        void AttackAnimEndListener()
        {
            isEnd = true;
            animProv.Walk();
        }

    }
}

