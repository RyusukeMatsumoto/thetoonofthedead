﻿using UnityEngine;
using System.Collections;


using TheToonOfTheDead.Utility;

namespace TheToonOfTheDead.Characters.State
{
    //==============================================================================
    //------------------------------------------------------------------------------
    // 日本兵の攻撃ステート.
    public class JPSoldierDeathState : StateBase
    {

        ICommander commander;
        ISoldierAnimProvider animProv;



        //------------------------------------------------------------------------------
        public JPSoldierDeathState(
                CharacterParam _data)
        {
            commander = GameObject.FindGameObjectWithTag(GameObjTagList.JPCommander).GetComponent<JPCommander>().GetInterface();
            animProv = _data.Obj.GetComponent<JPSoldierAnimProvider>() as ISoldierAnimProvider;

            animProv.Death();

            _data.Agent.isStopped = true;
        }


        //------------------------------------------------------------------------------
        public override StateBase Update(
                CharacterParam _data)
        {
            return this;
        }


    }
}


