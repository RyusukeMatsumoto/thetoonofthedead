﻿using UnityEngine;
using System.Collections;


using TheToonOfTheDead.Utility;

namespace TheToonOfTheDead.Characters.State
{
    //==============================================================================
    //------------------------------------------------------------------------------
    public class ZombieMovementState : StateBase
    {

        static readonly float AttackRange = 2.5f;

        ICommander commander;
        Transform target;
        IZombieAnimProvider animProv;


        //------------------------------------------------------------------------------
        // コンストラクタ.
        public ZombieMovementState(
                CharacterParam _data)
        {
            commander = GameObject.FindGameObjectWithTag(GameObjTagList.ZombieCommander).GetComponent<ZombieCommander>().GetInterface();
            _data.Agent.stoppingDistance = 1;

            animProv = _data.Obj.GetComponent<ZombieAnimProvider>() as IZombieAnimProvider;
            animProv.Walk();
        }


        //------------------------------------------------------------------------------
        public override StateBase Update(
                CharacterParam _data)
        {

            if ( commander == null ) return this;
            if ( target == null )
            {
                target = commander?.GetNeary(_data.Trans);
                return this;
            }

            _data.Agent.SetDestination(target.position);
            if ( Vector3.Distance(_data.Trans.position, target.position) <= AttackRange )
                return new ZombieCloseRangeAttackState(_data);

            return this;
        }

    }
}

