﻿using UnityEngine;
using System.Collections;


using TheToonOfTheDead.Utility;
using TheToonOfTheDead.Weapon;
using TheToonOfTheDead.Settings;

namespace TheToonOfTheDead.Characters.State
{
    //==============================================================================
    //------------------------------------------------------------------------------
    // 日本兵の攻撃ステート.
    public class JPSoldierAttackState : StateBase
    {

        ICommander commander;
        ISoldierAnimProvider animProv;
        IWeapon weapon;
        AudioSource weaponAudio;


        float coolTime = 5f; // テストで作った後で変更する( クールタイムは武器が主体となる ).
        float timer = 0f;
        int count = 0;
        Transform targetZombie;


        //------------------------------------------------------------------------------
        public JPSoldierAttackState(
                CharacterParam _data)
        {
            commander = GameObject.FindGameObjectWithTag(GameObjTagList.JPCommander).GetComponent<JPCommander>().GetInterface();
            animProv = _data.Obj.GetComponent<JPSoldierAnimProvider>() as ISoldierAnimProvider;
            weapon = _data.Weapon;
            weaponAudio = _data.Weapon.GetComponent<AudioSource>(typeof(AudioSource));

            animProv.Idle();
            _data.Agent.isStopped = true;
        }


        //------------------------------------------------------------------------------
        public override StateBase Update(
                CharacterParam _data)
        {
            if ( _data.Weapon.IsCoolTimeComplete() )
            {
                this.UpdateTargetZombie(_data);
                if (targetZombie)
                {
                    weapon.Fire();
                    animProv.Bolt(this.BoltCompleteListener, this.AnimAnyEventListener);
                }
            }
            return this;
        }


        //------------------------------------------------------------------------------
        void BoltCompleteListener()
        {
            if ( weapon.GetRemainingBullets() <= 0 )
            {
                weapon.Reload();
                animProv.Reload(() => animProv.Idle());
            }
            else
            {
                animProv.Idle();
            }
        }


        //------------------------------------------------------------------------------
        void AnimAnyEventListener(
            string _code )
        {
        }


        //------------------------------------------------------------------------------
        void UpdateTargetZombie(
                CharacterParam _data )
        {
            targetZombie = commander.GetNeary(_data.Trans);
            if (targetZombie == null) return;

            // ここはとりあえず入れたもの.
            Vector3 _temp = targetZombie.position;
            _temp.y = _data.Trans.position.y;
            _data.Trans.LookAt(_temp);
        }

    }
}


