﻿using UnityEngine;
using System.Collections;
using TheToonOfTheDead.Utility;


namespace TheToonOfTheDead.Characters.State
{
    //==============================================================================
    //------------------------------------------------------------------------------
    // ゾンビ死亡ステート.
    public class ZombieDeathState : StateBase
    {

        IZombieAnimProvider prov;
        ICommander commander;
        GameTimer timer;


        //------------------------------------------------------------------------------
        // コンストラクタ.
        public ZombieDeathState(
                CharacterParam _data )
        {
            _data.Agent.isStopped = true;
            commander = GameObject.FindGameObjectWithTag(GameObjTagList.ZombieCommander).GetComponent<ZombieCommander>().GetInterface();
            prov = _data.Obj.GetComponent<ZombieAnimProvider>() as IZombieAnimProvider;
            switch ( Random.Range(0,4) )
            {
                case 0: prov.DeathA(); break;
                case 1: prov.DeathB(); break;
                case 2: prov.DeathC(); break;
                case 3: prov.DeathD(); break;
            }
            
            timer = new GameTimer(5f);
            timer.Start();
        }


        //------------------------------------------------------------------------------
        public override StateBase Update(
                CharacterParam _data)
        {
            if (timer.IsComplete())
            {
                commander.RemoveElem(_data.Trans);                
                GameObject.Destroy(_data.Obj);
            }


            return this;
        }


    }
}
