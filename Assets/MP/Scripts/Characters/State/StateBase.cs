﻿using UnityEngine;
using System.Collections;

namespace TheToonOfTheDead.Characters.State
{
    //==============================================================================
    //------------------------------------------------------------------------------
    // 各種ステートクラスのベースクラス.
    public class StateBase
    {


        //------------------------------------------------------------------------------
        /// <summary> 更新. </summary>
        /// <param name="_data"></param>
        /// <returns> ステート. </returns>
        public virtual StateBase Update(
                CharacterParam _data )
        {

            //_data.Agent.SetDestination(_data.Trans.position + Vector3.forward * 0.1f);

            return this;
        }

    }
}

