﻿using UnityEngine;
using System.Collections;

using TheToonOfTheDead.Settings;
using TheToonOfTheDead.Characters.State;
using TheToonOfTheDead.Weapon;

namespace TheToonOfTheDead.Characters
{
    //==============================================================================
    //------------------------------------------------------------------------------
    // 通常日本兵クラス.
    public class JPNormalSoldier : SoldierBase, ISoldier, ICharacter
    {

        [SerializeField] SoundPackage soundPack;
        ISoldierAnimProvider animProv;
        IWeapon weapon;
        StateBase state;
        CharacterParam param;




        //------------------------------------------------------------------------------
        void Start()
        {
            this.Init();
        }


        //------------------------------------------------------------------------------
        void Update()
        {
            /*
            if ( UnityEngine.Input.GetKeyDown(KeyCode.Alpha0) ) animProv.Reload(()=> { animProv.Idle(); });
            if ( UnityEngine.Input.GetKeyDown(KeyCode.Alpha1) ) animProv.Shoot(()=> { animProv.Idle(); });
            if ( UnityEngine.Input.GetKeyDown(KeyCode.Alpha2) ) animProv.Bolt(()=> { animProv.Idle(); });
            if ( UnityEngine.Input.GetKeyDown(KeyCode.Alpha3) ) animProv.CrouchIdle(()=> { animProv.Idle(); });
            if ( UnityEngine.Input.GetKeyDown(KeyCode.Alpha4) ) animProv.CrouchShoot(() => { animProv.Idle(); });
            if ( UnityEngine.Input.GetKeyDown(KeyCode.Alpha5) ) animProv.CrouchBolt(() => { animProv.Idle(); });
            if ( UnityEngine.Input.GetKeyDown(KeyCode.Alpha6) ) animProv.CrouchReload(() => { animProv.Idle(); });
            if ( UnityEngine.Input.GetKeyDown(KeyCode.Alpha7) ) animProv.Run();
            if ( UnityEngine.Input.GetKeyDown(KeyCode.Alpha8) ) animProv.Bayonet(() => { animProv.Idle(); });
            if ( UnityEngine.Input.GetKeyDown(KeyCode.Alpha9) ) animProv.Damage(() => { animProv.Idle(); });
            if ( UnityEngine.Input.GetKeyDown(KeyCode.A) ) animProv.Death(() => { animProv.Idle(); });
            if ( UnityEngine.Input.GetKeyDown(KeyCode.B) ) animProv.Walk(() => { animProv.Idle(); });
            */

            if ( UnityEngine.Input.GetKeyDown(KeyCode.Space) )
                this.FireSoldierSkill();

            state = state.Update(param);
        }

        //------------------------------------------------------------------------------
        void DeathListener()
        {
            Debug.Log("DeathListener : 死んだぜ!!");
            //state = new ZombieDeathState(param);
        }


        //------------------------------------------------------------------------------
        // 初期化.
        public override void Init()
        {
            base.Init();
            hp = 100f;
            animProv = GetComponent<JPSoldierAnimProvider>() as ISoldierAnimProvider;
            weapon = weaponObj.GetComponent<Type38Rifle>() as IWeapon;

            param = new CharacterParam(gameObject, weapon, transform, agent, hp);
            param.SetDeathListener(this.DeathListener);
            
            state = new JPSoldierMovementState(param);
        }


        //------------------------------------------------------------------------------
        // ダメージのセット.
        public void SetDamage(
                WeaponAttackEffectData _attackEffect )
        {
            Debug.Log($"Damage : {_attackEffect.ToString()}");
        }


        //------------------------------------------------------------------------------
        // 日本兵のスキルを発動.
        public void FireSoldierSkill()
        {
            audioSource.PlayOneShot(soundPack.SkillVoice.GetClip());
        }

    }
}


