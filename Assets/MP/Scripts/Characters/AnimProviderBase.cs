﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using System;


namespace TheToonOfTheDead.Characters
{
    //==============================================================================
    //------------------------------------------------------------------------------
    // 各種アニメーションプロバイダクラスのベース.
    [RequireComponent(typeof(SimpleAnimation))]
    public class AnimProviderBase : MonoBehaviour
    {

        #region --- AnimationExeData ---------------------------------------------------
        //==============================================================================
        //------------------------------------------------------------------------------
        public class AnimationExeData : IDisposable
        {
            public bool IsPlayable { get; set; }
            UnityAction listener;
            readonly bool isAllwaysPlayable = false;


            //------------------------------------------------------------------------------
            public AnimationExeData()
            {
                listener = null;
                IsPlayable = true;
            }
            public AnimationExeData(
                    bool _isAlwaysPlayable)
            {
                isAllwaysPlayable = _isAlwaysPlayable;
                listener = null;
                IsPlayable = true;
            }


            //------------------------------------------------------------------------------
            /// <summary> リスナのセット,nullセットの場合でもIsPlayingの設定があるので呼び出す. </summary>
            /// <param name="_listener"> アニメーション官僚時のリスナ. </param>
            public void SetListener(
                    UnityAction _listener )
            {
                listener = _listener;
                IsPlayable = isAllwaysPlayable;
            }


            //------------------------------------------------------------------------------
            /// <summary> リスナの実行,実行後リスナを破棄する. </summary>
            public void Invoke()
            {
                listener?.Invoke();
                listener = null;
                IsPlayable = true;
            }


            //------------------------------------------------------------------------------
            public void Dispose()
            {
                listener = null;
            }
        }
        #endregion --- AnimationExeData ------------------------------------------------



        [SerializeField] protected SimpleAnimation simpleAnim;
        protected Dictionary<string, AnimationExeData> animCompleteListenerDic = null;
        string prevKey = "";
        UnityAction<string> anyEventListener;


        //------------------------------------------------------------------------------
        void Awake()
        {
            this.Init();
        }


        //------------------------------------------------------------------------------
        void OnDestroy()
        {
            animCompleteListenerDic.Clear();
        }


        //------------------------------------------------------------------------------
        protected virtual void Init()
        { }


        //------------------------------------------------------------------------------
        // アニメーション終了イベント,登録した完了リスナはここで実行される.
        void AnimEnd(
                string _key)
        {
            this.InvokeListener(_key);
        }


        //------------------------------------------------------------------------------
        // アニメーションのうちの任意なタイミングで発生させるイベント.
        void AnyEvent(
                string _code )
        {
            anyEventListener?.Invoke(_code);
        }


        //------------------------------------------------------------------------------
        // アニメーション完了リスナ実行.
        void InvokeListener(
                string _key)
        {
            if ( animCompleteListenerDic == null ) this.Init();
            if ( !animCompleteListenerDic.ContainsKey(_key) ) return;
            animCompleteListenerDic[_key]?.Invoke();
            animCompleteListenerDic[_key].IsPlayable = true;
            Debug.Log($"Invoke {_key}");
        }


        //------------------------------------------------------------------------------
        /// <summary> ディクショナリに完了リスナを登録する. </summary>
        /// <param name="_key"> キー. </param>
        /// <param name="_listener"> 完了リスナ. </param>
        void SetCompleteListener(
                string _key,
                UnityAction _listener)
        {
            if ( animCompleteListenerDic == null ) this.Init();
            if ( !animCompleteListenerDic.ContainsKey(_key) ) return;
            animCompleteListenerDic[_key].SetListener(_listener);
        }


        //------------------------------------------------------------------------------
        /// <summary> 指定したキーのアニメーションが再生可能か否か判定. </summary>
        /// <param name="_key"> アニメーションのキー. </param>
        /// <returns> 可能か否か. </returns>
        protected bool AnimationPlayable(
                string _key)
        {
            if ( !animCompleteListenerDic.ContainsKey(_key) ) return false;
            return animCompleteListenerDic[_key].IsPlayable;
        }


        //------------------------------------------------------------------------------
        /// <summary> アニメーションのクロスフェード. </summary>
        /// <param name="_key"> キー. </param>
        /// <param name="_fadeLength"> フェードの長さ. </param>
        /// <param name="_listener"> 完了リスナ. </param>
        protected void CrossFade(
                string _key,
                float _fadeLength,
                UnityAction _listener,
                UnityAction<string> _anyEventListener = null)
        {
            simpleAnim.CrossFade(_key, _fadeLength);
            this.SetCompleteListener(_key, _listener);
            anyEventListener = _anyEventListener;
            if ( prevKey != "" )
            {
                animCompleteListenerDic[prevKey].IsPlayable = true;
                prevKey = _key;
            }
            else
                prevKey = _key;
        }


        //------------------------------------------------------------------------------
        /// <summary> アニメーションのクロスフェード,キューバージョン. </summary>
        /// <param name="_key"> キー. </param>
        /// <param name="_fadeLength"> フェードの長さ. </param>
        /// <param name="_mode"> キューのモード. </param>
        /// <param name="_listener"> 完了リスナ. </param>
        protected void CrossFadeQueue(
                string _key,
                float _fadeLength,
                QueueMode _mode,
                UnityAction _listener)
        {
            simpleAnim.CrossFadeQueued(_key, _fadeLength, _mode);
            this.SetCompleteListener(_key, _listener);
        }
    }
}

