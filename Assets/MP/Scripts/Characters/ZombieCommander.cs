﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using TheToonOfTheDead.Utility;


namespace TheToonOfTheDead.Characters
{
    //==============================================================================
    //------------------------------------------------------------------------------
    // ゾンビ達の司令塔クラス.
    public class ZombieCommander : MonoBehaviour, ICommander
    {

        ICommander soldierCommander;


        [SerializeField] GameObject zombiePF; // ゾンビのプレハブ( 今後は別スクリプタブルオブジェクトで参照を取得予定 ).
        [SerializeField] Transform[] spawnPoint; // ゾンビのスポーンポイント.
        List<Transform> zombieList;

        //------------------------------------------------------------------------------
        void Start()
        {
            zombieList = new List<Transform>();
            soldierCommander = GameObject.FindGameObjectWithTag(GameObjTagList.JPCommander).GetComponent<SoldierCommanderBase>().GetInterface();
            if ( soldierCommander == null )
                Debug.LogError("soldierCommander is null");
            StartCoroutine(this.SpawnZombie());
        }


        //------------------------------------------------------------------------------
        void Update()
        {
        }


        //------------------------------------------------------------------------------
        void Init()
        {
            
        }

        
        //------------------------------------------------------------------------------
        // ゾンビのスポーン処理( とりあえずテスト ).
        IEnumerator SpawnZombie()
        {

            while (true)
            {
                yield return new WaitForSeconds(2f);
                GameObject _zombie = GameObject.Instantiate(zombiePF);
                if ( spawnPoint == null ) 
                    _zombie.transform.SetPositionAndRotation(Vector3.zero, Quaternion.identity);
                else
                    _zombie.transform.SetPositionAndRotation(spawnPoint[Random.Range(0, spawnPoint.Length -1)].position, Quaternion.identity);
                zombieList.Add(_zombie.transform);
            }
        }
        
        
        //------------------------------------------------------------------------------
        public ICommander GetInterface()=> this;

        
        //------------------------------------------------------------------------------
        /// <summary> すべてのゾンビを取得. </summary>
        /// <returns> ゾンビのTransform. </returns>
        public Transform[] GetAllZombie()
        {
            return zombieList.ToArray();
        }


        //------------------------------------------------------------------------------
        public Transform GetNextMovePoint()=> soldierCommander.GetRandom();


        //------------------------------------------------------------------------------
        public Transform GetPlayer() => null;


        //------------------------------------------------------------------------------
        public Transform GetRandom()
        {
            if ( zombieList == null ) return null;
            return zombieList[Random.Range(0, zombieList.Count)];
        }


        //------------------------------------------------------------------------------
        public Transform[] GetAll()
        {
            return zombieList.ToArray();
        }


        //------------------------------------------------------------------------------
        public void RemoveElem(
            Transform _elem)
        {
            zombieList.Remove(_elem);
        }
        
        
        //------------------------------------------------------------------------------
        public Transform GetNeary(
                Transform _target )
        {
            Transform _ret = null;
            float _distance = 0f;
            foreach ( var _soldier in soldierCommander.GetAll() )
            {
                if ( _ret == null )
                {
                    _ret = _soldier;
                    _distance = Vector3.Distance(_target.position, _soldier.position);
                    continue;
                }
                else
                {
                    float _tempDistance = Vector3.Distance(_target.position, _soldier.position);
                    if ( _tempDistance < _distance )
                    {
                        _ret = _soldier;
                        _distance = _tempDistance;
                    }
                }
            }
            return _ret;
        }
    }
}

