﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;


namespace TheToonOfTheDead.Settings
{
    //==============================================================================
    //------------------------------------------------------------------------------
    // サウンドパッケージ.
    [System.Serializable]
    public class SoundPackage : ScriptableObject
    {
        #region --- SoundContainer -----------------------------------------------------
        //==============================================================================
        //------------------------------------------------------------------------------
        // サウンドコンテナクラス.
        [System.Serializable]
        public class SoundContainer
        {
            #region --- Content ------------------------------------------------------------
            //==============================================================================
            //------------------------------------------------------------------------------
            [System.Serializable]
            public class Content
            {
                [SerializeField] AudioClip clip;
                public AudioClip Clip { get { return clip; } }

                [SerializeField,Range(0,90)] int weight;
                public int Weight { get { return weight; } }

                [SerializeField] string key;
                public string Key { get { return key; } }

                public Content()
                {
                    clip = null;
                    weight = 0;
                    key = "";
                }
                public Content(
                        AudioClip _clip,
                        int _weight,
                        string _key = "" )
                {
                    clip = _clip;
                    weight = _weight;
                    key = _key;
                }

            }
            #endregion --- Content ---------------------------------------------------------


            // コンテンツ配列が単一の要素のみか否か.
            public bool IsContentArray
            {
                get
                {
                    if ( contents == null ) return false;
                    return 1 < contents.Length;
                }
            }

            // コンテンツに要素があるか否か.
            public bool IsContentsExists { get { return contents != null; } }

            [SerializeField] Content[] contents;


            //------------------------------------------------------------------------------
            public SoundContainer(){}


            //------------------------------------------------------------------------------
            public Content GetContent()
            {
                if ( contents == null ) return null;
                Content _ret = contents[0];
                return _ret;
            }


            //------------------------------------------------------------------------------
            public Content[] GetContents()
            {
                if ( contents == null ) return null;
                Content[] _ret = contents;
                return _ret;
            }


            //------------------------------------------------------------------------------
            // ランダムにAudioClipを取得.
            public AudioClip GetClip()
            {
                if ( contents == null ) return null;
                if ( contents.Length < 1 ) return contents[0].Clip;

                int _maxWeight = 100;
                int _index = 0;
                while ( 0 < _maxWeight )
                {
                    _index = Random.Range(0, contents.Length);
                    _maxWeight -= contents[_index].Weight;
                }
                return contents[_index].Clip;
            }

        }
        #endregion --- CoundContainer --------------------------------------------------

        [SerializeField] SoundContainer skillVoice;
        public SoundContainer SkillVoice { get { return skillVoice; } }

        [SerializeField] SoundContainer damageVoice;
        public SoundContainer DamageVoice { get { return damageVoice; } }

        [SerializeField] SoundContainer victoryVoice;
        public SoundContainer VictoryVoice { get { return victoryVoice; } }

        [SerializeField] SoundContainer defeatVoice;
        public SoundContainer DefeatVoice { get { return defeatVoice; } }

    }
}