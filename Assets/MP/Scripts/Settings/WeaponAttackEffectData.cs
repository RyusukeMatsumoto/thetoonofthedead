﻿using UnityEngine;
using System;

namespace TheToonOfTheDead.Settings
{
    //==============================================================================
    //------------------------------------------------------------------------------
    // 武器の攻撃力計算用パラメータクラス.
    [System.Serializable]
    public class WeaponAttackEffectData : ScriptableObject
    {

        // 効果の種類.
        public enum Effect
        {
            NormalDamage,
            SlipDamage,
            Heal,
        }


        // 武器の名前.
        [SerializeField] string name;
        public string Name { get { return name; } }

        // 効果の種類.
        [SerializeField] Effect effectID = Effect.NormalDamage;
        public Effect EffectID { get { return effectID; } }

        // 効果の威力.
        [SerializeField] float effectPower = 0f;
        public float EffectPower { get { return effectPower; } }

        // 射程.
        [SerializeField] float range = 0f;
        public float Range { get { return range; } }

        // スリップダメージの時の効果時間.
        [SerializeField] float slipTime = 0f;
        public float SlipTime { get { return slipTime; } }

        GameObject actor; // 攻撃したキャラクタ.
        GameObject Actor
        {
            get { return actor; }
            set { if ( actor == null ) actor = value; }
        }


        //------------------------------------------------------------------------------
        // コンストラクタ.
        public WeaponAttackEffectData()
        {
            effectID = Effect.NormalDamage;
            effectPower = 0f;
            range = 0f;
            slipTime = 0f;
        }
        public WeaponAttackEffectData(
                Effect _effectId,
                float _effectPower,
                float _range,
                float _slipTime )
        {
            effectID = _effectId;
            effectPower = _effectPower;
            range = _range;
            slipTime = _slipTime;
        }


        //------------------------------------------------------------------------------
        public override string ToString()
        {
            var _sb = new System.Text.StringBuilder(256);
            _sb.AppendLine("--- WeaponAttackData ---");
            if ( Actor != null ) _sb.AppendLine($"Actor : {Actor.gameObject.ToString()}");
            _sb.AppendLine($"WeaponName : {Name}");
            _sb.AppendLine($"EffectID : {EffectID.ToString()}");
            _sb.AppendLine($"Power : {EffectPower.ToString()}");
            _sb.AppendLine($"Range : {Range.ToString()}");
            _sb.AppendLine($"SlipTime : {SlipTime.ToString()}");
            _sb.AppendLine("------------------------");
            return _sb.ToString();
        }
    }
}


