﻿using UnityEngine;
using System.Collections;


using TheToonOfTheDead.Settings;
using TheToonOfTheDead.Utility;
using TheToonOfTheDead.Characters;

namespace TheToonOfTheDead.Weapon
{
    //==============================================================================
    //------------------------------------------------------------------------------
    public class Type100SubMachineGun : WeaponBase, IWeapon
    {


        AudioSource audio;


        //------------------------------------------------------------------------------
        void Start()
        {
            base.Start();
            audio = GetComponent<AudioSource>();
        }


        //------------------------------------------------------------------------------
        void Update()
        {
            base.Update();
        }


        //------------------------------------------------------------------------------
        IEnumerator ReloadSE()
        {
            audio.PlayOneShot(sePack.ReloadSE.GetClip());
            yield return new WaitUntil(() => !audio.isPlaying);
            audio.PlayOneShot(sePack.CockingSE.GetClip());
        }


        //------------------------------------------------------------------------------
        /// <summary> 残弾数の確認. </summary>
        /// <returns> 現在マガジンに入っている残弾. </returns>
        public int GetRemainingBullets()=> this.Bullets;


        //------------------------------------------------------------------------------
        /// <summary> 最大弾丸数を取得. </summary>
        /// <returns> その武器の最大弾丸数. </returns>
        public int GetMaxBullets()=> this.MaxBullets;


        //------------------------------------------------------------------------------
        /// <summary> 射撃. </summary>
        public void Fire()
        {

            // テスト、今回は弾の剛体を飛ばさないでRayで当たり判定をとる( 後で剛体を飛ばすようにする ).
            {
                RaycastHit _hit;
                WeaponAttackEffectData _attackEffect = this.GetAttackEffect();
                if ( Physics.Raycast(this.GetRay(), out _hit, _attackEffect.Range) )
                {
                    if ( _hit.transform.gameObject.tag == GameObjTagList.Zombie )
                    {
                        ICharacter _character = _hit.transform.gameObject.GetComponent<ICharacter>();
                        _character.SetDamage(_attackEffect);
                    }
                }
            }

            audio.pitch = 3f;
            audio.PlayOneShot(sePack.FireSE.GetClip());
            this.CoolTimeStopwatchStart();
            this.RemoveAmmo();
            base.Recoil();
        }


        //------------------------------------------------------------------------------
        /// <summary> リロードとクールタイム双方が完了していれば銃を放つことができる. </summary>
        /// <returns> CoolTime, ReloadTime 双方が完了しているか否か. </returns>
        public bool IsCoolTimeComplete()=> ( this.CoolTimeComplete() && this.ReloadTimeWatchComplete() );


        //------------------------------------------------------------------------------
        /// <summary> リロード. </summary>
        public void Reload()
        {
            audio.pitch = 1f;
            StartCoroutine(this.ReloadSE());
            this.ReloadStopwatchStart();
            this.ResetAmmo();
        }


        //------------------------------------------------------------------------------
        public GameObject GetGameObject()=> gameObject;


        //------------------------------------------------------------------------------
        public WeaponSEPackage GetSEPack()=> sePack;


        //------------------------------------------------------------------------------
        public T GetComponent<T>( System.Type _t) where T : class
        {
            return this.GetGameObject().GetComponent(_t) as T;
        }
        
    }
}