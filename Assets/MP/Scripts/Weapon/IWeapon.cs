﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using TheToonOfTheDead.Settings;

namespace TheToonOfTheDead.Weapon
{
    //==============================================================================
    //------------------------------------------------------------------------------
    public interface IWeapon
    {
        bool IsCoolTimeComplete();
        int GetRemainingBullets();
        int GetMaxBullets();
        void Fire();
        void Reload();
        GameObject GetGameObject();
        WeaponSEPackage GetSEPack();
        T GetComponent<T>( System.Type _t ) where T : class;
    }
}


