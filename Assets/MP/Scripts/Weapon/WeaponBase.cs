﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TheToonOfTheDead.Settings;
using TheToonOfTheDead.Utility;


namespace TheToonOfTheDead.Weapon
{
    //==============================================================================
    //------------------------------------------------------------------------------
    // すべての武器のベースクラス.
    [RequireComponent(typeof(AudioSource))]
    public class WeaponBase : MonoBehaviour
    {

        [SerializeField] bool usePlayer = false;
        [SerializeField] GameObject reticle;
        [SerializeField, Header("発射時のルート( 銃口のようなもの ).")] GameObject attackRoot; 

        [SerializeField] protected float coolTime;
        public long CoolTimeMilliSeconds { get { return (long)coolTime * 1000; } }

        [SerializeField] protected float reloadTime;
        public long ReloadTimeMilliSeconds { get { return (long)reloadTime * 1000; } }

        [SerializeField,Header("弾丸周り")] int bullets;
        public int Bullets{ get { return bullets; } } // 残りの銃弾数( Inspectorでセットする関係上このつくりに ).
        public int MaxBullets { get;  private set; }

        [SerializeField, Header("武器の攻撃パラメータ.")] WeaponAttackEffectData attackEffect;
        [SerializeField,Header("SEPackage")] protected WeaponSEPackage sePack;

        GameTimer coolTimeWatch;
        GameTimer reloadTimeWatch;

        // リコイル追加に伴うパラメータ.
        Quaternion initRotation;
        [SerializeField,Range(0,10)] float recoilPower;
        [SerializeField,Range(1,10)] float recoilReturnPower;
        [SerializeField,Header("反動で左右に触れるか否か")] bool shakeLeftRight = false;
        
        
        
        //------------------------------------------------------------------------------
        void Awake()
        {
            initRotation = transform.localRotation;
            
            coolTimeWatch = new GameTimer(coolTime);
            reloadTimeWatch = new GameTimer(reloadTime);
            coolTimeWatch.Start();
            reloadTimeWatch.Start();
            MaxBullets = bullets;
        }


        //------------------------------------------------------------------------------
        protected void Start()
        {
            if ( reticle != null ) reticle.SetActive(usePlayer);
        }


        //------------------------------------------------------------------------------
        protected void Update()
        {
            this.RecoilReturn();
        }


        //------------------------------------------------------------------------------
        void OnDestroy()
        {
            coolTimeWatch = reloadTimeWatch = null;
        }

        
        //------------------------------------------------------------------------------
        // 反動復帰処理.
        protected void RecoilReturn()
        {
            transform.localRotation = Quaternion.Lerp(transform.localRotation, initRotation, Time.deltaTime * recoilReturnPower);
        }
        
        
        //------------------------------------------------------------------------------
        // 反動処理.
        protected void Recoil()
        {
            transform.Rotate(Vector3.left, recoilPower);
            if (shakeLeftRight)
            {
                float _power = (Random.Range(0, 10) % 2 == 0) ? recoilPower : -recoilPower;
                _power *= Random.Range(0.1f, 0.5f);
                transform.Rotate(Vector3.up, _power);                
                
            }
        }
        
        
        //------------------------------------------------------------------------------
        protected Vector3 AttackDir()=> attackRoot.transform.TransformDirection(Vector3.forward);


        //------------------------------------------------------------------------------
        protected Vector3 AttackRootPos()=> attackRoot.transform.position;


        //------------------------------------------------------------------------------
        /// <summary> 撃方向が決まってるものはRayを取得する事で処理を簡略化する. </summary>
        /// <returns> 攻撃のRay. </returns>
        protected Ray GetRay()=>new Ray(this.AttackRootPos(), this.AttackDir());


        //------------------------------------------------------------------------------
        /// <summary> クールタイムのストップウォッチを起動. </summary>
        protected void CoolTimeStopwatchStart()
        {
            if ( !coolTimeWatch.IsComplete() ) return;
            coolTimeWatch.Start();
        }


        //------------------------------------------------------------------------------
        /// <summary> クールタイム完了したか否か判定. </summary>
        /// <returns> クールタイムが完了したか否か. </returns>
        protected bool CoolTimeComplete()=> coolTimeWatch.IsComplete();


        //------------------------------------------------------------------------------
        /// <summary> リロードのストップウォッチ起動. </summary>
        protected void ReloadStopwatchStart()
        {
            if ( !reloadTimeWatch.IsComplete() ) return;
            reloadTimeWatch.Start();
        }


        //------------------------------------------------------------------------------
        /// <summary> リロードが完了したか否か. </summary>
        /// <returns> リロードが完了したか否か. </returns>
        protected bool ReloadTimeWatchComplete()=> reloadTimeWatch.IsComplete();


        //------------------------------------------------------------------------------
        /// <summary>残弾のリセット. </summary>
        protected void ResetAmmo()=> bullets = MaxBullets;


        //------------------------------------------------------------------------------
        /// <summary> 弾の加算( ショットガンとかだと一発ずつリロードするとか? ) </summary>
        /// <param name="_num"> 補充する弾数. </param>
        protected void AddAmmo(int _num = 1)
        {
            _num = Mathf.Abs(_num);
            bullets = ( MaxBullets <= bullets + _num ) ? MaxBullets : bullets + _num;
        }


        //------------------------------------------------------------------------------
        /// <summary> 弾を減算 </summary>
        /// <param name="_num"> 減算する弾数. </param>
        protected void RemoveAmmo(int _num = 1)
        {
            _num = Mathf.Abs(_num);
            bullets = ( bullets - _num <= 0 ) ? 0 : bullets - _num;
        }


        //------------------------------------------------------------------------------
        /// <summary> 武器の攻撃パラメータのデータを取得. </summary>
        /// <returns> 攻撃パラメータデータ. </returns>
        public WeaponAttackEffectData GetAttackEffect()=> attackEffect;

    }

}


