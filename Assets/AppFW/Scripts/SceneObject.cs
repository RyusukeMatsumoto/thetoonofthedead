﻿using System;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif



namespace AppFW
{
    //==============================================================================
    //------------------------------------------------------------------------------
    // シーンをInspectorでオブジェクトの様に扱うための拡張.
    [System.Serializable]
    public class SceneObject
    {
        [SerializeField] string sceneName;

        //------------------------------------------------------------------------------
        public static implicit operator string( SceneObject _sceneObject )=> _sceneObject.sceneName;


        //------------------------------------------------------------------------------
        public static implicit operator SceneObject( string _sceneName )=> new SceneObject(){sceneName = _sceneName};


        //------------------------------------------------------------------------------
        public override string ToString()=> sceneName;

    }


#if UNITY_EDITOR
    //==============================================================================
    //------------------------------------------------------------------------------
    [CustomPropertyDrawer(typeof(SceneObject))]
    public class SceneObjectEditor : PropertyDrawer
    {

        static readonly string SceneNameProperty = "sceneName";


        //------------------------------------------------------------------------------
        // シーンオブジェクトを取得.
        protected SceneAsset GetSceneObject(
                string _sceneObjectName )
        {
            if ( string.IsNullOrEmpty(_sceneObjectName) ) return null;

            foreach ( var _scene in EditorBuildSettings.scenes )
                if ( _scene.path.IndexOf(_sceneObjectName) != -1 )
                    return AssetDatabase.LoadAssetAtPath(_scene.path, typeof(SceneAsset)) as SceneAsset;

            Debug.LogError($"Scene [ {_sceneObjectName} ] can not used. Add this scene to 'Scenes in the Build' in the build settings.");
            return null;
        }


        //------------------------------------------------------------------------------
        public override void OnGUI( 
                Rect _position,
                SerializedProperty _property,
                GUIContent _label )
        {
            SceneAsset _sceneObj = this.GetSceneObject(_property.FindPropertyRelative(SceneNameProperty).stringValue);
            SceneAsset _newScene = EditorGUI.ObjectField(_position, _label, _sceneObj, typeof(SceneAsset), false) as SceneAsset;
            if ( _newScene == null )
            {
                var _prop = _property.FindPropertyRelative(SceneNameProperty);
                _prop.stringValue = "";
            }
            else
            {
                if ( _newScene.name != _property.FindPropertyRelative(SceneNameProperty).stringValue )
                {
                    SceneAsset _temp = this.GetSceneObject(_newScene.name);
                    if ( _temp == null )
                    {
                        Debug.LogWarning($"The scene {_newScene.name} cannot be used. To use this scene add it to the build settings for the project.");
                    }
                    else
                    {
                        var _prop = _property.FindPropertyRelative(SceneNameProperty);
                        _prop.stringValue = _newScene.name;
                    }
                }
            }
        }


    }
#endif

}
