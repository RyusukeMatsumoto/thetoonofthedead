﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AppFW
{

    //==============================================================================
    //------------------------------------------------------------------------------
    // SceneViewに表示する座標の目印.
    // 実行形式の側にはギズモ描画処理は不要なので含めない.
    public class PositionMarker : MonoBehaviour
    {


        // 描画するギズモの形指定.
        // 最低限の基本形のみ.
        public enum GizmoID : byte
        {
            None = 0x00,
            Sphere,
            Cube,
            WireSphere,
            WireCube,
        }

        [SerializeField] bool isThisPosition = true;
        [SerializeField] bool isDraw = false;
        [SerializeField] GizmoID gizmoID = GizmoID.None;
	    [SerializeField] Color gizmoColor = Color.white;
        [SerializeField] float gizmoScale = 1.0f;
        [SerializeField] Vector3[] gizmoPositions = null;


#if UNITY_EDITOR
        //------------------------------------------------------------------------------
        void OnDrawGizmos()
        {
            if ( !isDraw ) return;

            if ( isThisPosition )
                this.SetMarker( transform.position, gizmoScale, gizmoColor, gizmoID );

            if ( gizmoPositions == null || gizmoPositions.Length < 0 )
                return;
            this.SetMarkers( gizmoPositions, gizmoScale, gizmoColor, gizmoID );
        }
#endif

        //------------------------------------------------------------------------------
        /// <summary>
        /// 指定した座標へギズモを描画( 基本形のみ ).
        /// </summary>
        /// <param name="_pos"> 座標. </param>
        /// <param name="_scale"> スケール. </param>
        /// <param name="_color"> 色. </param>
        /// <param name="_id"> ギズモID. </param>
        public void SetMarker(
                Vector3 _pos,
                float _scale,
                Color _color,
                GizmoID _id )
        {
#if UNITY_EDITOR

            Gizmos.color = _color;
            switch ( _id )
            {
            case GizmoID.Sphere: Gizmos.DrawSphere( _pos, _scale ); break;
            case GizmoID.Cube: Gizmos.DrawCube( _pos, Vector3.one * _scale ); break;
            case GizmoID.WireSphere: Gizmos.DrawWireSphere( _pos, _scale ); break;
            case GizmoID.WireCube: Gizmos.DrawWireCube( _pos, Vector3.one * _scale ); break;
            case GizmoID.None: Debug.LogWarning("GizmoIDが指定されていません"); break;
            }
            Gizmos.color = Color.white;
#endif
        }


        //------------------------------------------------------------------------------
        /// <summary>
        /// 指定した座標リストへギズモを描画( 基本形のみ ).
        /// </summary>
        /// <param name="_posList"> 座標リスト. </param>
        /// <param name="_scale"> スケール. </param>
        /// <param name="_color"> 色. </param>
        /// <param name="_id"> ギズモID. </param>
        public void SetMarkers(
                Vector3[] _posList,
                float _scale,
                Color _color,
                GizmoID _id )
        {
#if UNITY_EDITOR
            if ( _posList == null ) return;
            for ( int i = 0; i < _posList.Length; ++i )
            {
                this.SetMarker( _posList[i], _scale, _color, _id );
            }
#endif
        }
    }




}