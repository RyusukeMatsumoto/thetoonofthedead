﻿using UnityEngine;
using System.Collections;
using AppFW;


namespace AppFW
{
	// サウンドマネージャ以外からは見られたくないので二重に名前空間をかける.
	namespace SoundManager
    {
		//==============================================================================
		//------------------------------------------------------------------------------
		// SE再生用スピーカー.
		public class SESpeaker : MonoBehaviour
        {
			
			// ボリューム.
			public float Volume { get { return source.volume; } }

			// 使用しているオーディオクリップ.
			public AudioClip Clip { get { return source.clip; } }


			[SerializeField] AudioSource source = null;


			//------------------------------------------------------------------------------
			void Start()
            {
				if ( !source )
					source = GetComponent<AudioSource>();
				source.clip = null;
			}
	

			//------------------------------------------------------------------------------
			// 効果音の再生.
			public void PlaySE(
					AudioClip _clip,
					float _volume = 1.0f )
            {
				source.PlayOneShot( _clip, _volume );
			}


			//------------------------------------------------------------------------------
			// 一時停止.
			public void Pause()
            {
				source.Pause();
			}


			//------------------------------------------------------------------------------
			// 一時停止を終了.
			public void UnPause()
            {
				source.UnPause();
			}


		}
	}
}