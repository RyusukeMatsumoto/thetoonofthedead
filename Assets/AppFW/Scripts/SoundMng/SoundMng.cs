﻿#pragma warning disable 0618, 0414
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using AppFW;
using AppFW.SoundManager;


namespace AppFW
{

	//==============================================================================
	//------------------------------------------------------------------------------
	// サウンドマネージャ.
	// 3D音源はどのみちシーンに直接配置なので2D音源のみのサポートとする.
	public class SoundMng : MonoBehaviour
    {


        static SoundMng instance_;
        static SoundMng SI_
        {
            get {
                if ( !instance_ )
                {
                    instance_ = FindObjectOfType( typeof(SoundMng) ) as SoundMng;
                    if ( !instance_ )
                        Debug.LogError("SoundMngをアタッチしているGameObjectが存在しないので生成するかしてください");
                }
                return instance_;
            }
        }



		// サウンド識別子.
		public enum SoundID
        {
			BGM = 0,
			SE,
			Voice
		}


        readonly int MaxBGMSpeaker = 2; 	// BGMスピーカの最大数( 2固定 ).


		[SerializeField] Transform bgmRoot = null;		// BGMスピーカーのルートオブジェクト.
		[SerializeField] Transform seRoot = null;		// SEスピーカーのルートオブジェクト.
		[SerializeField] Transform voiceRoot = null;	// VOICEスピーカーのルートオブジェクト.
		[SerializeField] BGMSpeaker[] bgm = null;		// BGM用スピーカー.
		[SerializeField] SESpeaker[] se = null;			// 一つでよさそうだが今後もしかしたら複数必要になるかもしれないので一応配列.
		[SerializeField] VoiceSpeaker[] voice = null;	// 具体的な方針が決まっていないので↑のSEと同じ方式で再生しておく.
		
		int curBgmSpeaker = 0;  // 現在使用しているBGMスピーカー.

		// 各サウンドテーブル.
		Dictionary<string, AudioClip> bgmTable = null;
		Dictionary<string, AudioClip> seTable = null;
		Dictionary<string, AudioClip> voiceTable = null;



		//------------------------------------------------------------------------------
		void Awake()
        {
            if ( this != SI_ )
            {
                Destroy( gameObject );
                return;
            }

            GameObject.DontDestroyOnLoad( gameObject );
            bgm = null;
			if ( bgm == null )
            {
				bgm = new BGMSpeaker[MaxBGMSpeaker];
				for ( int i = 0; i < bgmRoot.transform.childCount; ++i )
                {
					if ( i < MaxBGMSpeaker )
						bgm[i] = bgmRoot.transform.GetChild(i).GetComponent<BGMSpeaker>();
				}
			}

            se = null;
            if ( se == null )
            {
                se = new SESpeaker[1];
                for ( int i = 0; i < seRoot.transform.childCount; ++i )
                {
                    if ( i < 1 )
                        se[i] = seRoot.transform.GetChild(i).GetComponent<SESpeaker>();
                }
            }

			seTable = new Dictionary< string, AudioClip >();
			bgmTable = new Dictionary< string, AudioClip >();
			voiceTable = new Dictionary< string, AudioClip >();
		}


        #region --- StaticMethods ------------------------------------------------------



        //------------------------------------------------------------------------------
        /// <summary> インスタンスの取得( 稀にこれを使う場合がある ). </summary>
        /// <returns> SoundMngのインスタンス. </returns>
        public static SoundMng GetInstance()=> SI_;


        //------------------------------------------------------------------------------
        /// <summary> GameObjectのTransform取得. </summary>
        /// <returns> SoundMngがアタッチされているGameObjectのTransform. </returns>
        public static Transform GetTransform()=> SI_.transform;


        //------------------------------------------------------------------------------
        /// <summary>
        /// テーブルにAudioClipを追加.
        /// </summary>
        /// <param name="_clip"> AudioClip. </param>
        /// <param name="_key"> キー値. </param>
        /// <param name="id"> サウンドID. </param>
        public static void AddTable(
				AudioClip _clip,
				string _key,
				SoundID _id )
        {
			switch (_id)
            {
				case SoundID.BGM: SI_.bgmTable.Add( _key, _clip ); break;
				case SoundID.SE: SI_.seTable.Add( _key, _clip ); break;
				case SoundID.Voice: SI_.voiceTable.Add( _key, _clip ); break;
				default: Debug.LogError("無効なIDです."); break;
			}
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// 指定したキーのサウンドをテーブルから削除.
		/// </summary>
		/// <param name="_key"> key値. </param>
		/// <param name="id"> サウンドID. </param>
		public static void ClearKey(
				string _key,
				SoundID _id )
        {
			switch (_id)
            {
				case SoundID.BGM: SI_.bgmTable.Remove(_key); break;
				case SoundID.SE: SI_.seTable.Remove(_key); break;
				case SoundID.Voice: SI_.voiceTable.Remove(_key); break;
				default: Debug.LogError("無効なIDです."); break;
			}
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// 指定したテーブルをクリア.
		/// </summary>
		/// <param name="_id"> サウンドID. </param>
		public static void ClearTable(
				SoundID _id )
        {
			switch (_id)
            {
				case SoundID.BGM: SI_.bgmTable.Clear(); break;
				case SoundID.SE: SI_.seTable.Clear(); break;
				case SoundID.Voice: SI_.voiceTable.Clear(); break;
				default: Debug.LogError("無効なIDです."); break;
			}
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// BGM,SE,Voiceを一時停止.
		/// </summary>
		public static void AllPause()
        {
			SI_.bgm[SI_.curBgmSpeaker].Pause();
			int ncIndex = ( SI_.curBgmSpeaker == 0 ) ? 0 : 1;
			if ( SI_.bgm[ncIndex].Clip != null ) SI_.bgm[ncIndex].Pause();
			
			SI_.se[0].Pause();
			SI_.voice[0].Pause();
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// BGM,SE,Voiceを一時停止を終了.
		/// </summary>
		public static void AllUpPause()
        {
			SI_.bgm[SI_.curBgmSpeaker].UnPause();
			int ncIndex = ( SI_.curBgmSpeaker == 0 ) ? 0 : 1;
			if ( SI_.bgm[ncIndex].Clip != null ) SI_.bgm[ncIndex].UnPause();
			
			SI_.se[0].UnPause();
			SI_.voice[0].UnPause();
		}



		#region BGM
		//------------------------------------------------------------------------------
		// BGMの再生.
		public static void PlayBGM(
				AudioClip _clip,
				bool _isLoop )
        {
			SI_.bgm[SI_.curBgmSpeaker].PlayBGM( _clip, _isLoop );
		}
		public static void PlayBGM(
				string _key,
				bool _isLoop )
        {
			if ( !SI_.bgmTable.ContainsKey(_key) )
            {
				Debug.LogError("指定したキーはテーブルに登録されていません.");
				return;
			}
			SI_.bgm[SI_.curBgmSpeaker].PlayBGM(SI_.bgmTable[_key], _isLoop);
		}


		//------------------------------------------------------------------------------
		// BGMの停止.
		public static void StopBGM()
        {
			SI_.bgm[SI_.curBgmSpeaker].StopBGM();
		}


		//------------------------------------------------------------------------------
		// BGMの一時停止.
		public static void PauseBGM()
        {
			SI_.bgm[SI_.curBgmSpeaker].Pause();
		}


		//------------------------------------------------------------------------------
		// BGM一時停止終了.
		public static void unPauseBGM()
        {
			SI_.bgm[SI_.curBgmSpeaker].UnPause();
		}


		//------------------------------------------------------------------------------
		// BGMのフェードイン再生.
		public void FadeinBGM(
				AudioClip _clip,
				bool _isLoop,
				float _fadeTime = 1.0f,
				float _fromVolume = 0.0f,
				float _toVolume = 1.0f )
        {
			bgm[curBgmSpeaker].PlayFadein( _clip, _isLoop, _fadeTime, _fromVolume, _toVolume );
		}
		public static void FadeinBGM(
				string _key,
				bool _isLoop,
				float _fadeTime = 1.0f,
				float _fromVolume = 0.0f,
				float _toVolume = 1.0f )
        {
			if ( !SI_.bgmTable.ContainsKey(_key) )
            {
				Debug.LogError($"指定したキーはテーブルに登録されていません : {_key}");
				return;
			}
			SI_.bgm[SI_.curBgmSpeaker].PlayFadein(SI_.bgmTable[_key], _isLoop, _fadeTime, _fromVolume, _toVolume);
		}


		//------------------------------------------------------------------------------
		// BGM1のフェードアウト.
		public static void FadeoutBGM(
				float _fadeTime = 1.0f )
        {
			SI_.bgm[SI_.curBgmSpeaker].Fadeout(_fadeTime);
		}


		//------------------------------------------------------------------------------
		// BGMのクロスフェード.
		// ボリュームの値はフェード後BGMのみに適用.
		public static void CrossFadeBGM(
				AudioClip _clip,
				bool _isLoop = true,
				float _fadeTime = 1.0f,
				float _fromVolume = 0.0f,
				float _toVolume = 1.0f )
        {
			SI_.bgm[SI_.curBgmSpeaker].Fadeout(_fadeTime, true);
			SI_.SwitchCurBgmSpeaker();
			SI_.bgm[SI_.curBgmSpeaker].PlayFadein(_clip, _isLoop, _fadeTime, _fromVolume, _toVolume);
		}
		public static void CrossFadeBGM(
				string _key,
				bool _isLoop = true,
				float _fadeTime = 1.0f,
				float _fromVolume = 0.0f,
				float _toVolume = 1.0f )
        {
			if ( !SI_.bgmTable.ContainsKey(_key) )
            {
				Debug.LogError($"指定したキーはテーブルに登録されていません : {_key}");
				return;
			}
			SI_.bgm[SI_.curBgmSpeaker].Fadeout(_fadeTime, true);
			SI_.SwitchCurBgmSpeaker();
			SI_.bgm[SI_.curBgmSpeaker].PlayFadein(SI_.bgmTable[_key], _isLoop, _fadeTime, _fromVolume, _toVolume);
		}
				

		//------------------------------------------------------------------------------
		// 現在使用しているBGMスピーカーのインデックス番号を切り替え.
		void SwitchCurBgmSpeaker()
        {
			curBgmSpeaker = ( curBgmSpeaker == 0 ) ? 1 : 0;
		}

				
		#endregion


		#region SE
		//------------------------------------------------------------------------------
		// 効果音の再生.
		public static void PlaySE(
				AudioClip _clip,
				float _volume = 1.0f )
        {
			SI_.se[0].PlaySE( _clip, _volume );
		}
		public static void PlaySE(
				string _key,
				float _volume = 1.0f )
        {
			if ( !SI_.seTable.ContainsKey(_key) )
            {
				Debug.LogError($"指定したキーがテーブルに登録されていません : {_key}" );
				return;
			}
			SI_.se[0].PlaySE(SI_.seTable[_key], _volume);
		}

		#endregion
		

		#region Voice
		//------------------------------------------------------------------------------
		// ボイスの再生.
		public static void PlayVoice(
				AudioClip _clip,
				float _volume = 1.0f )
        {
			SI_.voice[0].PlayVoice(_clip, _volume);
		}
		public static void PlayVoice(
				string _key,
				float _volume = 1.0f )
        {
			if ( !SI_.voiceTable.ContainsKey(_key) )
            {
				Debug.LogError($"指定したキーがテーブルに登録されていません : {_key}" );
				return;
			}
			SI_.voice[0].PlayVoice(SI_.seTable[_key], _volume);
		}

		#endregion

        #endregion --- StaticMethods ---------------------------------------------------
	}
}