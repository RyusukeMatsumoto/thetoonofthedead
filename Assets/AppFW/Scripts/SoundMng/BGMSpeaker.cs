﻿using UnityEngine;
using System.Collections;
using System;
using AppFW;

namespace AppFW
{
	// サウンドマネージャ以外から見られたくないので二重に名前空間をかける.
	namespace SoundManager
    {
	
		//==============================================================================
		//------------------------------------------------------------------------------
		// BGM再生用.
		public class BGMSpeaker : MonoBehaviour
        {
			// ボリューム.
			public float Volume { get { return source.volume; } }

			// 使用しているオーディオクリップ.
			public AudioClip Clip { get { return source.clip; } }


			[SerializeField] AudioSource source = null;


			//------------------------------------------------------------------------------
			void Awake()
            {
				if ( !source )
					source = GetComponent<AudioSource>();
			}


			//------------------------------------------------------------------------------
			void Start()
            {
				source.clip = null;
				source.playOnAwake = false;
				source.volume = 1.0f;
			}
			

			//------------------------------------------------------------------------------
			// BGMをスタート.
			public void PlayBGM(
					AudioClip _clip,
					bool _isLoop )
            {
				source.clip = _clip;
				source.loop = _isLoop;
				source.Play();
			}


			//------------------------------------------------------------------------------
			// BGMのストップ.
			// ストップ完了後AudioClipは破棄する.
			public void StopBGM()
            {
				source.Stop();
				source.clip = null;
				source.volume = 0.0f;
			}


			//------------------------------------------------------------------------------
			// 一時停止.
			public void Pause()
            {
				source.Pause();
			}


			//------------------------------------------------------------------------------
			// 一時停止を終了.
			public void UnPause()
            {
				source.UnPause();
			}


			//------------------------------------------------------------------------------
			// BGMをフェードイン.
			public void PlayFadein(
					AudioClip _clip,
					bool _isLoop,
					float _fadeTime = 1.0f,
					float _fromVolume = 0.0f,
					float _toVolume = 1.0f )
            {
				source.clip = _clip;
				source.loop = _isLoop;
				source.volume = 1.0f;
				source.Play();
				StartCoroutine( this.fadeVolumeCulc( source, _fadeTime, _fromVolume, _toVolume ) );
			}


			//------------------------------------------------------------------------------
			// BGMをフェードアウト.
			public void Fadeout(
				float _fadeTime = 1.0f,
				bool _stopBGM = true )
            {
				if ( _stopBGM )
					StartCoroutine( this.fadeVolumeCulc( source, _fadeTime, source.volume, 0.0f, this.StopBGM ) );
				else
					StartCoroutine( this.fadeVolumeCulc( source, _fadeTime, source.volume, 0.0f, null ) );
			}


			//------------------------------------------------------------------------------
			// フェードのボリューム計算.
			IEnumerator fadeVolumeCulc(
					AudioSource _src,
					float _time,
					float _fromVolume,
					float _toVolume,
					Action _func = null )
            {
				float _ct = 0.0f;
				while (true)
                {
					_src.volume = Mathf.Lerp( _fromVolume, _toVolume, ( _ct / _time ) );
					_ct += Time.deltaTime;
					if ( 1.0f <= _ct / _time )
						break;

					yield return new WaitForSeconds(0);
				}
				_src.volume = _toVolume;

				if ( _func != null )
					_func();

				yield return null;
			}


		}
	}
}

