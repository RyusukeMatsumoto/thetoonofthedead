﻿using UnityEngine;
using UnityEngine.Events;
using System;


namespace AppFW
{


	//==============================================================================
	//------------------------------------------------------------------------------
	// 登録するイベント.
	[System.Serializable] public class CollisionSenderEvent : UnityEvent<Collision> {}
	[System.Serializable] public class TriggerSenderEvent : UnityEvent<Collider> {}


	//==============================================================================
	//------------------------------------------------------------------------------
	// アタッチしたオブジェクトのあたり判定をとり,衝突したら登録したイベントを発生させる.
	public class CollisionSender : MonoBehaviour
    {


		public enum SendID : int
        {
			CollisionEnter = 0,
			CollisionStay,
			CollisionExit,
			TriggerEnter,
			TriggerStay,
			TriggerExit,
		}


		[SerializeField] CollisionSenderEvent onCollisionEnter = null;	// コライダ衝突.
		[SerializeField] CollisionSenderEvent onCollisionStay = null;	// コライダ衝突続け.
		[SerializeField] CollisionSenderEvent onCollisionExit = null;	// コライダ離れ.

		[SerializeField] TriggerSenderEvent onTriggerEnter = null;	// トリガ入り.
		[SerializeField] TriggerSenderEvent onTriggerStay = null;	// トリガ入り続け.
		[SerializeField] TriggerSenderEvent onTriggerExit = null;	// トリガ離れ.


		//------------------------------------------------------------------------------
		void Strart()
        {
			onCollisionEnter = new CollisionSenderEvent();
			onCollisionStay = new CollisionSenderEvent();
			onCollisionExit = new CollisionSenderEvent();

			onTriggerEnter = new TriggerSenderEvent();
			onTriggerStay = new TriggerSenderEvent();
			onTriggerExit = new TriggerSenderEvent();
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// コライダに衝突した際に呼ばれる.
		/// </summary>
		/// <param name="_col"></param>
		void OnCollisionEnter( 
				Collision _col )
        {
			if ( onCollisionEnter != null ) onCollisionEnter.Invoke(_col);
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// コライダに衝突し続けている間呼ばれる.
		/// </summary>
		/// <param name="_col"></param>
		void OnCollisionStay(
				Collision _col )
        {
			if ( onCollisionStay != null ) onCollisionStay.Invoke(_col);
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// コライダから衝突判定が外れた際に呼ばれる.
		/// </summary>
		/// <param name="_col"></param>
		void OnCollisionExit( 
				Collision _col )
        {
			if ( onCollisionExit != null ) onCollisionExit.Invoke(_col);

		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// トリガに衝突した際に呼ばれる.
		/// </summary>
		/// <param name="_col"></param>
		void OnTriggerEnter( 
				Collider _col )
        {
			if ( onTriggerEnter != null ) onTriggerEnter.Invoke(_col);
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// トリガに衝突し続けているときに呼ばれる.
		/// </summary>
		/// <param name="_col"></param>
		void OnTriggerStay( 
				Collider _col )
        {
			if ( onTriggerStay != null ) onTriggerStay.Invoke(_col);
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// トリガから離れた際に呼ばれる.
		/// </summary>
		/// <param name="_col"></param>
		void OnTriggerExit(
				Collider _col )
        {
			if ( onTriggerExit != null ) onTriggerExit.Invoke(_col);
		}


        //------------------------------------------------------------------------------
        /// <summary>
        /// リスナをセット.
        /// </summary>
        /// <param name="_id"></param>
        /// <param name="_listener"></param>
        public void SetListener(
				SendID _id,
				UnityAction<Collision> _listener )
        {
			switch ( _id )
            {
				case SendID.CollisionEnter: onCollisionEnter.AddListener(_listener); break;
				case SendID.CollisionStay: onCollisionStay.AddListener(_listener); break;
				case SendID.CollisionExit: onCollisionExit.AddListener(_listener); break;
			}
		}
		public void SetListener(
				SendID _id,
				UnityAction<Collider> _listener )
        {
			switch ( _id )
            {
				case SendID.TriggerEnter: onTriggerEnter.AddListener(_listener); break;
				case SendID.TriggerStay: onTriggerStay.AddListener(_listener); break;
				case SendID.TriggerExit: onTriggerExit.AddListener(_listener); break;
			}
		}

	}
}
