﻿using UnityEngine;
using System.Collections;
using AppFW;


namespace AppFW
{

	//==============================================================================
	//------------------------------------------------------------------------------
	// リソースパスの定義データクラス.
	// フレームワーク周りのオブジェクト等のパスをここに定義している.
	public class ResourcePath
    {


		/// <summary>
		/// PathIDとpathArrayは連動しているので順番を変更する時は要注意.
		/// フォルダの場所を変更したりした場合はpathArrayのResources以下のパスを編集しないと正常に動作しない.
		/// パスはResourcesフォルダ以下からのパス.
		/// </summary>
		public enum PathID : int
        {
			PhotonMngPF = 0,

			#if USE_PHOTON
			MultiPlayerConfigFile,
			#endif

			SoundMngPF,
			BGMSpeakerPF,
		};
		static string[] pathArray = {

			#if USE_PHOTON
			Application.dataPath + "/AppFW_Photon/Resources/MultiPlayer/PhotonMng/PhotonMngPF",
			#endif

			#if USE_PHOTON && UNITY_EDITOR
			Application.dataPath + "/AppFW_Photon/Resources/MultiPlayer/Config/ConnectConfig.json",
			#elif USE_PHOTON
			Application.dataPath + "/ConnectConfig.json",
			#endif

			Application.dataPath + "/AppFW/Resources/Prefabs/SoundMng/SoundMngPF",
			Application.dataPath + "/AppFW/Resources/Prefabs/SoundMng/BGMSpeakerPF",
		};


		//------------------------------------------------------------------------------
		/// <summary>
		/// getPath
		/// PathIDを指定することでそのオブジェクトのパスを取得することが可能.
		/// </summary>
		/// <param name="_id"> パスID. </param>
		/// <returns> 指定したオブジェクトのパス. </returns>
		static public string GetPath(
				PathID _id )
        {
			int idNum = ( int )_id;
			if (	pathArray.Length < idNum ||
					idNum < 0 )
            {
				Debug.LogError("定義されていないIDです.");
				return string.Empty;
			}
			return pathArray[idNum];
		}

	}
}

