﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using AppFW;

namespace AppFW
{

	//==============================================================================
	//------------------------------------------------------------------------------
	// ゲームオブジェクト生成周りのUtility.
	static public class GOUtil
    {

        static readonly string ResourcesFolder = "Resources/";

		//------------------------------------------------------------------------------
		/// <summary>
		/// ゲームオブジェクトの生成.
		/// </summary>
		/// <param name="_path"> Resources以下のパス.</param>
		/// <param name="_name"> 名前. </param>
		/// <param name="_parent"> 親オブジェクト. </param>
		/// <returns></returns>
		static public GameObject CreateGO(
				string _path,
				string _name = "",
				Transform _parent = null )
        {
			GameObject _go = GameObject.Instantiate( Resources.Load( PathAdjust(_path) ) ) as GameObject;
			_go.name = ( _name == "" ) ? _go.name : _name;
			if ( _parent != null )
				_go.transform.SetParent(_parent);
			return _go;
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// ゲームオブジェクトの生成.
		/// 指定した型のコンポーネントを同時にアタッチする.
		/// </summary>
		/// <typeparam name="TT"> Component. </typeparam>
		/// <param name="_path"> Resources以下のパス. </param>
		/// <param name="_name"> 名前. </param>
		/// <param name="_parent"> 親オブジェクト. </param>
		/// <returns></returns>
		static public GameObject CreateGO<TT>(
				string _path,
				string _name,
				Transform _parent = null ) where TT : UnityEngine.Object
        {

			GameObject _go = CreateGO( _path, _name, _parent );
			_go.AddComponent( typeof(TT) );
			return _go;
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// 指定した名前のゲームオブジェクトのリストを返す.
		/// 速度は期待できない.
		/// </summary>
		/// <param name="_obj"> 捜索オブジェクト. </param>
		/// <param name="_name"> 子オブジェクトの名前. </param>
		/// <returns> 子オブジェクトのリスト. </returns>
		static public GameObject[] GetChildren(
				GameObject _obj,
				string _name )
        {
			Queue<GameObject> _q = new Queue<GameObject>();
			SearchChildren( _obj.transform, _name, ref _q );			
			GameObject[] _list = _q.ToArray();
			_q.Clear();
			return _list;
		}


		//------------------------------------------------------------------------------
		// 上の関数で使用する,実際に子オブジェクトを検索する処理.
		static void SearchChildren(
				Transform _t,
				string _name,
				ref Queue<GameObject> _q )
        {
			if ( 0 < _t.childCount )
            {
				for ( int i = 0; i < _t.childCount; ++i )
                {
					SearchChildren( _t.GetChild(i), _name, ref _q );
				}
			}
            else
            {
				if ( _t.name.Contains( _name ) )
					_q.Enqueue(_t.gameObject);
			}
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// 子オブジェクトから指定したコンポーネントを保持しているものを取り出す.
		/// 速度に期待はできない.
		/// </summary>
		/// <typeparam name="TT"> コンポーネント型. </typeparam>
		/// <param name="_obj"> 捜索するオブジェクト. </param>
		/// <returns> コンポーネント参照リスト. </returns>
		static public TT[] GetChildrenComponent<TT>(
				GameObject _obj ) where TT : UnityEngine.Object
        {
			Queue<TT> _q = new Queue<TT>();
			SearchChildrenComponent<TT>( _obj.transform, ref _q );
			TT[] _list = _q.ToArray();
			_q.Clear();
			return _list;
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// 指定したカメラにゲームオブジェクトが移っているか.
		/// </summary>
		/// <param name="_cam"></param>
		/// <param name="_obj"></param>
		/// <returns></returns>
		static public bool IsInView(
				Camera _cam,
				GameObject _obj )
        {
			Matrix4x4 _v = _cam.worldToCameraMatrix;
			Matrix4x4 _p = _cam.projectionMatrix;
			Matrix4x4 _vp = _p * _v;

			Vector3 _objPos = _obj.transform.position;
			Vector4 _pos = _vp * new Vector4( _objPos.x, _objPos.y, _objPos.z, 1.0f );
			if ( _pos.w == 0 )
				return true;

			_pos.x /= _pos.w;
			_pos.y /= _pos.w;
			_pos.z /= _pos.w;
			if ( _pos.x < -1.0f || 1.0f < _pos.x ) return false;
			if ( _pos.y < -1.0f || 1.0f < _pos.y ) return false;
			if ( _pos.z < -1.0f || 1.0f < _pos.z ) return false;

			return true;
		}


		//------------------------------------------------------------------------------
		// 上の関数で使用する,実際に子オブジェクトのコンポーネントを検索する処理.
		static void SearchChildrenComponent<TT>(
				Transform _t,
				ref Queue<TT> _q ) where TT : UnityEngine.Object
        {
			if ( 0 < _t.childCount )
            {
				for ( int i = 0; i < _t.childCount; ++i )
                {
					SearchChildrenComponent<TT>( _t.GetChild(i), ref _q );
				}

			}
            else
            {
				TT _tt = _t.gameObject.GetComponent( typeof(TT) ) as TT;
				if ( _tt != null )
					_q.Enqueue(_tt);
			}
		}


		//------------------------------------------------------------------------------
		// 指定されたパスがフルパスだったりした場合オブジェクト生成に失敗する.
		// 此の関数でパスを Resources 以下のフォルダからの相対パスにする.
		// Resources/ が見つからない場合はすでにResourcesからの相対パスと仮定してパスを調整.
		static string PathAdjust( 
				string _srcPath )
        {
			int _index = _srcPath.LastIndexOf(ResourcesFolder);
			if ( _index != -1 )
            {
				_srcPath = _srcPath.Substring( _index, _srcPath.Length - _index );
				_srcPath = _srcPath.Replace( ResourcesFolder, "" );
			}
            else
            {
				if ( _srcPath.Substring( 0, 1 ) == "/" )
					_srcPath = _srcPath.Substring( 1, _srcPath.Length - 1 );
			}
			return _srcPath;
		}
        

	}
}








