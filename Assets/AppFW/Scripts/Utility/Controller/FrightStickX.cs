﻿using UnityEngine;
using System.Collections;
using AppFW;

namespace AppFW {



	//------------------------------------------------------------------------------
	//==============================================================================
	// FrightStickXのラッピングクラス.
	// このフライトスティックにはボタンのところに番号が振ってあるのでその番号のボタン名で記述している.
	static public class FrightStickX{

		// キーが割り当てられていない箇所.
		// PanoramicViewスティック, 2ボタンの左隣の奴.
		// Increase/reduce throttle, スティック左下のスロットルバー.
		// MAPPINGボタン.
		// RESETボタン.



		// ボタン1.
		static public bool Button1_Down { get { return Input.GetKeyDown( KeyCode.JoystickButton0 ); } }
		static public bool Button1		{ get { return Input.GetKey( KeyCode.JoystickButton0 ); } }
		static public bool Button1_Up	{ get { return Input.GetKeyUp( KeyCode.JoystickButton0 ); } }

		// ボタン2.
		static public bool Button2_Down { get { return Input.GetKeyDown( KeyCode.JoystickButton1 ); } }
		static public bool Button2		{ get { return Input.GetKey( KeyCode.JoystickButton1 ); } }
		static public bool Button2_Up	{ get { return Input.GetKeyUp( KeyCode.JoystickButton1 ); } }

		// ボタン3.
		static public bool Button3_Down { get { return Input.GetKeyDown( KeyCode.JoystickButton2 ); } }
		static public bool Button3		{ get { return Input.GetKey( KeyCode.JoystickButton2 ); } }
		static public bool Button3_Up	{ get { return Input.GetKeyUp( KeyCode.JoystickButton2 ); } }

		// ボタン4.
		static public bool Button4_Down { get { return Input.GetKeyDown( KeyCode.JoystickButton3 ); } }
		static public bool Button4		{ get { return Input.GetKey( KeyCode.JoystickButton3 ); } }
		static public bool Button4_Up	{ get { return Input.GetKeyUp( KeyCode.JoystickButton3 ); } }

		// ボタン5.
		static public bool Button5_Down { get { return Input.GetKeyDown( KeyCode.JoystickButton4 ); } }
		static public bool Button5		{ get { return Input.GetKey( KeyCode.JoystickButton4 ); } }
		static public bool Button5_Up	{ get { return Input.GetKeyUp( KeyCode.JoystickButton4 ); } }

		// ボタン6.
		static public bool Button6_Down { get { return Input.GetKeyDown( KeyCode.JoystickButton5 ); } }
		static public bool Button6		{ get { return Input.GetKey( KeyCode.JoystickButton5 ); } }
		static public bool Button6_Up	{ get { return Input.GetKeyUp( KeyCode.JoystickButton5 ); } }

		// ボタン7.
		static public bool Button7_Down { get { return Input.GetKeyDown( KeyCode.JoystickButton6 ); } }
		static public bool Button7		{ get { return Input.GetKey( KeyCode.JoystickButton6 ); } }
		static public bool Button7_Up	{ get { return Input.GetKeyUp( KeyCode.JoystickButton6 ); } }

		// ボタン8.
		static public bool Button8_Down { get { return Input.GetKeyDown( KeyCode.JoystickButton7 ); } }
		static public bool Button8		{ get { return Input.GetKey( KeyCode.JoystickButton7 ); } }
		static public bool Button8_Up	{ get { return Input.GetKeyUp( KeyCode.JoystickButton7 ); } }

		// ボタン9.
		static public bool Button9_Down { get { return Input.GetKeyDown( KeyCode.JoystickButton8 ); } }
		static public bool Button9		{ get { return Input.GetKey( KeyCode.JoystickButton8 ); } }
		static public bool Button9_Up	{ get { return Input.GetKeyUp( KeyCode.JoystickButton8 ); } }

		// ボタン10.
		static public bool Button10_Down { get { return Input.GetKeyDown( KeyCode.JoystickButton9 ); } }
		static public bool Button10		{ get { return Input.GetKey( KeyCode.JoystickButton9 ); } }
		static public bool Button10_Up	{ get { return Input.GetKeyUp( KeyCode.JoystickButton9 ); } }

		// ボタン11.
		static public bool Button11_Down { get { return Input.GetKeyDown( KeyCode.JoystickButton10 ); } }
		static public bool Button11		{ get { return Input.GetKey( KeyCode.JoystickButton10 ); } }
		static public bool Button11_Up	{ get { return Input.GetKeyUp( KeyCode.JoystickButton10 ); } }

		// ボタン12.
		static public bool Button12_Down { get { return Input.GetKeyDown( KeyCode.JoystickButton11 ); } }
		static public bool Button12		{ get { return Input.GetKey( KeyCode.JoystickButton11 ); } }
		static public bool Button12_Up	{ get { return Input.GetKeyUp( KeyCode.JoystickButton11 ); } }

		// スティック水平.
		static public float Stick_H { get { return Input.GetAxis( "Horizontal" ); } }

		// スティック垂直.
		static public float Stick_V { get { return Input.GetAxis( "Vertical" ); } }


	}

	
}
