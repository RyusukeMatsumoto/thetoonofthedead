﻿using UnityEngine;
using System.Collections;

using AppFW;

namespace AppFW {

	//------------------------------------------------------------------------------
	//==============================================================================
	/// BUFFALO の ClassicUSBGamePadのキーバインド. 
	/// 今のところ TURBO と CLEAR ボタンのIDがわからない.
	static public class ClassicUSBGamePad {

		// Aボタン.
		static public bool Button_A_Down	{ get { return Input.GetKeyDown( KeyCode.JoystickButton0 ); } }
		static public bool Button_A			{ get { return Input.GetKey( KeyCode.JoystickButton0 ); } }
		static public bool Button_A_Up		{ get { return Input.GetKeyDown( KeyCode.JoystickButton0 ); } }

		// Bボタン.
		static public bool Button_B_Down	{ get { return Input.GetKeyDown( KeyCode.JoystickButton1 ); } }
		static public bool Button_B			{ get { return Input.GetKey( KeyCode.JoystickButton1 ); } }
		static public bool Button_B_Up		{ get { return Input.GetKeyDown( KeyCode.JoystickButton1 ); } }
		
		// Xボタン.
		static public bool Button_X_Down	{ get { return Input.GetKeyDown( KeyCode.JoystickButton2 ); } }
		static public bool Button_X			{ get { return Input.GetKey( KeyCode.JoystickButton2 ); } }
		static public bool Button_X_Up		{ get { return Input.GetKeyDown( KeyCode.JoystickButton2 ); } }
		
		// Yボタン.
		static public bool Button_Y_Down	{ get { return Input.GetKeyDown( KeyCode.JoystickButton3 ); } }
		static public bool Button_Y			{ get { return Input.GetKey( KeyCode.JoystickButton3 ); } }
		static public bool Button_Y_Up		{ get { return Input.GetKeyDown( KeyCode.JoystickButton3 ); } }
		
		// Lボタン.
		static public bool Button_L_Down	{ get { return Input.GetKeyDown( KeyCode.JoystickButton4 ); } }
		static public bool Button_L			{ get { return Input.GetKey( KeyCode.JoystickButton4 ); } }
		static public bool Button_L_Up		{ get { return Input.GetKeyDown( KeyCode.JoystickButton4 ); } }

		// Rボタン.
		static public bool Button_R_Down	{ get { return Input.GetKeyDown( KeyCode.JoystickButton5 ); } }
		static public bool Button_R			{ get { return Input.GetKey( KeyCode.JoystickButton5 ); } }
		static public bool Button_R_Up		{ get { return Input.GetKeyDown( KeyCode.JoystickButton5 ); } }

		// SELECTボタン.
		static public bool Button_SELECT_Down	{ get { return Input.GetKeyDown( KeyCode.JoystickButton6 ); } }
		static public bool Button_SELECT		{ get { return Input.GetKey( KeyCode.JoystickButton6 ); } }
		static public bool Button_SELECT_Up		{ get { return Input.GetKeyDown( KeyCode.JoystickButton6 ); } }
		
		// STARTボタン.
		static public bool Button_START_Down	{ get { return Input.GetKeyDown( KeyCode.JoystickButton7 ); } }
		static public bool Button_START			{ get { return Input.GetKey( KeyCode.JoystickButton7 ); } }
		static public bool Button_START_Up		{ get { return Input.GetKeyDown( KeyCode.JoystickButton7 ); } }

		// 方向キー水平( コントローラの構造上 -1 | 0 | 1 の値 ).
		static public float Horizontal { get { return Input.GetAxisRaw( "Horizontal" ); } }

		// 方向キー垂直( コントローラの構造上 -1 | 0 | 1 の値 ).
		static public float Vertical { get { return Input.GetAxisRaw( "Vertical" ); } }


	}



}