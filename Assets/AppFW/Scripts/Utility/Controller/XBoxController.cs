﻿using UnityEngine;
using System.Collections;
using AppFW;

namespace AppFW {


	//------------------------------------------------------------------------------
	//==============================================================================
	// XBoxコントローラーのキーバインド.
	// 通常のInput.GetKeyではかなりわかりづらいのでここで直観的なキー判定にする.
	// 今現在右スティックの値をとる方法が無い,InputManagerをカスタムすれば可能だがいちいち設定するのは手間,後でXboxのボタン,スティックすべてに対応できる
	// ものを作成する予定なので今はこの機能のみでの利用でお願いしたい.
	static public class XBoxController{

		// 現在未対応なもの.
		// L,Rのトリガ.
		// 右スティック.
		// 左の方向キー.




		// Aボタン.
		static public bool Button_A_Down	{ get { return Input.GetKeyDown( KeyCode.JoystickButton0 ); } }
		static public bool Button_A		{ get { return Input.GetKey( KeyCode.JoystickButton0 ); } }
		static public bool Button_A_Up		{ get { return Input.GetKeyUp( KeyCode.JoystickButton0 ); } }

		// Bボタン.
		static public bool Button_B_Down	{ get { return Input.GetKeyDown( KeyCode.JoystickButton1 ); } }
		static public bool Button_B		{ get { return Input.GetKey( KeyCode.JoystickButton1 ); } }
		static public bool Button_B_Up		{ get { return Input.GetKeyUp( KeyCode.JoystickButton1 ); } }

		// Xボタン.
		static public bool Button_X_Down	{ get { return Input.GetKeyDown( KeyCode.JoystickButton2 ); } }
		static public bool Button_X		{ get { return Input.GetKey( KeyCode.JoystickButton2 ); } }
		static public bool Button_X_Up		{ get { return Input.GetKeyUp( KeyCode.JoystickButton2 ); } }

		// Yボタン.
		static public bool Button_Y_Down	{ get { return Input.GetKeyDown( KeyCode.JoystickButton3 ); } }
		static public bool Button_Y		{ get { return Input.GetKey( KeyCode.JoystickButton3 ); } }
		static public bool Button_Y_Up		{ get { return Input.GetKeyUp( KeyCode.JoystickButton3 ); } }

		// LBボタン.
		static public bool Button_LB_Down	{ get { return Input.GetKeyDown( KeyCode.JoystickButton4 ); } }
		static public bool Button_LB		{ get { return Input.GetKey( KeyCode.JoystickButton4 ); } }
		static public bool Button_LB_Up	{ get { return Input.GetKeyUp( KeyCode.JoystickButton4 ); } }

		// RBボタン.
		static public bool Button_RB_Down	{ get { return Input.GetKeyDown( KeyCode.JoystickButton5 ); } }
		static public bool Button_RB		{ get { return Input.GetKey( KeyCode.JoystickButton5 ); } }
		static public bool Button_RB_Up	{ get { return Input.GetKeyUp( KeyCode.JoystickButton5 ); } }

		// 中央左ボタン( コントローラ上ではBackボタン ).
		static public bool Button_Back_Down	{ get { return Input.GetKeyDown( KeyCode.JoystickButton6 ); } }
		static public bool Button_Back			{ get { return Input.GetKey( KeyCode.JoystickButton6 ); } }
		static public bool Button_Back_Up		{ get { return Input.GetKeyUp( KeyCode.JoystickButton6 ); } }

		// 中央右ボタン( コントローラ上ではStartボタン ).
		static public bool Button_Start_Down	{ get { return Input.GetKeyDown( KeyCode.JoystickButton7 ); } }
		static public bool Button_Start		{ get { return Input.GetKey( KeyCode.JoystickButton7 ); } }
		static public bool Button_Start_Up		{ get { return Input.GetKeyUp( KeyCode.JoystickButton7 ); } }

		// 左アナログスティック押し込み.
		static public bool Button_LStick_Down	{ get { return Input.GetKeyDown( KeyCode.JoystickButton8 ); } }
		static public bool Button_LStick		{ get { return Input.GetKey( KeyCode.JoystickButton8 ); } }
		static public bool Button_LStick_Up	{ get { return Input.GetKeyUp( KeyCode.JoystickButton8 ); } }

		// 右アナログスティック押し込み.
		static public bool Button_RStick_Down	{ get { return Input.GetKeyDown( KeyCode.JoystickButton9 ); } }
		static public bool Button_RStick		{ get { return Input.GetKey( KeyCode.JoystickButton9 ); } }
		static public bool Button_RStick_Up	{ get { return Input.GetKeyUp( KeyCode.JoystickButton9 ); } }

		// XBoxボタン( 真ん中のXBoxマークのボタン,使い道は多分ない ).
		static public bool Button_XBox_Down	{ get { return Input.GetKeyDown( KeyCode.JoystickButton10 ); } }
		static public bool Button_XBox			{ get { return Input.GetKey( KeyCode.JoystickButton10 ); } }
		static public bool Button_XBox_Up		{ get { return Input.GetKeyUp( KeyCode.JoystickButton10 ); } }

		// 左アナログスティック水平( -1.0 ～ 1.0 の値 ).
		static public float LStick_H { get { return Input.GetAxis( "Horizontal" ); } }

		// 左アナログスティック水平( -1.0 | 0 | 1.0 )間の値がない.
		static public float LStick_H_Raw { get { return Input.GetAxisRaw( "Horizontal" ); } }

		// 左スティック垂直( -1.0 ～ 1.0 の値 ).
		static public float LStick_V { get { return Input.GetAxis( "Vertical" ); } }

		// 左スティック垂直( -1.0 | 0 | 1.0 )間の値がない.
		static public float LStick_V_Raw { get { return Input.GetAxisRaw( "Vertical" ); } }

	}






}