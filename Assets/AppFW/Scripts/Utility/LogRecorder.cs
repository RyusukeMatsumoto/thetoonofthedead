﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System;
using AppFW;

namespace AppFW
{
	//==============================================================================
	//------------------------------------------------------------------------------
	// ログ記録用クラス.
	// とりあえずApplication.dataPath以下に配置している.
	// 文字コードは UTF-8を使用している.
	public class LogRecorder
    {


		/// <summary>
		/// yyyyMMddHHmmssの書式を分かりやすくした文字列.
		/// </summary>
		static public string Now {
			get {
				string y = DateTime.Now.ToString( "yyyy" );
				string m = DateTime.Now.ToString( "MM" );
				string d = DateTime.Now.ToString( "dd" );
				string h = DateTime.Now.ToString( "HH" );
				string mm = DateTime.Now.ToString( "mm" );
				string s = DateTime.Now.ToString( "ss" );
				return "[" + y + "/" + m + "/" + d + " " + h + ":" + mm +":" + s + "]" ;
			}
		}

		// 拡張子.
		static readonly string EXT = ".txt";

		// 保存先ディレクトリパス.
		static string Path { get { return Application.dataPath + "/"; } }




		//------------------------------------------------------------------------------
		// 書き込みストリームの取得.
		static StreamWriter GetSW(
				string _path,
				bool _isCreate )
        {
			return ( LogRecorder.Check( _path, _isCreate ) ) ? new StreamWriter( _path, true, System.Text.Encoding.UTF8 ) : null;
		}


		//------------------------------------------------------------------------------
		// 読み込みストリームの取得.
		static StreamReader GetSR(
				string _path,
				bool _isCreate )
        {
			return ( LogRecorder.Check( _path, _isCreate ) ) ? new StreamReader(_path) : null;
		}

	
		//------------------------------------------------------------------------------
		// ログテキストファイルがあるか否かをチェック.
		// なければ作成.
		static bool Check(
				string _path,
				bool _isCreate )
        {
			if ( !File.Exists(_path) )
            {
				if ( _isCreate )
                {
					Debug.LogWarning("指定したログファイルが存在しなかったので新たに生成しました.");
                    using ( FileStream fs = new FileStream( _path ,FileMode.OpenOrCreate ,FileAccess.ReadWrite ,FileShare.ReadWrite ) )
                    {
                        fs.Dispose();
                        fs.Close();
                    }
					return true;
				}
                else
                {
					Debug.LogError("指定したログファイルが存在しません.");
					return false;
				}
			}
			return true;
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// ログを取得.
		/// なければからのログを作成して取得.
		/// </summary>
		/// <param name="_logName"> 取得するログファイル名. </param>
		/// <param name="_isCreate"> 存在しなかった場合作成するか否か. </param>
		/// <returns></returns>
		static public string[] GetLog(
				string _logName,
				bool _isCreate = true )
        {
			string[] _ret = null;
            using ( StreamReader _sr = GetSR( LogRecorder.Path + _logName + LogRecorder.EXT ,_isCreate ) )
            {
                if ( _sr == null )
                    return null;

                Queue<string> _q = new Queue<string>();
                while ( !_sr.EndOfStream )
                {
                    _q.Enqueue(_sr.ReadLine());
                }
                _sr.Close();
                _ret = _q.ToArray();
                _q.Clear();
            }
			return _ret;
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// ログを追加.
		/// </summary>
		/// <param name="_logName"> 追加したいログファイル名. </param>
		/// <param name="_text"> 追加するテキスト. </param>
		/// <param name="_isCreate"> 存在しなかった場合作成するか否か. </param>
		static public void AddLog(
				string _logName,
				string _text,
				bool _isCreate = true )
        {
            using ( StreamWriter _sw = GetSW( LogRecorder.Path + _logName + LogRecorder.EXT ,_isCreate ) )
            {
                if ( _sw == null ) return;
                _sw.WriteLine(_text);
                _sw.Flush();
                _sw.Close();
            }
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// ログの行数を返す.
		/// </summary>
		/// <param name="_logName"> 行数を取得したいログ名. </param>
		/// <param name="_isCreate"> 存在しなかった場合作成するか否か. </param>
		/// <returns> ログの行数. </returns>
		static public int GetLogCount(
				string _logName,
				bool _isCreate = true )
        {
			int _count = 0;
            using ( StreamReader _sr = GetSR( LogRecorder.Path + _logName + LogRecorder.EXT ,_isCreate ) )
            {
                if ( _sr == null ) return -1;

                while ( !_sr.EndOfStream )
                {
                    string _str = _sr.ReadLine();
                    if ( _str != string.Empty )
                        _count++;
                }
                _sr.Close();
            }
			return _count;
		}

	}
}
