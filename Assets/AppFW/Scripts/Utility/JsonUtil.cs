﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text;


namespace AppFW
{
	//==============================================================================
	//------------------------------------------------------------------------------
	// UnityデフォのJsonUtilityをもう少し使いやすく.
	static public class JsonUtil
    {

        static string JsonExt = ".json";


		//------------------------------------------------------------------------------
		/// <summary>
		/// 渡されたクラスを指定したパスにJSON形式ファイルへ変換保存.
		/// </summary>
		/// <param name="_path"></param>
		/// <param name="_fileName"></param>
		/// <param name="_obj"></param>
		/// <param name="_append"></param>
		static public void SaveJson(
				string _path,
				string _fileName,
				object _obj,
				bool _append )
        {
			_path = ( _path.LastIndexOf( '/' ) != ( _path.Length - 1 ) ) ? _path + "/" : _path;
			string _filePath = _path + _fileName;
			JsonUtil.CheckExtension( ref _filePath );
			string _json = JsonUtility.ToJson( _obj, true );
            using ( StreamWriter _sw = new StreamWriter( _filePath ,_append ,Encoding.Default ) )
            {
                _sw.Write( _json );
                _sw.Flush();
                _sw.Close();
            }
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// Json形式ファイルを読み込む.
		/// 存在しなければnullを返す.
		/// </summary>
		/// <param name="_path"> ファイルのパス. </param>
		/// <param name="_type"> 型 </param>
		/// <returns> 読み込まれたJsonファイル. </returns>
		static public object LoadJson(
				string _path,
				System.Type _type )
        {
			JsonUtil.CheckExtension( ref _path );
			object _ret = null;
			if ( !File.Exists( _path ) )
				return null;

            using ( StreamReader _sr = new StreamReader( _path ) )
            {
                _ret = JsonUtility.FromJson( _sr.ReadToEnd() ,_type );
                _sr.Close();
            }
			return _ret;
		}


		//------------------------------------------------------------------------------
		// 拡張子チェック.
		// 拡張子が指定されていなかった場合は付け足す.
		static void CheckExtension(
				ref string _path )
        {
			if ( !_path.Contains(JsonUtil.JsonExt) )
				_path += JsonUtil.JsonExt;
		}	


	}


}








