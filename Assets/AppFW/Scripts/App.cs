﻿#pragma warning disable 0414, 0114
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using AppFW;

using UnityObject = UnityEngine.Object;

namespace AppFW
{

    //==============================================================================
	//------------------------------------------------------------------------------
	// アプリケーション全体の流れを管理するクラス.
	public class App: MonoBehaviour
    {

        readonly string Clone = "(Clone)";


        static App instance_;
        static App SI_
        {
            get {
                if ( !instance_ )
                {
                    instance_ = FindObjectOfType( typeof(App) ) as App;
                    if ( !instance_ )
                        Debug.LogError("AppをアタッチしているGameObjectが存在しないので生成するかしてください");
                }
                return instance_;
            }
        }


		// シーン管理スクリプト( 各シーンの全体の流れを管理するスクリプト ).
		public UnityObject SceneScript { get; set;}


		Transform managers = null;									// 各種マネージャのルート.
		bool isInitialize = false;									// 初期化したか否か.
		Scene currentScene;											// 現在のシーン.
		Dictionary< string, System.Object > tempDatas = null;		// 一時保存データのテーブル.
		
		
		//------------------------------------------------------------------------------
		void Awake()
        {
            if ( this != SI_ )
            {
                Destroy( gameObject );
                return;
            }
			this.Init();
		}


        //------------------------------------------------------------------------------
        void Start()
        {
            SceneManager.sceneLoaded += this.SceneLoadedListener;
            currentScene = SceneManager.GetActiveScene();
        }


        //------------------------------------------------------------------------------
        private void OnDestroy()
        {
            SceneManager.sceneLoaded -= this.SceneLoadedListener;
        }


        //------------------------------------------------------------------------------
        // 新しいシーンがロードされた際に呼び出される.
        void SceneLoadedListener(
                Scene _scene, 
                LoadSceneMode _mode )
        {
            if ( _mode == LoadSceneMode.Single )
            {
			    if ( currentScene != _scene )
                {
				    currentScene = _scene;
				    SceneScript = null;
			    }
            }
        }


		//------------------------------------------------------------------------------
		// 初期化.
		// 各種マネージャ( 最低限必要なもの )がなければ作成し子オブジェクトにする.
		void Init()
        {
			if ( isInitialize ) return;

            GameObject.DontDestroyOnLoad( gameObject );
			tempDatas = new Dictionary< string, System.Object >();
			managers = transform.Find("Managers");
			gameObject.name = gameObject.name.Replace( "(Clone)", "" );

			if ( SoundMng.GetInstance() == null )
				GOUtil.CreateGO( ResourcePath.GetPath(ResourcePath.PathID.SoundMngPF), "SoundMngPF", managers );
			SoundMng.GetTransform().SetParent(managers);

			isInitialize = true;
		}


        #region --- StaticMethods ------------------------------------------------------

        //------------------------------------------------------------------------------
        /// <summary>
        /// 一時保存データの追加.
        /// </summary>
        /// <param name="_key"> 保存するキー値. </param>
        /// <param name="_data"> 保存するデータ( TempDataの継承クラスでなければならない ). </param>
        public static void AddTempData(
				string _key,
				System.Object _data )
        {
			if ( SI_.tempDatas.ContainsKey(_key) )
				Debug.LogWarning("指定したキー値は既にあります,テーブルには登録しませんでした.");
			else
				SI_.tempDatas.Add(_key, _data);
		}
		
		
		//------------------------------------------------------------------------------
		// 一時保存データの取得.
		public static System.Object GetTempData(
				string _key,
				bool _deleteFromTable )
        {
			if ( !SI_.tempDatas.ContainsKey(_key) )
            {
				Debug.LogError("指定したキーのデータは存在しません nullを返します.");
				return null;
			}

			System.Object ret = SI_.tempDatas[_key];
			if ( _deleteFromTable )
				SI_.tempDatas.Remove(_key);
			return ret;
		}


		//------------------------------------------------------------------------------
		// 一時保存データの削除.
		public static void DeleteTempData(
				string _key )
        {
			if ( !SI_.tempDatas.ContainsKey(_key) )
				Debug.LogError("指定したキーのデータは存在しまっせん,削除を中止します.");
			else
				SI_.tempDatas.Remove(_key);
		}

        #endregion --- StaticMethods ---------------------------------------------------

	}
}