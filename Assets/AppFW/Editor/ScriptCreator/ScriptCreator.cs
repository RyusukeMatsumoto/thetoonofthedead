﻿using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Text;
using System.Collections.Generic;


namespace AppFW.Editor.ScriptCreator
{
    //==============================================================================
    //------------------------------------------------------------------------------
    // テンプレートテキストからスクリプト作成を行う.
    public class ScriptCreator : EditorWindow
    {

	    static readonly string TEMPLATE_SCR_EXT = ".txt";	// テンプレートの拡張子.
	    static readonly string SCR_EXT = ".cs";			// 生成するスクリプトの拡張子.

	    // テンプレートがあるディレクトリへのパスAssets以下.
	    static readonly string TEMP_SCR_DIR_PATH = "/AppFW/ScriptTemplates/";

	    // 置換用タグ.
	    static readonly string SCRIPT_NAME_TAG = "#SCRIPTNAME#";


	    string outputPath = string.Empty;		// 出力先フォルダパス.
	    string templateScrName = string.Empty;	// 作成元のテンプレート名.
	    string newScrName = string.Empty;		// 新しく作成するスクリプト及びクラス名.


	    string[] templateList = null;
	    bool isListVisible = true;


	    //------------------------------------------------------------------------------
	    // ウィンドウ表示.
	    [MenuItem( "AppFWTools/ScriptCreator" )]
	    public static void ShowWindow()
        {
		    EditorWindow _window = EditorWindow.GetWindow( typeof( ScriptCreator ) );
		    _window.minSize = new Vector2( 300, 500 );
		    _window.position = new Rect( 200, 200, _window.minSize.x, _window.minSize.y );
	    }

	    //------------------------------------------------------------------------------
	    void CreateTemplateList()
        {
		    // テンプレート一覧を作成.
		    DirectoryInfo _dir = new DirectoryInfo( Application.dataPath + TEMP_SCR_DIR_PATH );
		    FileInfo[] _info = _dir.GetFiles( "*" + TEMPLATE_SCR_EXT );
		    Queue< string > _q = new Queue< string >();
		    foreach( FileInfo f in _info ) {
			    _q.Enqueue( f.Name );
		    }
		    templateList = _q.ToArray();
	    }


	    //------------------------------------------------------------------------------
	    // GUI描画.
	    public void OnGUI()
        {
		    // テンプレート名一覧ボタン.
		    GUILayout.Label( "TemplateList" );
		    if ( templateList == null )
			    this.CreateTemplateList();
		    isListVisible = EditorGUILayout.Toggle( isListVisible );
		    if ( isListVisible )
            {
			    for ( int i = 0; i < templateList.Length; ++i )
                {
				    if ( GUILayout.Button( templateList[ i ] ) )
                    {
					    templateScrName = templateList[ i ];
				    }
			    }
		    }
		    GUILayout.Space( 20 );


		    // 使用するテンプレート名.
		    GUILayout.Label( "Use Template Name" );
		    GUILayout.Label( templateScrName );
		    GUILayout.Space( 10 );

		    // 作成するスクリプト名.
		    GUILayout.Label( "New Script Name" );
		    newScrName = EditorGUILayout.TextField( newScrName );
		    GUILayout.Space( 10 );

		    // 出力先パス.
		    GUILayout.Label( "Output Folder Paqth" );
		    outputPath = EditorGUILayout.TextField( outputPath );
		    GUILayout.Space( 10 );

		    // 作成ボタン.
		    if ( GUILayout.Button( "Crete!!" ) )
            {
			    Debug.Log( "作成." );
			    if ( CreateScript( outputPath, templateScrName, newScrName ) )
				    Debug.Log( "作成成功." );
			    else
				    Debug.Log( "作成失敗." );
		    }

	    }


	    //------------------------------------------------------------------------------
	    /// <summary>
	    /// テンプレートからスクリプトを作成.
	    /// 外部からも此のメソッドを呼び出せるようにしている.
	    /// </summary>
	    /// <param name="_outputPath"> 出力先パス. </param>
	    /// <param name="_templateScrName"> 使用するテンプレートファイル名. </param>
	    /// <param name="_newScrName"> 出力するスクリプトファイル名. </param>
	    /// <param name="_scrSummary"> スクリプト説明. </param>
	    /// <param name="_authorName"> 作成者名. </param>
	    /// <param name="_createDate"> 作成日. </param>
	    /// <returns></returns>
	    public static bool CreateScript(
			    string _outputPath,
			    string _templateScrName,
			    string _newScrName )
        {
		    if ( string.IsNullOrEmpty( _outputPath ) )
            {
			    Debug.LogWarning( "出力先パスが指定されていない為Assets直下に指定します." );
			    _outputPath = Application.dataPath + "/";
		    }

		    if ( string.IsNullOrEmpty( _templateScrName ) )
            {
			    Debug.LogError( "テンプレートファイルが指定されていません." );
			    return false;
		    }

		    if ( string.IsNullOrEmpty( _newScrName ) )
            {
			    Debug.LogError( "スクリプト名が入力されていないため,スクリプトを作成できませんでした." );
			    return false;
		    }

		    // テンプレート読み込み.
		    string _templatePath = Application.dataPath + TEMP_SCR_DIR_PATH + _templateScrName;
		    StreamReader _sr = new StreamReader( _templatePath, Encoding.GetEncoding( "Shift_JIS" ) );
		    string _scriptText = _sr.ReadToEnd();
		    _sr.Close();

		    // 各項目を置換.
		    _scriptText = _scriptText.Replace( SCRIPT_NAME_TAG, _newScrName );

		    // スクリプト書き出し.
		    if ( !_outputPath.EndsWith( "/" ) )
			    _outputPath += "/";
		    File.WriteAllText( _outputPath + _newScrName + SCR_EXT, _scriptText, Encoding.UTF8 );
		    AssetDatabase.SaveAssets();
		    AssetDatabase.Refresh( ImportAssetOptions.ImportRecursive );

		    return true;
	    }

    }
}





