﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;
using UnityEditor.SceneManagement;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;


namespace AppFW.AutoSceneLoader
{
	//==============================================================================
	//------------------------------------------------------------------------------
	// https://gist.github.com/kankikuchi/911f23e01f73f138ce6ecb268a70fc9a#file-playmodestateobserver-cs
	// ↑を参考に作成.
	// 指定したシーン( 初期化用シーンなど )を自動的に読み込むクラス.
	[InitializeOnLoad]
	public class AutoSceneLoader : EditorWindow
    {


		// シーンアセットの拡張子.
		readonly string SCENE_ASSET_EXT = ".unity";



		#region SceneAssetContainor
		//==============================================================================
		//------------------------------------------------------------------------------
		// ロードするシーンアセットのデータ保存用オブジェクト.
		class SceneAssetContainer
        {

			bool isSelected = false;
			public bool IsSelected { get { return this.isSelected; } set { this.isSelected = value; } }

			public string Path { get { return AssetDatabase.GUIDToAssetPath( this.guid ); } }

			string guid = string.Empty;
			public string Guid { get { return this.guid; } }

			SceneAsset item = null;
			public SceneAsset Item { get { return this.item; } }

			public string SceneName { get { return System.IO.Path.GetFileNameWithoutExtension( this.Path ); } }


			//------------------------------------------------------------------------------
			// コンストラクタ.
			public SceneAssetContainer(
					SceneAsset _item )
            {
				this.guid = AssetDatabase.AssetPathToGUID( AssetDatabase.GetAssetPath( _item ) );
			}
			public SceneAssetContainer(
					string _guid )
            {
				this.guid = _guid;
				this.item = AssetDatabase.LoadAssetAtPath( this.Path, typeof( SceneAsset ) ) as SceneAsset;
			}
		}
		#endregion


		List< SceneAssetContainer > list = new List< SceneAssetContainer >();
		Vector2 listScrollPos = Vector2.zero;
		UnityEngine.Object[] ddObj = null;		// Drag&Dropされたオブジェクト( D&Dと同時にリストを追加して描画するとエラーが出るので遅延させて追加する ).


		//------------------------------------------------------------------------------
		// このウィンドウで自動でロードしたいシーンをセットしておく.
		void OnGUI()
        {
			drawDebugLog_ = EditorGUILayout.Toggle( "Draw DebugLog", drawDebugLog_ );
			

			this.DrawDragAndDropBox();

			EditorGUILayout.Space();
			if ( 0 < this.list.Count )
            {
				this.listScrollPos = EditorGUILayout.BeginScrollView( this.listScrollPos, GUI.skin.box, GUILayout.Height( 150f ) );
				for ( int i = 0; i < this.list.Count; ++i )
                {
					this.DrawList( i );
				}
				EditorGUILayout.EndScrollView();
			}
			this.UpdateList();
			EditorGUILayout.Space();

			if ( GUILayout.Button( "Register Scene", GUILayout.Width( 200f ) ) )
            {
				string _register = string.Empty;
				for ( int i = 0; i < this.list.Count; ++i )
                {
					_register += this.list[ i ].Guid + ",";
				}
				EditorUserSettings.SetConfigValue( SCENE_LIST_KEY, _register );
				this.list.Clear();
			}

			if ( GUILayout.Button( "Display Registed Scenes", GUILayout.Width( 200f ) ) )
            {
				string[] _datas = EditorUserSettings.GetConfigValue( SCENE_LIST_KEY ).Split( ',' );
				for ( int i = 0; i < _datas.Length; ++i )
                {
					if ( _datas[ i ] != string.Empty )
                    {
						this.addList( new SceneAssetContainer( _datas[ i ] ) );
					}
				}
			}


			if ( GUILayout.Button( "Delete Scene", GUILayout.Width( 200f ) ) )
            {
				EditorUserSettings.SetConfigValue( SCENE_LIST_KEY, string.Empty );
				this.list.Clear();
			}
		}


		//------------------------------------------------------------------------------
		// ドラッグ&ドロップボックス描画.
		// 複数オブジェクトに対応しているが .unity 以外の拡張子がある場合はリジェクトする.
		void DrawDragAndDropBox()
        {
			EditorGUILayout.BeginVertical( GUI.skin.box, GUILayout.Width( 345f ) );

			EditorGUILayout.LabelField( "Load SceneAsset" );
			Event _evt = Event.current;
			Rect _dropArea = GUILayoutUtility.GetRect( 0f, 50f, GUILayout.ExpandWidth( true ) );
			GUI.Box( _dropArea, "Drag & Drop Any Scene File" );
			switch ( _evt.type ) {

			case EventType.DragUpdated:
			case EventType.DragPerform:
				if ( !_dropArea.Contains( _evt.mousePosition ) ) break;

				bool _existOtherExtension = false;
				for ( int i = 0; i < DragAndDrop.objectReferences.Length; ++i )
                {
					if ( !DragAndDrop.paths[ i ].Contains( this.SCENE_ASSET_EXT ) )
						_existOtherExtension = true;
				}
				if ( _existOtherExtension )
                {
					DragAndDrop.visualMode = DragAndDropVisualMode.Rejected;
					break;
				}
				
				DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
				if ( _evt.type == EventType.DragPerform )
                {
					DragAndDrop.AcceptDrag();
					this.ddObj = DragAndDrop.objectReferences;
				}
				break;
			}
			EditorGUILayout.EndVertical();
		}


		//------------------------------------------------------------------------------
		// リストの内容を描画.
		void DrawList( 
				int _index )
        {
			EditorGUILayout.BeginVertical( GUI.skin.box );
			EditorGUILayout.BeginHorizontal();

			this.list[ _index ].IsSelected = EditorGUILayout.Toggle( "", this.list[ _index ].IsSelected, GUILayout.Width( 20f ) );
			EditorGUILayout.LabelField( this.list[ _index ].SceneName );

			EditorGUILayout.EndHorizontal();

			if ( this.list[ _index ].IsSelected )
            {
				EditorGUILayout.SelectableLabel( this.list[ _index ].Path );
				if ( GUILayout.Button( "Delete", GUILayout.Width( 60f ) ) ) 
					this.list.RemoveAt( _index );
			}
			EditorGUILayout.EndVertical();
		}


		//------------------------------------------------------------------------------
		// 今のC#のバージョンだとasync/awaitが使えない, Observerパターンを利用するにもコードが冗長になる.
		// 不本意だがこのメソッドをDrawList()の後に配置してエラーの対処とする.
		void UpdateList()
        {
			if ( this.ddObj != null )
            {
				for ( int i = 0; i < this.ddObj.Length; ++i )
                {
					this.addList( new SceneAssetContainer( ( SceneAsset )this.ddObj[ i ] ) );
				}
				this.ddObj = null;
			}
		}


		//------------------------------------------------------------------------------
		// リストに要素を追加.
		// 既に同名のファイルがリストに含まれていないかチェックをかける.
		// ある場合はリストに追加しない.
		void addList( 
				SceneAssetContainer _sc )
        {
			if ( this.list.Exists( e => e.SceneName == _sc.SceneName ) ) return;
			this.list.Add( _sc );
		}

		

		#region StaticMember
		
		static readonly string SCENE_LIST_KEY = "AutoSceneLoader_Scenes";
		static readonly char SPLIT_CHAR = ',';

		static AutoSceneLoader autoSceneLoaderWindow_ = null;	// このウィンドウは1つのみとする.
		static bool drawDebugLog_;


		
		//------------------------------------------------------------------------------
		// コンストラクタ( エディタ起動時に呼び出される ).
		static AutoSceneLoader()
        {
			if ( drawDebugLog_ )
				Debug.Log( "AutoSceneLoader Start Up!!" );
			EditorApplication.playmodeStateChanged += OnPlayModeStateChanged;
			EditorApplication.update += OnUpdate;

			string _val = EditorUserSettings.GetConfigValue( SCENE_LIST_KEY );
			if ( _val == null )
            {
				if ( drawDebugLog_ )
					Debug.Log( "AutoSceneLoader_Initialized" );
				EditorUserSettings.SetConfigValue( SCENE_LIST_KEY, string.Empty );
			}
		}


		//------------------------------------------------------------------------------
		// ウィンドウ表示.
		[MenuItem( "AppFWTools/AutoSceneLoader" )]
		public static void ShowWindow()
        {
			if ( autoSceneLoaderWindow_ == null )
            {
				autoSceneLoaderWindow_ = ( AutoSceneLoader )EditorWindow.GetWindow( typeof( AutoSceneLoader ) );
				autoSceneLoaderWindow_.minSize = autoSceneLoaderWindow_.maxSize = new Vector2( 350f, 330f );
				autoSceneLoaderWindow_.position = new Rect( 200, 200f, autoSceneLoaderWindow_.minSize.x, autoSceneLoaderWindow_.minSize.y );
			}
			autoSceneLoaderWindow_.Show();
		}


		//------------------------------------------------------------------------------
		// エディタから再生周りのイベントを取得するハンドラ( コンストラクタでセットしている ).
		// 今後のために一応取っておく.
		static void OnPlayModeStateChanged()
        {
			if ( !EditorApplication.isPlaying &&	EditorApplication.isPlayingOrWillChangePlaymode )
            {
				// シーン再生ボタン押下処理.
				if ( !EditorApplication.isPaused ) {}

			}
            else if ( EditorApplication.isPlaying && !EditorApplication.isPlayingOrWillChangePlaymode )
            {
			}
		}


		//------------------------------------------------------------------------------
		// 更新処理.
		static void OnUpdate() {}


		//------------------------------------------------------------------------------
		// 先に読み込むシーン名を取得.
		static string[] GetPreloadSceneName()
        {
			return EditorUserSettings.GetConfigValue( SCENE_LIST_KEY ).Split( SPLIT_CHAR );
		}


		//------------------------------------------------------------------------------
		// 指定されたゲームオブジェクトの配列のSetActiveにフラグを渡す.
		static void SetActiveObjs(
				GameObject[] _objs,
				bool _flg )
        {
			if ( drawDebugLog_ )
				Debug.Log( "SetActive : " + _flg.ToString()  );
			for ( int i = 0; i < _objs.Length; ++i )
            {
				_objs[ i ].SetActive( _flg );
			}
		}


		//------------------------------------------------------------------------------
		// Awakeよりも先に実行されるAttributeを設定した.
		// ここでシーンをAddすれば _t.Awake() init.Awake() -> _t.Start() init.Start() で間に合わせる事が出来そう.
		[RuntimeInitializeOnLoadMethod( RuntimeInitializeLoadType.BeforeSceneLoad )]
		static void Hohoho()
        {
			string[] _preloadScene = GetPreloadSceneName();
			if ( _preloadScene == null ) return;
			for ( int i = 0; i < _preloadScene.Length; ++i )
            {
				if ( _preloadScene[ i ] != string.Empty )
                {
					EditorApplication.LoadLevelAdditiveInPlayMode( AssetDatabase.GUIDToAssetPath( _preloadScene[ i ] ) );
				}
			}
		}

		#endregion
	}
}



