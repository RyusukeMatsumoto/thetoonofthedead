﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;


namespace AppFW.Editor.ObjectFinder
{
    //------------------------------------------------------------------------------
    //==============================================================================
    // オブジェクト検索機能.
    public class ObjectFinder : EditorWindow
    {

	    // 検索タイプ.
	    public enum SearchType
        {
		    Name = 0,	// オブジェクト名.
		    Component,	// コンポーネント名.
	    }

	    // 検索範囲.
	    public enum SearchScope
        {
		    Hierarchy = 0,	// ヒエラルキー.
		    ProjectWindow,	// プロジェクトウィンドウ.
		    BothSide,		// 両方.
	    }


        const float MainLeftElemWidth = 250f;       // メインウィンドウ左側描画領域幅.
        const float MainMiddleElemWidth = 250f;     // メインウィンドウ中央描画領域幅.
        const float MainRightElemWidth = 300f;      // メインウィンドウ右側描画領域幅.
        const string InSceneExt = ".unity";         // シーンに配置されているゲームオブジェクトの拡張子.
        const string InProjectWindowExt = ".prefab";// プロジェクトウィンドウに配置されているゲームオブジェクトの拡張子.



	    SearchType searchType = SearchType.Name;			// 検索タイプ.
	    SearchScope searchScope = SearchScope.Hierarchy;	// 検索範囲.
	    ObjectTreeDrawSetting objTreeSetting = null;		// オブジェクトツリー描画領域セッティング.
	    ObjectTree objTree;						// オブジェクトツリー.
	    Vector2 middleScrPos = Vector2.zero;	// 中央描画領域のスクロール座標.
	    Vector2 rightScrPos = Vector2.zero;		// 右側描画領域のスクロール座標.
	    string searchText = string.Empty;		// 検索する文字列( オブジェクト名,コンポーネント名併用 ).
	    List< Object > objList = null;			// 検索結果オブジェクトリスト.
	    bool dumpFoldout = false;				// パラメータダンプテキスト表示するか否か.
	    bool wholeWord = false;					// 完全一致か否か.
	    bool caseSencitive = false;				// 大文字小文字.



	    //--------------------------------------------------------------------------------------------------
	    // window表示.
	    [MenuItem( "AppFWTools/ObjectFinder" )]
	    public static void ShowWindow()
        {
		    EditorWindow _window = EditorWindow.GetWindow( typeof( ObjectFinder ) );
		    _window.minSize = new Vector2( MainLeftElemWidth + MainMiddleElemWidth + MainRightElemWidth, 250 );
		    _window.position = new Rect( 200, 200, _window.minSize.x, _window.minSize.y );
	    }


	    //--------------------------------------------------------------------------------------------------
	    // GUI描画.
	    void OnGUI()
        {
		    EditorGUILayout.BeginHorizontal();
		    this.DrawMainLeftElem();
		    this.DrawMainMiddleElem();
		    this.DrawMainRightElem();
		    EditorGUILayout.EndHorizontal();
	    }


	    //--------------------------------------------------------------------------------------------------
	    // メインウィンドウ左側描画領域.
	    // 検索条件などを指定する領域.
	    void DrawMainLeftElem()
        {
		    EditorGUILayout.BeginVertical( GUI.skin.box, GUILayout.Width( MainLeftElemWidth ) );

		    searchType = ( SearchType )EditorGUILayout.EnumPopup( "検索タイプ", searchType );
		    searchScope = ( SearchScope )EditorGUILayout.EnumPopup( "検索範囲", searchScope );
		    wholeWord = EditorGUILayout.Toggle( "完全一致か否か.", wholeWord );
		    if ( !wholeWord )
			    caseSencitive = EditorGUILayout.Toggle( "大小文字一致比較.", caseSencitive );


		    switch ( searchType )
            {
			    case SearchType.Name: EditorGUILayout.LabelField( "Input ObjectName" );	break;
			    case SearchType.Component: EditorGUILayout.LabelField( "Input ComponentName" ); break;
			    default: break;
		    }
		    searchText = EditorGUILayout.TextField( searchText, GUI.skin.textField );
			
		    if ( GUILayout.Button( "Search" ) )
            {
			    Debug.Log( "捜索します." );
			    this.Search();
		    }

            EditorGUILayout.Space();
			EditorGUILayout.Space();
			EditorGUILayout.Space();
            GUILayout.Box( "", GUILayout.Width( MainLeftElemWidth ), GUILayout.Height( 1 ) );
            dumpFoldout = EditorGUILayout.Foldout( dumpFoldout, "Show Dump Palameter" );
		    if ( dumpFoldout )
			    this.DrawDumpPalameter();

		    EditorGUILayout.EndVertical();
	    }

	
	    //--------------------------------------------------------------------------------------------------
	    // メインウィンドウ中央描画領域.
	    void DrawMainMiddleElem()
        {
		    EditorGUILayout.BeginVertical( GUI.skin.box, GUILayout.Width( MainMiddleElemWidth ) );

		    EditorGUILayout.LabelField( "検索結果表示欄." );
		    if ( objList != null )
            {
			    // ここで検索結果のオブジェクトをリスト表示する.
			    middleScrPos = EditorGUILayout.BeginScrollView( middleScrPos );
			    {

				    for ( int i = 0; i < objList.Count; ++i )
                    {
					    if ( GUILayout.Button( objList[ i ].name ) )
                        {
						    objTree = null;
						    objTree = new ObjectTree( ( GameObject )objList[ i ] );
						    Object[] _obj = { ( Object )objList[ i ] };
						    Selection.objects = _obj;
					    }
				    }

			    }
			    EditorGUILayout.EndScrollView();
		    }

		    EditorGUILayout.EndVertical();
	    }
	

	    //--------------------------------------------------------------------------------------------------
	    // メインウィンドウ右側描画領域.
	    // 選択されたオブジェクトのツリー描画機能.
	    void DrawMainRightElem()
        {
		    EditorGUILayout.BeginVertical( GUI.skin.box, GUILayout.Width( MainRightElemWidth ) );

		    EditorGUILayout.LabelField( "ObjectTreeWindow" );
		    if ( objTreeSetting == null )
			    objTreeSetting = new ObjectTreeDrawSetting();
		    objTreeSetting.FaldOut = EditorGUILayout.Foldout( objTreeSetting.FaldOut, "Setting" );
		    if ( objTreeSetting.FaldOut )
            {
			    objTreeSetting.FontSize = EditorGUILayout.IntSlider( "FontSize", objTreeSetting.FontSize, 0, 20 );
		    }

		    if (	objList != null &&
				    objTree != null )
            {

			    rightScrPos = EditorGUILayout.BeginScrollView( rightScrPos );
			    {

				    if ( objTree.TreeExist )
                    {
					    for ( int i = 0; i < objTree.Tree.Length; ++i )
                        {
						    objTreeSetting.Style.normal.textColor = objTree.GetTextColor( i );
						    if ( GUILayout.Button( objTree.GetElemText( i ), objTreeSetting.Style ) ){
							    Object[] _obj = { ( Object )objTree.Tree[ i ].Obj };
							    Selection.objects = _obj;
						    }
					    }

				    }
                    else
                    {
					    objTreeSetting.Style.normal.textColor = objTree.GetTextColor( 0 );
					    if ( GUILayout.Button( objTree.GetElemText( 0 ), objTreeSetting.Style ) )
                        {
						    Object[] _obj = { ( Object )objTree.Root.Obj };
						    Selection.objects = _obj;
					    }
				    }
			    }
			    EditorGUILayout.EndScrollView();
		    }

		    EditorGUILayout.EndVertical();
	    }

	
	    //--------------------------------------------------------------------------------------------------
	    // 検索設定のパラメータ表示.
	    void DrawDumpPalameter()
        {
		    EditorGUILayout.LabelField( "Scope : " + searchScope.ToString() );
		    EditorGUILayout.LabelField( "Search To : " + searchType.ToString() );
		    EditorGUILayout.LabelField( "TargetName : " + searchText );
	    }

	
	    //--------------------------------------------------------------------------------------------------
	    // オブジェクト検索.
	    void Search()
        {
		    if ( objList != null )
			    objList.Clear();
		    else
			    objList = new List< Object >();

		    Object[] _allObject = Resources.FindObjectsOfTypeAll( typeof( GameObject ) );
		    if ( searchType == SearchType.Name )
            {
			    foreach ( Object anObject in _allObject )
                {
				    if ( this.SamplingSearchScope( anObject ) != null )
                    {
					    objList.Add( anObject );
				    }
			    }
		    }
            else
            {
			    foreach ( GameObject anObject in _allObject )
                {
				    if ( 	anObject.GetComponent( searchText ) != null &&
						    this.SamplingSearchScope( anObject ) != null )
                    {
					    objList.Add( anObject );
				    }
			    }
		    }
	    }


	    //--------------------------------------------------------------------------------------------------
	    // 検索範囲及びテキストで抽出.
	    Object SamplingSearchScope( 
			    Object _obj )
        {
		    // 検索しているものがコンポーネント名からならばそのまま返す.
		    if ( searchType == SearchType.Component )
			    return _obj;

		    // テキストが適合しない場合はここでnullを返す.
		    if ( wholeWord )
            {
			    if ( !_obj.name.Equals( searchText ) )
				    return null;
		    }
            else
            {
			    // 含まれている文字列で大文字小文字一致比較.
			    if ( caseSencitive )
                {
				    if ( _obj.name.IndexOf( searchText ) < 0 )
					    return null;
			    }
                else
                {
				    string _lowerName = _obj.name.ToLower();
				    if ( _lowerName.IndexOf( searchText.ToLower() ) < 0 )
					    return  null;
			    }
		    }

		    string _path = AssetDatabase.GetAssetOrScenePath( _obj );
		    if ( ( 0 <= _path.IndexOf( InSceneExt ) ) && ( searchScope == SearchScope.Hierarchy ) )
            {
			    return _obj;
		    }
            else if ( ( 0 <= _path.IndexOf( InProjectWindowExt ) ) && ( searchScope == SearchScope.ProjectWindow ) )
            {
			    return _obj;
		    }
            else if ( searchScope == SearchScope.BothSide )
            {
			    return _obj;
		    }
		    return null;
	    }



	    //--- 内包クラス宣言 ---
	

	    //==================================================================================================
	    // オブジェクトツリー描画領域セッティング.
	    class ObjectTreeDrawSetting
        {

		    // 表示の可否.
		    bool faldOut = false;
		    public bool FaldOut{ get{ return faldOut; } set{ faldOut = value; } }

		    // ボタンテキストフォントサイズ.
		    int fontSize = 10;
		    public int FontSize
            {
			    get{ return fontSize; } 
			    set{ fontSize = value;
				    style.fontSize = fontSize;
			    }
		    }

		    // ボタンテキストスタイル.
		    GUIStyle style = null;
		    public GUIStyle Style { get{ return style; } }


		    //--------------------------------------------------------------------------------------------------
		    // コンストラクタ.
		    public ObjectTreeDrawSetting()
            {
			    style = new GUIStyle( GUI.skin.button );
			    style.fontSize = fontSize;
			    style.alignment = TextAnchor.MiddleLeft;
		    }

        }
	

	    //==================================================================================================
	    // オブジェクトツリー.
	    public class ObjectTree
        {
		    // オブジェクトツリールート.
		    ObjectTreeElem root;
		    public ObjectTreeElem Root
            {
			    get { return root; }
		    }

		    // オブジェクトツリー.
		    ObjectTreeElem[] tree = null;
		    public ObjectTreeElem[] Tree
            { 
			    get { 
				    return tree;
			    }
		    }

		    // ツリーが存在するか否か.
		    public bool TreeExist
            {
			    get { return tree != null; }
		    }

		    GameObject selectedObj;// 選択されたオブジェクト.


		    //--------------------------------------------------------------------------------------------------
		    // コンストラクタ.
		    public ObjectTree( 
				    GameObject _obj )
            {
			    selectedObj = _obj;
			    root = new ObjectTreeElem( 0, _obj.transform.root.gameObject );
			    tree = this.createTree();
		    }


		    //--------------------------------------------------------------------------------------------------
		    // ツリー要素のテキスト取得.
		    public string GetElemText( 
				    int _index )
            {
			    if ( !TreeExist )
				    return root.ButtonText;

			    if ( _index < 0 || tree.Length < _index )
				    return "index error";
			    return tree[ _index ].ButtonText;
		    }


		    //--------------------------------------------------------------------------------------------------
		    // ツリー要素のテキストカラー取得.
		    public Color GetTextColor( 
				    int _index )
            {
			    if ( !TreeExist )
				    return ( root.Obj.GetInstanceID() == selectedObj.GetInstanceID() ) ? Color.cyan : Color.black;
			    else
				    return ( tree[ _index ].Obj.GetInstanceID() == selectedObj.GetInstanceID() ) ? Color.cyan : Color.black;
		    }


		    //--------------------------------------------------------------------------------------------------
		    // オブジェクトツリーの取得.
		    private ObjectTreeElem[] createTree()
            {
			    return root.GetBranch();
		    }

	    }



	    //==================================================================================================
	    // オブジェクトツリー各要素.
	    public class ObjectTreeElem
        {

		    // 前の要素.
		    ObjectTreeElem prev = null;
		    public ObjectTreeElem Prev { get{ return prev; } }

		    // 次の要素群.
		    ObjectTreeElem[] next = null;
		    public ObjectTreeElem[] Next { get{ return next; } }

		    // ルートからの深さ.
		    int count;
		    public int Count { get{ return count; } }

		    // 自身の保持するオブジェクト.
		    GameObject obj;	
		    public GameObject Obj { get{ return obj; } }

		    // ボタンに表示するテキスト.
		    string buttonText = string.Empty;
		    public string ButtonText { get{ return buttonText; } }



		    //--------------------------------------------------------------------------------------------------
		    // コンストラクタ.
		    public ObjectTreeElem(
				    int _count,
				    GameObject _obj )
            {
			    count = _count;
			    obj = _obj;

			    string _depth = "    ";
			    for ( int i = 0; i < count; ++i )
                {
				    buttonText += _depth;
			    }
			    buttonText += ( 0 < count ) ? ">" : "";
			    buttonText += obj.name;

			    if ( obj.transform.childCount != 0 )
                {
				    next = new ObjectTreeElem[ obj.transform.childCount ];
				    for ( int i = 0; i < obj.transform.childCount; ++i )
                    {
					    next[ i ] = new ObjectTreeElem( count + 1, obj.transform.GetChild( i ).gameObject );
				    }
			    }
		    }


		    //--------------------------------------------------------------------------------------------------
		    // 自身より下の階層にいるオブジェクトを配列化して取得.
		    public ObjectTreeElem[] GetBranch()
            {
			    if ( obj.transform.childCount < 1 )
                {
				    return null;
			    }
                else
                {
				    List< ObjectTreeElem > _ret = new List< ObjectTreeElem >();
				    _ret.Add( this );
				    for ( int i = 0; i < obj.transform.childCount; ++i )
                    {
					    ObjectTreeElem[] _elem = next[ i ].GetBranch();
					    if ( _elem != null )
						    _ret.AddRange( next[ i ].GetBranch() );
					    else
						    _ret.Add( next[ i ] );
				    }
				    return _ret.ToArray();
			    }
		    }

        }



    }
}












