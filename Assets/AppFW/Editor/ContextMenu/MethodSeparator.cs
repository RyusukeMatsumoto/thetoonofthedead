﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Text;

namespace AppFW.Editor.MethodSeparator
{
    //==============================================================================
    //------------------------------------------------------------------------------
    // メソッドなどに利用する区切り線をクリップボードに追加.
    public class MethodSeparator
    {
	
        //------------------------------------------------------------------------------
	    /// <summary>
	    /// クリップボードにフォルダパスをコピーする.
	    /// 一応複数ファイルのパスを取得できるようにはしているがあまり使わないと思う.
	    /// </summary>
	    [MenuItem("Assets/MethodSeparator")]
	    static void CopyingMethodSeparator() {
            StringBuilder _buffer = new StringBuilder( 256 );
            _buffer.Append( "//==============================================================================\n" );
            _buffer.Append( "//------------------------------------------------------------------------------\n" );        
            EditorGUIUtility.systemCopyBuffer = _buffer.ToString();
	    }


    }
}
