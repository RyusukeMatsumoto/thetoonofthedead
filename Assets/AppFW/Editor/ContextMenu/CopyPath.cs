﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;


namespace AppFW.Editor.CopyPath
{
    //------------------------------------------------------------------------------
    //==============================================================================
    // フォルダパスをコピーするコンテキストメニュー.
    public class CopyPath
    {
	    //------------------------------------------------------------------------------
	    /// <summary>
	    /// クリップボードにフォルダパスをコピーする.
	    /// 一応複数ファイルのパスを取得できるようにはしているがあまり使わないと思う.
	    /// </summary>
	    [MenuItem("Assets/CopyPath")]
	    static void CopyingPath()
        {

		    if ( Selection.assetGUIDs != null && 0 < Selection.assetGUIDs.Length )
            {
			    List< string > _fileList = new List< string >();
			    foreach ( string files in Selection.assetGUIDs )
                {
				    string _path = AssetDatabase.GUIDToAssetPath( files );
				    _fileList.Add( _path );
			    }

			    string _buffer = string.Empty;
			    foreach ( string file in _fileList )
                {
				    _buffer += ( !string.IsNullOrEmpty( _buffer ) ) ? file + "\n" : file;
			    }
			    EditorGUIUtility.systemCopyBuffer = _buffer;
		    }
	    }
    }
}
