﻿using UnityEngine;
using UnityEditor;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;



namespace AppFW.Editor.SymbolEditor
{
	//==============================================================================
	//------------------------------------------------------------------------------
	// http://kan-kikuchi.hatenablog.com/entry/ScriptingDefineSymbolsEditor
	// ↑を参考に作成.
	// DefineSymbolを操作するクラス.
	public static class SymbolEditor
    {

        // シンボルを区切る文字.
        const string SymbolSeparator = ";";

		// シンボル名一覧を保存する時のkey.
        const string SymbolKeyListSaveKey = "SymbolKeyListKey";

		// シンボル一覧.
		static List<DefineSymbol> symbolList;
		public  static List<DefineSymbol> SymbolList{
			get{
				if ( symbolList == null )
					Init();
				return symbolList;
			}
		}

		// 現在選ばれてるプラットフォーム.
		public static BuildTargetGroup TargetGroup { get; private set; }

		// シンボルを編集したか否か.
		public  static bool IsEdited { get; private set; }

		// 重複したシンボルのkeyがあるか否か.
		public static bool IsDuplicateSymbolKey { get { return symbolList.Select( symbol => symbol.Key ).Distinct().Count() != symbolList.Count; } }



		//メニューからウィンドウを表示
		[MenuItem("AppFWTools/SymbolEditorWindow")]
		public static void Open()
        {
			SymbolEditorWindow.GetWindow( typeof(SymbolEditorWindow) );
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// 初期化
		/// </summary>
		public static void Init()
        {
			IsEdited = false;

			// 現在選ばれてるプラットフォームを取得.
			TargetGroup = EditorUserBuildSettings.selectedBuildTargetGroup;

			// シンボルのkeyをまとめるリスト作成.
			List<string> _symbolKeyList = new List<string>();

			// 現在のプラットフォームに設定されてるシンボル名を取得し、リストに追加.
			string[] _settingSymbols = PlayerSettings.GetScriptingDefineSymbolsForGroup( TargetGroup ).Split( new []{ SymbolSeparator }, StringSplitOptions.None );
			_symbolKeyList.AddRange(_settingSymbols.ToList());

			// EditorUserSettingsに保存されているシンボル名一覧があれば、リストに追加.
			string _savingSymbolKeys = EditorUserSettings.GetConfigValue(SymbolKeyListSaveKey);
			List<string> _savingSymbolKeyList = new List<string>();
			if( !string.IsNullOrEmpty(_savingSymbolKeys) )
            {
				_savingSymbolKeyList = new List<string>( _savingSymbolKeys.Split( new []{ SymbolSeparator }, StringSplitOptions.None ) );
			}
			_symbolKeyList.AddRange(_savingSymbolKeyList);


			// 空白及び重複しているシンボル名を除き、アルファベット順にソート.
			_symbolKeyList = _symbolKeyList.Where( symbolKey => !string.IsNullOrEmpty(symbolKey) ).Distinct().ToList();
			_symbolKeyList.Sort();

			// 各シンボルkeyからシンボルを作成.
			symbolList = new List<DefineSymbol>();
			foreach ( string symbolKey in _symbolKeyList )
            {
				bool _isEnabled = _settingSymbols.Contains(symbolKey);
				DefineSymbol _symbol = new DefineSymbol( symbolKey, _isEnabled );

				// EditorUserSettingsに保存されていなかったkeyは前のValueとInfoが残っている可能性があるので削除.
				if ( !_savingSymbolKeyList.Contains(symbolKey) )
					_symbol.DeleteValueAndInfo();

				symbolList.Add(_symbol);
			}

			// 有効になっているシンボルを上に表示するように
			symbolList = symbolList.OrderBy( symbol => symbol.IsEnabled ? 0 : 1 ).ToList();
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// シンボルを修正
		/// </summary>
		public static void EditSymbol(
				int _symbolNo, 
				string _symbolKey, 
				string _symbolValue, 
				string _symbolInfo, 
				bool _isEnabled,
				bool _definedAppFW = false )
        {
			IsEdited = true;

			// 新たなシンボル作成.
			if ( symbolList.Count <= _symbolNo )
            {
				DefineSymbol _newSymbol = new DefineSymbol( _symbolKey, _isEnabled );
				_newSymbol.SetDefinedAppFW(_definedAppFW);
				symbolList.Add(_newSymbol);
			}

			DefineSymbol _symbol = symbolList[_symbolNo];
			_symbol.Edit( _symbolKey, _symbolValue, _symbolInfo, _isEnabled, _definedAppFW );
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// 全てのシンボルを有効or無効にする
		/// </summary>
		public static void SetAllEnabled(
				bool _isEnabled )
        {
			IsEdited = true;
			foreach ( DefineSymbol symbol in symbolList )
            {
				symbol.Edit(_isEnabled);
			}
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// 全てのシンボルを削除する
		/// </summary>
		public static void AllDelete()
        {
			IsEdited = true;
			symbolList.Clear();
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// シンボルを削除する
		/// </summary>
		public static void Delete(
				int _symbolNo )
        {
			if ( symbolList.Count <= _symbolNo )
				return;
			IsEdited = true;
			symbolList[_symbolNo].DeleteValueAndInfo();
			symbolList.RemoveAt(_symbolNo);
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// 指定したプラットフォームにシンボルを保存
		/// </summary>
		public static void Save(
				BuildTargetGroup _targetGroup, 
				bool _needToCreateDefineValue = true )
        {
			IsEdited = false;
			// シンボルのkeyを,空白を無視かつ重複しないように取得.
			List<string> _symbolKeyList = symbolList
				.Select( symbol => symbol.Key ).Where( symbolKey => !string.IsNullOrEmpty(symbolKey) )
				.Distinct().ToList();

			// シンボルを一つの文字列にまとめ、EditorUserSettingsに保存.
			EditorUserSettings.SetConfigValue( SymbolKeyListSaveKey, string.Join( SymbolSeparator , _symbolKeyList.ToArray() ) );

			// 各シンボルの対応した設定を保存.
			string _enabledSymbols = "";
			Dictionary< string, string > _defineValueDic = new Dictionary< string, string >();
			foreach ( DefineSymbol symbol in symbolList )
            {
				symbol.Save ();

				// valueが設定されている場合は定数クラスに書き出すためにDictにまとめる.
				if ( _needToCreateDefineValue && !string.IsNullOrEmpty(symbol.Value) )
                {
					_defineValueDic[symbol.Key] = symbol.Value;
				}

				// 有効になっているシンボルは設定するように;区切りでenabledSymbolsにまとめる.
				if ( symbol.IsEnabled )
                {
					if ( !string.IsNullOrEmpty(_enabledSymbols) )
                    {
						_enabledSymbols += SymbolSeparator;
					}
					_enabledSymbols += symbol.Key;
				}
			}

			// 設定するグループが不明だとエラーがでるので設定しないように.
			if ( TargetGroup != BuildTargetGroup.Unknown )
            {
				PlayerSettings.SetScriptingDefineSymbolsForGroup( TargetGroup, _enabledSymbols );
			}

			// Symbolに対応した値を定数クラス、DefineValueを書き出す.
			if ( _needToCreateDefineValue )
            {
				ConstantsClassCreator.Create<string>( "DefineValue", "Symbolに対応した値を定数で管理するクラス", _defineValueDic );
			}
			AppFWDefineIO.OutputAppFWDef(symbolList.ToArray());
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// 全プラットフォームに同じシンボルを保存.
		/// </summary>
		public static void SaveAll()
        {
			// Symbolに対応した値をまとめたDefineValueを作成するかのフラグ.
			bool _needToCreateDefineValue = true;
			foreach ( BuildTargetGroup buildTarget in Enum.GetValues( typeof(BuildTargetGroup) ) )
            {
				if ( !IsObsolete(buildTarget) )
                {
					Save( buildTarget, _needToCreateDefineValue );
					// 一度書き出したら、DefineValueを再度書き出さないように.
					_needToCreateDefineValue = false;
				}
			}
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// 指定されたenumの値が廃止されたものか否か.
		/// </summary>
		public static bool IsObsolete(
				Enum _value )
        {
			System.Reflection.FieldInfo _fi = _value.GetType().GetField(_value.ToString());
			ObsoleteAttribute[] _attributes = (ObsoleteAttribute[])_fi.GetCustomAttributes( typeof(ObsoleteAttribute), false );
			return ( _attributes != null && _attributes.Length > 0 );
		}


	}
}




























