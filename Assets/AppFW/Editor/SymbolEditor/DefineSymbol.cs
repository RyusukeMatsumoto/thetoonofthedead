﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;



namespace AppFW.Editor.SymbolEditor
{
	//==============================================================================
	//------------------------------------------------------------------------------
	// http://kan-kikuchi.hatenablog.com/entry/ScriptingDefineSymbolsEditor
	// ↑ここを参考に作成.
	// シンボル名,対応する値,説明を保持するクラス.
	public class DefineSymbol
    {

		// key値.
		public string Key { get; private set; }

		// 対応する値.
		public string Value { get; private set; }

		// シンボルが有効か否か.
		public bool IsEnabled { get; private set; }

		// シンボルの説明.
		public string Info { get; private set; }

		// AppFWシンボルか否か.
		public bool DefinedAppFW { get; private set; }

		
		// 保持する時のkey値の形式.
        const string SymbolValueSaveKeyFormat = "SymbolValueKeyFor{0}";
        const string SymbolInfoSaveKeyFormat = "SymbolInfoKeyFor{0}";



		//------------------------------------------------------------------------------
		// コンストラクタ.
		public DefineSymbol(
				string _key,
				bool _isEnabled )
        {
			Key = _key;
			IsEnabled = _isEnabled;

			// シンボルに対応した値を取得.
			Value = EditorUserSettings.GetConfigValue( this.GetSymbolValueSaveKey(Key) );			
			if ( string.IsNullOrEmpty(Value) )
				Value = "";

			//シンボルの説明を取得
			Info = EditorUserSettings.GetConfigValue( this.GetSymbolInfoSaveKey(Key) );
			if ( string.IsNullOrEmpty(Info) )
				Info = "";
		}


		//------------------------------------------------------------------------------
		// シンボルに対応した値を保存する時に使用するkeyを取得.
		string GetSymbolValueSaveKey(
				string _symbol )
        {
			return string.Format( SymbolValueSaveKeyFormat, _symbol );
		}


		//------------------------------------------------------------------------------
		// シンボルの説明を保存する時に使うkeyを取得.
		string GetSymbolInfoSaveKey(
				string _symbol )
        {
			return string.Format( SymbolInfoSaveKeyFormat, _symbol );
		}


		//------------------------------------------------------------------------------
		// シンボル名を半角英数字+アンダースコア以外を除き、大文字で設定.
		static string ModifyKey(
				string _symbol )
        {
			Regex _regex = new Regex(@"[^_a-zA-Z0-9]");
			_symbol = _regex.Replace( _symbol, "" );
			return _symbol.ToUpper();
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// シンボルに対応した値と情報を削除する.
		/// </summary>
		public void DeleteValueAndInfo()
        {
			Value = "";
			EditorUserSettings.SetConfigValue( this.GetSymbolValueSaveKey(Key), "" );
			Info = "";
			EditorUserSettings.SetConfigValue( this.GetSymbolInfoSaveKey(Key), "" );
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// シンボルの設定を編集する
		/// </summary>
		public void Edit(
				string _key, 
				string _symbolValue, 
				string _symbolInfo, 
				bool _isEnabled,
				bool _definedAppFW = false )
        {
			Key			 = ModifyKey( _key );
			Value		 = _symbolValue;
			Info		 = _symbolInfo;
			IsEnabled	 = _isEnabled;
			DefinedAppFW = _definedAppFW;
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// シンボルの設定を編集する
		/// </summary>
		public void Edit(
				bool _isEnabled )
        {
			IsEnabled = _isEnabled;
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// シンボルの設定を編集する
		/// </summary>
		public void SetDefinedAppFW(
				bool _isDefinedAppFW )
        {
			DefinedAppFW = _isDefinedAppFW;
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// シンボルの設定を保存する
		/// </summary>
		public void Save()
        {
			EditorUserSettings.SetConfigValue( this.GetSymbolValueSaveKey( Key ), Value );
			EditorUserSettings.SetConfigValue( this.GetSymbolInfoSaveKey( Key ), Info );
		}


	}
}










