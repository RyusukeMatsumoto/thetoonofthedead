﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;



namespace AppFW.Editor.SymbolEditor
{
	//==============================================================================
	//------------------------------------------------------------------------------
	// http://kan-kikuchi.hatenablog.com/entry/ScriptingDefineSymbolsEditor
	// ↑を参考に作成.
	// DefineSymbolを操作する際に使用するウィンドウ.
	// こっちはAppFWシンボル定義側処理をまとめたもの.
	public partial class SymbolEditorWindow : EditorWindow
    {


		//------------------------------------------------------------------------------
		// AppFWシンボルメニュー作成.
		void CreateAppFWSymbolMenu()
        {
			// シンボル分メニュー作成.
			List<DefineSymbol> _symbolList = SymbolEditor.SymbolList;
			for ( int i = 0; i < _symbolList.Count; i++ )
            {
				if ( _symbolList[i].DefinedAppFW )
                {
					this.CreateAppFWSymbolMenuParts( _symbolList[i], i );
					GUILayout.Space(5);
				}
			}

			// 新規に追加できるよう、空白のメニューを追加.
			DefineSymbol _newSymbol = new DefineSymbol( "", false );
			_newSymbol.SetDefinedAppFW(true);
			this.CreateAppFWSymbolMenuParts( _newSymbol, _symbolList.Count );

			GUILayout.Space (10);

			// 保存されている状態に戻すボタン.
			if ( GUILayout.Button("Reset") )
            {
				SymbolEditor.Init();
			}

			GUILayout.Space(10);

			// 全て無効にするボタン.
			if ( GUILayout.Button("All Invalid") )
            {
				SymbolEditor.SetAllEnabled(false);
			}

			// 全て有効にするボタン.
			if ( GUILayout.Button("All Valid") )
            {
				SymbolEditor.SetAllEnabled(true);
			}

			GUILayout.Space(10);

			// 全部削除するボタン.
			if ( GUILayout.Button("All Delete") )
            {
				SymbolEditor.AllDelete();
			}
		}


		//------------------------------------------------------------------------------
		// AppFW各シンボルのメニューを作成.
		void CreateAppFWSymbolMenuParts(
				DefineSymbol _symbol, 
				int _symbolNo )
        {
			// 有効になっているかどうかでスキンを変える.
			EditorGUILayout.BeginVertical( _symbol.IsEnabled ? GUI.skin.button : GUI.skin.textField );
			{
				// 内容の変更チェック開始.
				EditorGUI.BeginChangeCheck();

				string _symbolKey = _symbol.Key;
				bool _isEnabled   = _symbol.IsEnabled;

				EditorGUILayout.BeginHorizontal();
				{
					// 内容の変更チェック開始.
					EditorGUI.BeginChangeCheck();

					// チェックボックス作成.
					_isEnabled = EditorGUILayout.Toggle( _isEnabled, GUILayout.Width(15) );

					// シンボル名.
					EditorGUILayout.LabelField( "Symbol", GUILayout.Width(45) );
					_symbolKey = GUILayout.TextField(_symbolKey);

					// 最後の新規入力欄以外は削除ボタンを表示.
					if ( _symbolNo < SymbolEditor.SymbolList.Count )
                    {
						if ( GUILayout.Button( "X", GUILayout.Width(20), GUILayout.Height(14) ) )
                        {
							SymbolEditor.Delete(_symbolNo);
							return;
						}
					}
				}
				EditorGUILayout.EndHorizontal();

				EditorGUI.indentLevel = 2;

				// シンボルに対応する値.
				string _symbolValue = _symbol.Value;
				EditorGUILayout.BeginHorizontal();
				{
					EditorGUILayout.LabelField( "Value", GUILayout.Width(64) );
					_symbolValue = GUILayout.TextField(_symbolValue);
				}
				EditorGUILayout.EndHorizontal();

				// シンボルの説明.
				string _symbolInfo = _symbol.Info;
				EditorGUILayout.BeginHorizontal();
				{
					EditorGUILayout.LabelField( "Info", GUILayout.Width(64) );
					_symbolInfo = GUILayout.TextField(_symbolInfo);
				}
				EditorGUILayout.EndHorizontal();

				// 内容が変更されていれば、シンボルを編集.
				if ( EditorGUI.EndChangeCheck() )
                {
					SymbolEditor.EditSymbol( _symbolNo, _symbolKey, _symbolValue, _symbolInfo, _isEnabled, true );
				}
			}
			EditorGUILayout.EndVertical();

			EditorGUI.indentLevel = 0;
		}


	}
}


































