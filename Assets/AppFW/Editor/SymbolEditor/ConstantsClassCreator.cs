﻿using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Text;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace AppFW.Editor.SymbolEditor
{

	//==============================================================================
	//------------------------------------------------------------------------------
	// http://kan-kikuchi.hatenablog.com/entry/ScriptingDefineSymbolsEditor
	// ↑を参考に作成.
	// 定数を管理するクラスを生成するクラス.
	public static class ConstantsClassCreator
    {


		// 無効な文字列を管理する配列.
        
        static readonly string[] InvalidChars = {
			" ", "!", "\"", "#", "$",
			"%", "&", "\'", "(", ")",
			"-", "=", "^",  "~", "\\",
			"|", "[", "{",  "@", "`",
			"]", "}", ":",  "*", ";",
			"+", "/", "?",  ".", ">",
			",", "<"
		};


		// 定数の区切り文字.
        const char Delimiter = '_';

		// スクリプトの拡張子.
        const string ScriptExt = ".cs";

		// 作成したスクリプトを書き出す先.
		//private const string EXPORT_DIRECTORY_PATH = "Assets/Scripts/Constants/";
        const string ExportDirectoryPath = "Assets/AppFW/Editor/SymbolEditor/";

		// 型名.
        const string StringName = "string";
        const string IntName = "int";
        const string FloatName = "Float";


		//------------------------------------------------------------------------------
		/// <summary>
		/// 定数を管理するクラスを自動生成する.
		/// </summary>
		/// <param name="_className"> クラスの名前. </param>
		/// <param name="_classInfo"> クラスを説明するコメント文. </param>
		/// <param name="_variableDic"> 定数名とその値をセットで登録したDictionary. </param>
		/// <typeparam name="T">定数の型、stringかintかfloat</typeparam>
		public static void Create<T>(
				string _className, 
				string _classInfo, 
				Dictionary< string, T > _valueDict )
        {
			// 入力された型の判定.
			string _typeName = null;
			if ( typeof(T) == typeof(string) )
            {
				_typeName = StringName;
			}
            else if ( typeof(T) == typeof(int) )
            {
				_typeName = IntName;
			}
            else if ( typeof(T) == typeof(float) )
            {
				_typeName = FloatName;
			}
            else
            {
				Debug.Log( _className + ScriptExt +"の作成に失敗しました.想定外の型" + typeof(T).Name  + "が入力されました" );
				return;
			}

			// ディクショナリーをソートしたものに.
			SortedDictionary< string, T > _sortDict = new SortedDictionary< string, T >(_valueDict);

			// 入力された辞書のkeyから無効な文字列を削除して、大文字に_を設定した定数名と同じものに変更し新たな辞書に登録.
			// 次の定数の最大長求めるところで、_を含めたものを取得したいので、先に実行.
			Dictionary< string, T > _newValueDict = new Dictionary< string, T >();
			foreach ( KeyValuePair< string, T > valuePair in _sortDict )
            {
				string _newKey = RemoveInvalidChars(valuePair.Key);
				_newKey = SetDelimiterBeforeUppercase(_newKey);
				_newValueDict[_newKey] = valuePair.Value;
			}

			// 定数名の最大長を取得し、空白数を決定.
			int _keyLengthMax = 0;
			if ( _newValueDict.Count > 0 )
            {
				_keyLengthMax = 1 + _newValueDict.Keys.Select( key => key.Length ).Max();
			}

			// コメント文とクラス名を入力.
			StringBuilder _builder = new StringBuilder();

			_builder.AppendLine("/// <summary>");
			_builder.AppendFormat( "/// {0}", _classInfo ).AppendLine();
			_builder.AppendLine("/// </summary>");
			_builder.AppendFormat( "public static class {0}", _className ).AppendLine( "{" ).AppendLine();

			// 入力された定数とその値のペアを書き出していく.
			string[] _keyArray = _newValueDict.Keys.ToArray();
			foreach ( string key in _keyArray )
            {
				if ( string.IsNullOrEmpty(key) )
                {
					continue;// 文字列が空白またはEmptyならば無視.
				}
                else if ( System.Text.RegularExpressions.Regex.IsMatch( key, @"^[0-9]+$" ) )
                {
					continue;// 数字だけのkeyだったら無視.
				}
                else if ( !System.Text.RegularExpressions.Regex.IsMatch( key, @"^[_a-zA-Z0-9]+$" ) )
                {
					continue;// keyに半英数字と_以外が含まれていたら無視.
				}

				// イコールが並ぶ用に空白を調整する.
				string _equalStr = String.Format( "{0, " + ( _keyLengthMax - key.Length ).ToString() + "}", "=" );

				// 上記で判定した型と定数名を入力.
				_builder.Append("\t").AppendFormat( @"public const {0} {1} {2} ", _typeName, key, _equalStr );

				if ( _typeName.Contains(StringName) )
                {
					// Tがstringの場合は値の前後に"を付ける.
					_builder.AppendFormat( @"""{0}"";", _newValueDict[key] ).AppendLine();
				}
                else if ( _typeName.Contains(FloatName) )
                {
					// Tがfloatの場合は値の後にfを付ける
					_builder.AppendFormat( @"{0}f;", _newValueDict[key] ).AppendLine();
				}
                else
                {
					_builder.AppendFormat( @"{0};", _newValueDict[key] ).AppendLine();
				}
			}
			_builder.AppendLine().AppendLine("}");

			// 書き出し、ファイル名はクラス名.cs.
			string _exportPath = ExportDirectoryPath + _className + ScriptExt;

			// 書き出し先のディレクトリが無ければ作成.
			string _directoryName = Path.GetDirectoryName(_exportPath);
			if ( !Directory.Exists(_directoryName) )
            {
				Directory.CreateDirectory(_directoryName);
			}

			File.WriteAllText( _exportPath, _builder.ToString(), Encoding.UTF8 );
			AssetDatabase.Refresh(ImportAssetOptions.ImportRecursive);

			Debug.Log( _className + ScriptExt + "の作成が完了しました" );
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// 無効な文字を削除.
		/// </summary>
		static string RemoveInvalidChars(
				string _str )
        {
			Array.ForEach( InvalidChars, c => _str = _str.Replace( c, string.Empty ) );
			return _str;
		}


		//------------------------------------------------------------------------------
		/// <summary>
		/// 区切り文字を大文字の前に設定する
		/// </summary>
		static string SetDelimiterBeforeUppercase(
				string _str )
        {
			string _conversionStr = "";
			for ( int strNo = 0; strNo < _str.Length; strNo++ )
            {
				bool _isSetDelimiter = true;
				if ( strNo == 0 )
                {
					// 最初には設定しない.
					_isSetDelimiter = false;
				}
                else if ( char.IsLower( _str[strNo] ) || char.IsNumber( _str[strNo] ) )
                {
					// 小文字か数字なら設定しない.
					_isSetDelimiter = false;
				}
                else if ( char.IsUpper( _str[ strNo - 1 ] ) && !char.IsNumber( _str[strNo] ) )
                {
					// 判定してるの前が大文字なら設定しない( 連続大文字の時 ).
					_isSetDelimiter = false;
				}
                else if ( _str[strNo] == Delimiter || _str[ strNo - 1 ] == Delimiter )
                {
					// 判定してる文字かその文字の前が区切り文字なら設定しない.
					_isSetDelimiter = false;
				}

				// 文字設定.
				if ( _isSetDelimiter )
                {
					_conversionStr += Delimiter.ToString();
				}
				_conversionStr += _str.ToUpper()[ strNo ];
			}

			return _conversionStr;
		}



	}
}















