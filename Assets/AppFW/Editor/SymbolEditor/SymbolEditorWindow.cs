﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;



namespace AppFW.Editor.SymbolEditor
{
	//==============================================================================
	//------------------------------------------------------------------------------
	// http://kan-kikuchi.hatenablog.com/entry/ScriptingDefineSymbolsEditor
	// ↑を参考に作成.
	// DefineSymbolを操作する際に使用するウィンドウ.
	public partial class SymbolEditorWindow : EditorWindow
    {

        Vector2 scrollPos = Vector2.zero;   // スクロール座標.
		bool editAppFWDefine = false;	// AppFWのdefine定義を編集するか否か.



		//------------------------------------------------------------------------------
		// 初期化.
		private void OnEnable()
        {
			SymbolEditor.Init();
		}


		//------------------------------------------------------------------------------
		// GUIの描画.
		void OnGUI()
        {
			if ( EditorApplication.isPlaying || Application.isPlaying || EditorApplication.isCompiling )
            {
				EditorGUILayout.HelpBox( "コンパイル中、実行中は変更できません", MessageType.Warning );
				return;
			}

			editAppFWDefine = EditorGUILayout.Toggle( "AppFWのシンボル定義をする", editAppFWDefine );


			// 変更があり、セーブしていない時は上に注意を表示.
			if ( SymbolEditor.IsEdited )
            {
				EditorGUILayout.HelpBox( "保存されていない情報があります", MessageType.Warning );
			}
            else
            {
				EditorGUILayout.HelpBox( "保存済みです", MessageType.Info );
			}

			scrollPos = EditorGUILayout.BeginScrollView( scrollPos, GUI.skin.scrollView );
			EditorGUILayout.PrefixLabel( "Current Target : " + SymbolEditor.TargetGroup.ToString() );

			// シンボル一覧表示.
			EditorGUILayout.BeginHorizontal(GUI.skin.box);
			{
				EditorGUILayout.BeginVertical(GUI.skin.box);
				{
					EditorGUILayout.PrefixLabel("Scripting Define Symbols");
					this.CreateSymbolMenu();
				}
				EditorGUILayout.EndVertical();


				if ( editAppFWDefine )
                {
					EditorGUILayout.BeginVertical(GUI.skin.box);
					{
					    this.CreateAppFWSymbolMenu();
					}
					EditorGUILayout.EndVertical();
				}
			}
			EditorGUILayout.EndHorizontal();


			GUILayout.Space(20);

			// Saveメニューを並べる.
			EditorGUILayout.BeginVertical(GUI.skin.box);
			{
				EditorGUILayout.PrefixLabel("Save");
				this.CreateSaveMenu();
			}
			EditorGUILayout.EndVertical();

			EditorGUILayout.EndScrollView();
		}


		//------------------------------------------------------------------------------
		// シンボルのメニューを作成
		void CreateSymbolMenu()
        {
			// シンボル分メニュー作成.
			List<DefineSymbol> _symbolList = SymbolEditor.SymbolList;
			for ( int i = 0; i < _symbolList.Count; i++ )
            {
				if ( _symbolList[i].DefinedAppFW ) this.CreateDontInputSymbolMenuParts( _symbolList[i], i );
				else							   this.CreateSymbolMenuParts( _symbolList[i], i );
				GUILayout.Space(5);
			}

			// 新規に追加できるよう、空白のメニューを追加.
			DefineSymbol _newSymbol = new DefineSymbol( "", false );
			this.CreateSymbolMenuParts( _newSymbol, _symbolList.Count );

			GUILayout.Space(10);

			// 保存されている状態に戻すボタン.
			if ( GUILayout.Button("Reset") )
            {
				SymbolEditor.Init();
			}

			GUILayout.Space(10);

			// 全て無効にするボタン.
			if ( GUILayout.Button("All Invalid") )
            {
				SymbolEditor.SetAllEnabled(false);
			}

			// 全て有効にするボタン.
			if ( GUILayout.Button("All Valid") )
            {
				SymbolEditor.SetAllEnabled(true);
			}

			GUILayout.Space(10);

			// 全部削除するボタン.
			if ( GUILayout.Button("All Delete") )
            {
				SymbolEditor.AllDelete();
			}
		}


		//------------------------------------------------------------------------------
		// 各シンボルのメニューを作成.
		void CreateSymbolMenuParts(
				DefineSymbol _symbol, 
				int _symbolNo )
        {
			// 有効になっているかどうかでスキンを変える.
			EditorGUILayout.BeginVertical( _symbol.IsEnabled ? GUI.skin.button : GUI.skin.textField );
			{
				// 内容の変更チェック開始.
				EditorGUI.BeginChangeCheck();

				string _symbolKey = _symbol.Key;
				bool _isEnabled   = _symbol.IsEnabled;

				EditorGUILayout.BeginHorizontal();
				{
					// 内容の変更チェック開始.
					EditorGUI.BeginChangeCheck();

					// チェックボックス作成.
					_isEnabled = EditorGUILayout.Toggle ( _isEnabled, GUILayout.Width(15) );

					// シンボル名.
					EditorGUILayout.LabelField( "Symbol", GUILayout.Width(45) );
					_symbolKey = GUILayout.TextField(_symbolKey);

					// 最後の新規入力欄以外は削除ボタンを表示.
					if ( _symbolNo < SymbolEditor.SymbolList.Count )
                    {
						if ( GUILayout.Button( "X", GUILayout.Width(20), GUILayout.Height(14) ) )
                        {
							SymbolEditor.Delete(_symbolNo);
							return;
						}
					}
				}
				EditorGUILayout.EndHorizontal();

				EditorGUI.indentLevel = 2;

				// シンボルに対応する値.
				string _symbolValue = _symbol.Value;
				EditorGUILayout.BeginHorizontal();
				{
					EditorGUILayout.LabelField( "Value", GUILayout.Width(64) );
					_symbolValue = GUILayout.TextField(_symbolValue);
				}
				EditorGUILayout.EndHorizontal();

				// シンボルの説明.
				string _symbolInfo = _symbol.Info;
				EditorGUILayout.BeginHorizontal();
				{
					EditorGUILayout.LabelField( "Info", GUILayout.Width(64) );
					_symbolInfo = GUILayout.TextField(_symbolInfo);
				}
				EditorGUILayout.EndHorizontal();

				// 内容が変更されていれば、シンボルを編集.
				if ( EditorGUI.EndChangeCheck() )
                {
					SymbolEditor.EditSymbol( _symbolNo, _symbolKey, _symbolValue, _symbolInfo, _isEnabled );
				}
			}
			EditorGUILayout.EndVertical();

			EditorGUI.indentLevel = 0;
		}


		//------------------------------------------------------------------------------
		// 各シンボルの有効化無効化以外入力不可能なメニューを作成.
		void CreateDontInputSymbolMenuParts(
				DefineSymbol _symbol, 
				int _symbolNo )
        {
			// 有効になっているかどうかでスキンを変える.
			EditorGUILayout.BeginVertical( _symbol.IsEnabled ? GUI.skin.button : GUI.skin.textField );
			{
				// 内容の変更チェック開始.
				EditorGUI.BeginChangeCheck();

				string _symbolKey = _symbol.Key;
				bool _isEnabled   = _symbol.IsEnabled;
				EditorGUILayout.BeginHorizontal();
				{
					// 内容の変更チェック開始.
					EditorGUI.BeginChangeCheck();

					// チェックボックス作成.
					_isEnabled = EditorGUILayout.Toggle( _isEnabled, GUILayout.Width(15) );

					// シンボル名.
					EditorGUILayout.LabelField( "Symbol", GUILayout.Width(45) );
					GUILayout.Label(_symbolKey);
				}
				EditorGUILayout.EndHorizontal();

				EditorGUI.indentLevel = 2;

				// シンボルに対応する値.
				string _symbolValue = _symbol.Value;
				EditorGUILayout.BeginHorizontal();
				{
					EditorGUILayout.LabelField( "Value", GUILayout.Width(64) );
					GUILayout.Label(_symbolValue);
				}
				EditorGUILayout.EndHorizontal();

				// シンボルの説明.
				string _symbolInfo = _symbol.Info;
				EditorGUILayout.BeginHorizontal();
				{
					EditorGUILayout.LabelField( "Info", GUILayout.Width(64) );
					GUILayout.Label(_symbolInfo);
				}
				EditorGUILayout.EndHorizontal();

				// 内容が変更されていれば、シンボルを編集( こちらでは有効か無効かのみ編集可能 ).
				if ( EditorGUI.EndChangeCheck() )
					SymbolEditor.EditSymbol( _symbolNo, _symbolKey, _symbolValue, _symbolInfo, _isEnabled, _symbol.DefinedAppFW );

			}
			EditorGUILayout.EndVertical();

			EditorGUI.indentLevel = 0;
		}


		//------------------------------------------------------------------------------
		// セーブメニュー作成.
		private void CreateSaveMenu()
        {
			if ( SymbolEditor.IsDuplicateSymbolKey )
            {
				EditorGUILayout.HelpBox( "Symbolが重複しているので保存できません。", MessageType.Warning );
				return;
			}

			if ( GUILayout.Button("Current Target") )
				SymbolEditor.Save(SymbolEditor.TargetGroup);

			if ( GUILayout.Button("All Target") )
				SymbolEditor.SaveAll();
		}


	}
}






















