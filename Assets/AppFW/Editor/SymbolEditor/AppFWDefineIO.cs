﻿using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;


namespace AppFW.Editor.SymbolEditor
{
	//==============================================================================
	//------------------------------------------------------------------------------
	// AppFWでの共通Symbolの外部への入出力機能.
	// フォーマットは.
	// {
	//		#SYMBOL# シンボル
	//		#VALUE# 値
	//		#INFO# 情報
	// }
	static public class AppFWDefineIO
    {

		// タグ.
        static readonly string SymbolStart = "{";
        static readonly string SymbolTag = "#SYMBOL#";
        static readonly string ValueTag = "#VALUE#";
        static readonly string InfoTag = "#INFO#";
        static readonly string SymbolEnd = "}";

		// 出力先パス.
		static readonly string OutputPath = "Assets/AppFW/Editor/SymbolEditor/AppDefineSymbol.txt";




		//------------------------------------------------------------------------------
		// 外部テキストへ出力.
		static public void OutputAppFWDef(
				DefineSymbol[] _symbols )
        {
			StringBuilder _sb = new StringBuilder();

			foreach ( DefineSymbol symbol in _symbols )
            {
				if ( symbol.DefinedAppFW )
                {
					_sb.AppendLine(SymbolStart);
					_sb.AppendLine( SymbolTag + " " + symbol.Key );
					_sb.AppendLine( ValueTag + " " + symbol.Value );
					_sb.AppendLine( InfoTag + " " + symbol.Info );
					_sb.AppendLine(SymbolEnd);
				}
			}


			// 書き出し先のディレクトリが無ければ作成.
			string _directoryName = Path.GetDirectoryName(OutputPath);
			if ( !Directory.Exists(_directoryName) )
            {
				Directory.CreateDirectory(_directoryName);
			}

			File.WriteAllText( OutputPath, _sb.ToString(), Encoding.UTF8 );
			AssetDatabase.Refresh(ImportAssetOptions.ImportRecursive);
		}
	}
}





