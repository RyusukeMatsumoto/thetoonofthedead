﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections;


namespace AppFW.Editor.ScriptableObjectCreator
{

    //------------------------------------------------------------------------------
    //==============================================================================
    // スクリプタブルオブジェクト生成エディタ.
    public class ScriptableObjectCreator : EditorWindow
    {

	    static readonly string ASSET_EXT = ".asset";

	    UnityEngine.Object obj = null;		// オブジェクト.
	    string dataName = string.Empty;		// 保存するオブジェクト名.
	    string outputPath = string.Empty;	// 出力先フォルダパス.



	    //------------------------------------------------------------------------------
	    // メニューのWindowにEditorExという項目を追加。
        [MenuItem("AppFWTools/ScriptableObjectCreator")]
        static void Open()
        {
            // メニューのWindow/EditorExを選択するとOpen()が呼ばれる。
            // 表示させたいウィンドウは基本的にGetWindow()で表示＆取得する。
            EditorWindow.GetWindow< ScriptableObjectCreator >( "ScriptableObjectCreator" ); // タイトル名を"EditorEx"に指定（後からでも変えられるけど）
        }

 
	    //------------------------------------------------------------------------------
	    // Windowのクライアント領域のGUI処理を記述
        void OnGUI()
        {
		    obj = EditorGUILayout.ObjectField( "データ", obj, typeof( UnityEngine.Object ), true );
		    dataName = EditorGUILayout.TextField( "FileName", dataName );
		    outputPath = EditorGUILayout.TextField( "Output Folder Path", outputPath );

		    if ( GUILayout.Button( "Create!!" ) )
            {
			    string _fileName = ( string.IsNullOrEmpty( dataName ) ) ? obj.name : dataName;
			    if ( Create( obj, outputPath, _fileName ) )
				    Debug.Log( "作成成功." );
			    else
				    Debug.Log( "作成失敗." );
		    }
	    }

		 
	    //------------------------------------------------------------------------------
	    /// <summary>
	    /// スクリプタブルオブジェクトの出力.
	    /// 外部からもこのメソッドを呼び出せるようにしている.
	    /// 同名ファイルがある場合は上書き保存する.
	    /// </summary>
	    /// <param name="_obj"> オブジェクト. </param>
	    /// <param name="_outputPath"> 出力先フォルダパス. </param>
	    /// <param name="_fileName"> 出力ファイル名. </param>
	    /// <returns></returns>
	    static public bool Create(
			    Object _obj,
			    string _outputPath,
			    string _fileName )
        {
		    ScriptableObject _instance = ScriptableObject.CreateInstance( _obj.name );

		    // パスチェック.
		    if ( string.IsNullOrEmpty( _outputPath ) )
            {
			    _outputPath = AssetDatabase.GetAssetPath( Selection.activeObject );
			    if ( string.IsNullOrEmpty( _outputPath ) )
				    _outputPath = "Assets/";
			    else
				    _outputPath += "/";
		    }
            else
            {
			    if ( !_outputPath.EndsWith( "/" ) )
				    _outputPath += "/";
		    }
		    string _path = _outputPath + _fileName + ASSET_EXT;

		    AssetDatabase.CreateAsset( _instance, _path );
		    AssetDatabase.SaveAssets();
		    AssetDatabase.Refresh( ImportAssetOptions.Default );
		    Selection.activeObject = _instance;
		    EditorUtility.SetDirty( _instance );

		    return true;
	    }

	
	    //------------------------------------------------------------------------------
	    /// <summary>
	    /// スクリプタブルオブジェクト生成.
	    /// 外部から呼び出せるように.
	    /// 生成したスクリプタブルオブジェクトのインスタンスを返す.
	    /// </summary>
	    /// <param name="_className"></param>
	    /// <param name="_outputPath"></param>
	    /// <param name="_fileName"></param>
	    /// <returns></returns>
	    static public ScriptableObject Create(
			    string _className,
			    string _outputPath,
			    string _fileName )
        {
		    ScriptableObject _instance = ScriptableObject.CreateInstance( _className );

		    // パスチェック.
		    if ( string.IsNullOrEmpty( _outputPath ) )
            {
			    _outputPath = AssetDatabase.GetAssetPath( Selection.activeObject );
			    if ( string.IsNullOrEmpty( _outputPath ) )
				    _outputPath = "Assets/";
			    else
				    _outputPath += "/";
		    }
            else
            {
			    if ( !_outputPath.EndsWith( "/" ) )
				    _outputPath += "/";
		    }
		    string _path = _outputPath + _fileName + ASSET_EXT;

		    AssetDatabase.CreateAsset( _instance, _path );
		    AssetDatabase.SaveAssets();
		    AssetDatabase.Refresh( ImportAssetOptions.Default );
		    Selection.activeObject = _instance;
		    EditorUtility.SetDirty( _instance );

		    return _instance;
	    }

    }
}








