﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System;

namespace AppFW.Editor.JsonFileEditor
{
	//==============================================================================
	//------------------------------------------------------------------------------
	// Jsonファイル出力エディタ,csファイル出力部分.
	public partial class JsonFileEditor : EditorWindow
    {


		//------------------------------------------------------------------------------
		// 出力スクリプトを作成.
		string CreateScriptText(
				string _fileName,
				VarContainer[] _list )
        {

			StringBuilder _sb = new StringBuilder();
			_sb.Capacity = 1024;	// とりあえず1024文字位を想定.
			_sb.Append( "using UnityEngine;\n" +
                        "using System.Collections;\n" +
                        "using System.Text;\n" +
                        "using AppFW;\n\n" +
                        $"namespace {Application.productName}\n{{" +
                        "\t//==============================================================================\n" +
                        "\t//------------------------------------------------------------------------------\n" +
						"\t[System.Serializable]\n" +
						$"\tpublic class {_fileName}\n" + 
                        "\t{\n\n" +
                        "\t\t// Data Path\n" +
                        "\t\tstatic public string Path\n" +
                        "\t\t{" +
                        "\t\t\tget\n" +
                        "\t\t\t{\n" +
                        "\t\t\t\tstring _ret = string.Empty;\n" +
                        "#if UNITY_EDITOR\n" +
                        $"\t\t\t\t_ret = Application.dataPath + $\"Assets/以降のパス\";\n" +
                        "#elif !UNITY_EDITOR\n" +
                        $"\t\t\t\t_ret = Application.dataPath + $\"/ファイル名.json\";\n" +
                        "#endif\n" +
                        "\t\t\t\treturn _ret;\n" +
                        "\t\t\t}\n" +
                        "\t\t}\n" +
                        "\n\n" +
						this.GetClassParamStr( ref _list ) +
                        "\n\n" +
                        "\t\t//------------------------------------------------------------------------------\n" +
                        $"\t\tpublic {_fileName}(){{}}\n" +
                        "\n\n" +
                        "\t\t//------------------------------------------------------------------------------\n" +
                        "\t\tpublic override string ToString()\n" +
                        "\t\t{\n" +
                        "\t\t\tStringBuilder _sb = new StringBuilder( 1024 );\n" +
                        $"\t\t\t_sb.Append( \"{_fileName}\" );\n" +
                        "\t\t\t_sb.Append( \"{\\n\" );\n" +
                        "\t\t\t// ここで各要素を表示するためのカスタムを施す\n" +
                        "\t\t\t_sb.Append( \"}\" );\n" +
                        "\t\t\treturn _sb.ToString();\n" +
                        "\t\t}\n" +
						"\n\t}\n}" );
			return _sb.ToString();
		}


		//------------------------------------------------------------------------------
		// Dataクラスの内部パラメータを生成し取得.
		string GetClassParamStr(
				ref VarContainer[] _list )
        {
			StringBuilder _sb = new StringBuilder();
			_sb.Capacity = 1024;
			for ( int i = 0; i < _list.Length; ++i )
            {
				if ( _list[ i ].Memo != string.Empty )
					_sb.Append( "// " + list[ i ].Memo + "\n" );
				_sb.Append( "\t\t[SerializeField] private " + this.GetTypeStr( list[i].ID ) + " " + this.GetVariableStr( list[i].Name ) + "\n" );
				_sb.Append( "\t\tpublic " + this.GetTypeStr( list[i].ID ) + " " + this.GetPropertyStr( list[i].Name ) + "\n\n" );
			}
			return _sb.ToString();
		}


		//------------------------------------------------------------------------------
		// 型を文字列で取得.
		string GetTypeStr(
				VarTypeID _id )
        {
			return _id.ToString().Replace( "_", "" );
		}


		//------------------------------------------------------------------------------
		// 変数の形式はLCCで記述.
		string GetVariableStr(
				string _varName )
        {
            string _ret = string.Empty;
            for ( var i = 0; i < _varName.Length; ++i )
                _ret += ( i == 0 ) ? _varName[i].ToString().ToLower() : _varName[i].ToString();
            return _ret + ";";
		}


		//------------------------------------------------------------------------------
		// プロパティの形式はUCCで記述.
		string GetPropertyStr(
				string _varName )
        {
			return char.ToUpper( _varName[0] ) + _varName.Substring(1) + " { get { return this." + this.GetVariableStr( _varName ) + " } }";
		}


	}

}



