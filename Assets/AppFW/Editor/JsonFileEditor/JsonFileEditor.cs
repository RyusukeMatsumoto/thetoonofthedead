﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Events;
using UnityEditor;

namespace AppFW.Editor.JsonFileEditor
{
    //==============================================================================
    //------------------------------------------------------------------------------
    // Jsonファイルエディタ.
    public partial class JsonFileEditor : EditorWindow
    {

        // 対応する型ID( 今後増えるかも ).
		// 下のstring配列にも対応させる.
		public enum VarTypeID : int
        {
			Non = -1,
			_bool = 0,
			_int,
			_long,
			_float,
			_double,
			_Vector2,
			_Vector3,
			_Vector4,
			_string,
		}
		readonly string[] TypeList = {
		    "bool" ,
		    "int",
		    "long",
		    "float",
		    "double",
		    "Vector2",
		    "Vector3",
		    "Vector4",
		    "string"
		};

        #region --- TABOO_NAME ---------------------------------------------------------
        // 此のリストにある文字列単体では利用できない変数名( 基本的にはC#の予約語 ).
        // 今のところ予約語以外は入れていない,長いのでregionした.
        readonly string[] TabooName = {
		    "abstract",
		    "as",
		    "async",
		    "await",
		    "base",
		    "bool",
		    "break",
		    "byte",
		    "case",
		    "catch",
		    "char",
		    "checked",
		    "class",
		    "const",
		    "continue",
		    "decimal",
		    "default",
		    "delegate",
		    "do",
		    "double",
		    "else",
		    "enum",
		    "event",
		    "explicit",
		    "extern",
		    "false",
		    "finally",
		    "fixed",
		    "float",
		    "for",
		    "foreach",
		    "goto",
		    "if",
		    "implicit",
		    "in",
		    "int",
		    "interface",
		    "internal",
		    "is",
		    "lock",
		    "long",
		    "namespace",
		    "new",
		    "null",
		    "object",
		    "operator",
		    "out",
		    "override",
		    "params",
		    "private",
		    "protected",
		    "public",
		    "readonly",
		    "ref",
		    "return",
		    "sbyte",
		    "sealed",
		    "short",
		    "sizeof",
		    "stackalloc",
		    "static",
		    "string",
		    "struct",
		    "switch",
		    "this",
		    "throw",
		    "true",
		    "try",
		    "typeof",
		    "uint",
		    "ulong",
		    "unchecked",
		    "unsafe",
		    "ushort",
		    "using",
		    "virtual",
		    "volatile",
		    "void",
		    "while",
		    "add",
		    "dynamic",
		    "get",
		    "partial",
		    "remove",
		    "set",
		    "value",
		    "var",
		    "where",
		    "yield",
		    "when",
		};
        #endregion --- TABOO_NAME ------------------------------------------------------


        #region --- VarContainer -------------------------------------------------------
        //==============================================================================
        //------------------------------------------------------------------------------
        // 生成する型定義.
        public class VarContainer
        {
			public string Name { get; set; }
			public VarTypeID ID { get; set; }
			public object VarValue { get; set; }
			public string Memo { get; set; }

			// エディタ上で利用する,選択されているか否か.
			// 此のパラメータは.cs .jsonいずれにも影響しない.
			private bool isSelected = false;
			public bool IsSelected { get { return isSelected; } set { isSelected = value; } }


			// コンストラクタ.
			public VarContainer(
					string _name,
					VarTypeID _id,
					string _memo )
            {
				Name = _name;
				ID = _id;
				Memo = _memo;

				switch ( ID ) {
				case VarTypeID._bool: VarValue = new bool(); break;
				case VarTypeID._int: VarValue = new int(); break;
				case VarTypeID._long: VarValue = new long(); break;
				case VarTypeID._float: VarValue = new float(); break;
				case VarTypeID._double: VarValue = new double(); break;
				case VarTypeID._Vector2: VarValue = new Vector2(); break;
				case VarTypeID._Vector3: VarValue = new Vector3(); break;
				case VarTypeID._Vector4: VarValue = new Vector4(); break;
				case VarTypeID._string: VarValue = string.Empty; break;
				}
			}

		}
        #endregion --- VarContainer ----------------------------------------------------


        #region --- DragAndDropJsonObj -------------------------------------------------
        //==============================================================================
        //------------------------------------------------------------------------------
        // D&DされたJsonファイルのデータ.
        [System.Serializable]
		class DragAndDropJsonObj
        {

			// 保持されるデータは以下の通り.
			// [ 0 ] : 型
			// [ 1 ] : 変数名
			// [ 2 ] : 値
			// [ 3 ] : メモ
			[SerializeField] string[] valInfo = null;
			public string[] ValInfo { get { return valInfo; } set { valInfo = value; } }

			// jsonファイル名.
			public string FileName { get; set; }
		}
        #endregion --- DragAndDropJsonObj ----------------------------------------------



        readonly string CsFileExt = ".cs"; // cs.ファイル拡張子.
        readonly string JsonFileExt = ".json"; // jsonファイル拡張子.


		string fileName = "any";	// 出力cs及びjsonファイル名.
		string outputPath = string.Empty;
		List<VarContainer> list = new List<VarContainer>();
		DragAndDropJsonObj ddJson = null;
		Vector2 elemScrollPos = Vector2.zero;


		// Addで追加する要素の値.
		string nextElemName = string.Empty;
		VarTypeID nextElemId = VarTypeID.Non;
		string nextElemMemo = string.Empty;
		int insertIndex = 0;



        //------------------------------------------------------------------------------
        // ウィンドウ表示.
        [MenuItem( "AppFWTools/JsonFileEditor")]
        public static void ShowWindow()
        {
            EditorWindow _window = EditorWindow.GetWindow( typeof( JsonFileEditor ) );
            _window.minSize = new Vector2( 650f, 500f );
            _window.position = new Rect( 200f, 200f, _window.minSize.x, _window.minSize.y );
            _window.titleContent = new GUIContent( "JsonFileEditor" );
        }


        //------------------------------------------------------------------------------
        void OnGUI()
        {

            if ( GUILayout.Button( "test" ) )
            {
                Debug.Log( $"{Application.productName}" );
            }


            fileName = EditorGUILayout.TextField( "FileName", fileName );
            outputPath = EditorGUILayout.TextField( "OutputPath", outputPath );
            EditorGUILayout.BeginVertical( GUILayout.Height( 50f ) );
            EditorGUILayout.LabelField( "output .cs path", this.GetOutputPath( CsFileExt ) );
            EditorGUILayout.LabelField( "output .json path", this.GetOutputPath( JsonFileExt ) );
            EditorGUILayout.EndVertical();

            this.DrawOutputButton();
            this.DrawAddMenu();
            if ( 0 < list.Count )
            {
				elemScrollPos = EditorGUILayout.BeginScrollView( elemScrollPos, GUI.skin.box );
				for ( int i = 0; i < list.Count; ++i )
                {
					this.DrawContainer(i);
				}

				EditorGUILayout.Space();
				EditorGUILayout.Space();
				EditorGUILayout.Space();
				if ( 0 < list.Count )
                {
					if ( GUILayout.Button("AllClear") )
						list.Clear();
				}
				EditorGUILayout.EndScrollView();
            }
        }


        //------------------------------------------------------------------------------
        void OnDestroy()
        {
            fileName = string.Empty;
            outputPath = string.Empty;
            list.Clear();
            ddJson = null;
            nextElemName = string.Empty;
            nextElemId = VarTypeID.Non;
            nextElemMemo = string.Empty;
            insertIndex = 0;      
        }


        //------------------------------------------------------------------------------
        // 指定した拡張子の出力ファイルパスを取得.
        string GetOutputPath( string _ext )=> outputPath + "/" + fileName + _ext;


        //------------------------------------------------------------------------------
		// 指定したパラメータ名が使用可能か否かチェック.
		bool CheckUsableName(
				string _name )
        {
			bool _ret = !list.Exists( e => e.Name == _name );

			List<string> _typeList = new List<string>();
			_typeList.AddRange(TypeList);
			_typeList.AddRange(TabooName);
			_ret = ( !_ret ) ? _ret : !_typeList.Exists( e => e == name );
			_typeList.Clear();

			return _ret;
		}


        //------------------------------------------------------------------------------
        // 出力ボタン描画.
        void DrawOutputButton()
        {
            if ( Directory.Exists( outputPath ) )
            {
                if ( GUILayout.Button( "output" ) )
                {
                    File.WriteAllText( this.GetOutputPath(CsFileExt), this.CreateScriptText( fileName, list.ToArray() ), Encoding.UTF8 );
					AssetDatabase.SaveAssets();
					AssetDatabase.Refresh(ImportAssetOptions.ImportRecursive);

					File.WriteAllText( this.GetOutputPath(JsonFileExt), this.CreateJsonText( fileName, list.ToArray() ), Encoding.UTF8 );
					AssetDatabase.SaveAssets();
					AssetDatabase.Refresh(ImportAssetOptions.ImportRecursive);
                }
            }
        }


        //------------------------------------------------------------------------------
        // 追加要素編集領域描画.
        void DrawAddMenu()
        {
			EditorGUILayout.BeginHorizontal( GUI.skin.box, GUILayout.Width(600f), GUILayout.Height(100f) );

            #region --- LeftElem -------------------------------------------------------
            EditorGUILayout.BeginVertical( GUI.skin.box, GUILayout.Width(300f) );

			EditorGUILayout.LabelField("Add Element");
			nextElemName = EditorGUILayout.TextField( "ElemName", nextElemName );
			nextElemId = (VarTypeID)EditorGUILayout.EnumPopup( "VariantType", nextElemId );
			nextElemMemo = EditorGUILayout.TextField( "Memo", nextElemMemo );
			EditorGUILayout.Space();
			EditorGUILayout.BeginHorizontal();
			if (	nextElemName != string.Empty &&
					this.CheckUsableName( nextElemName ) &&
					nextElemId != VarTypeID.Non )
            {
				if ( GUILayout.Button( "Add", GUILayout.Width(50f) ) )
                {
					list.Add( new VarContainer( nextElemName, nextElemId, nextElemMemo ) );
					nextElemName = string.Empty;
					nextElemId = VarTypeID.Non;
					nextElemMemo = string.Empty;
				}

				if ( 0 < list.Count )
                {
					EditorGUILayout.BeginHorizontal(GUI.skin.box);
					insertIndex = EditorGUILayout.IntField( insertIndex, GUILayout.Width(100f) );
					if ( GUILayout.Button( "Insert", GUILayout.Width(100f) ) )
                    {
						list.Insert( insertIndex, new VarContainer( nextElemName, nextElemId, nextElemMemo ) );
						nextElemName = string.Empty;
						nextElemId = VarTypeID.Non;
						nextElemMemo = string.Empty;
						insertIndex = 0;
					}
					EditorGUILayout.EndHorizontal();
				}
			}
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.EndVertical();
            #endregion --- LeftElem ----------------------------------------------------

            #region --- RightElem ------------------------------------------------------
            EditorGUILayout.BeginVertical( GUI.skin.box, GUILayout.Width(300f) );

			EditorGUILayout.LabelField("Load Json");
			Event _evt = Event.current;
			Rect _dropArea = GUILayoutUtility.GetRect( 0f, 50f, GUILayout.ExpandWidth(true) );
			switch ( _evt.type )
            {
			case EventType.DragUpdated:
			case EventType.DragPerform:
				if ( !_dropArea.Contains(_evt.mousePosition) ) break;
				if ( 1 < DragAndDrop.objectReferences.Length ||
					!DragAndDrop.paths[0].Contains(JsonFileExt) )
                {
					DragAndDrop.visualMode = DragAndDropVisualMode.Rejected;
					break;
				}

				DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
				if ( _evt.type == EventType.DragPerform )
                {
					DragAndDrop.AcceptDrag();
					ddJson = JsonUtility.FromJson( DragAndDrop.objectReferences[0].ToString(), typeof(DragAndDropJsonObj) ) as  DragAndDropJsonObj;
					ddJson.FileName = DragAndDrop.objectReferences[0].name;
				}
				break;
			}
			string _boxStr = ( ddJson != null ) ? "\n\n[ " + ddJson.FileName + JsonFileExt + " ] is loaded..." : "";
			GUI.Box( _dropArea, "Drag & Drop Any Json File" + _boxStr );

			if ( GUILayout.Button("Add JsonParam") && ddJson != null )
            {
				VarContainer[] _c = this.AnalysisDDJsonObj(ddJson);
				if ( _c != null )
                {
					for ( int i = 0; i < _c.Length; ++i )
                    {
						list.Add( _c[i] );
					}
					ddJson = null;
				}
			}		

			EditorGUILayout.EndVertical();
            #endregion --- RightElem ---------------------------------------------------

			EditorGUILayout.EndHorizontal();
        }


        //------------------------------------------------------------------------------
		// コンテナの内容を描画.
		void DrawContainer(
				int _index )
        {
            if ( list[_index].IsSelected ) EditorGUILayout.BeginHorizontal( GUI.skin.box );
            else                           EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField( _index.ToString(), GUILayout.Width(20f) );
			list[_index].IsSelected = EditorGUILayout.Toggle( "", list[_index].IsSelected, GUILayout.Width(30f) );

			GUILayoutOption _layout = GUILayout.ExpandWidth(false);
			EditorGUILayout.LabelField( TypeList[ (int)list[_index].ID ] + " " + list[_index].Name, _layout );
			switch ( list[_index].ID )
            {
			case VarTypeID._bool:	list[_index].VarValue = EditorGUILayout.Toggle( "", (bool)list[_index].VarValue, _layout ); break;
			case VarTypeID._int:	list[_index].VarValue = EditorGUILayout.IntField( "", (int)list[_index].VarValue, _layout ); break;
			case VarTypeID._long:	list[_index].VarValue = EditorGUILayout.LongField( "", (long)list[_index].VarValue, _layout ); break;
			case VarTypeID._float:	list[_index].VarValue = EditorGUILayout.FloatField( "", (float)list[_index].VarValue, _layout ); break;
			case VarTypeID._double:	list[_index].VarValue = EditorGUILayout.DoubleField( "", (double)list[_index].VarValue, _layout ); break;
			case VarTypeID._Vector2:list[_index].VarValue = EditorGUILayout.Vector2Field( "", (Vector2)list[_index].VarValue, _layout ); break;
			case VarTypeID._Vector3:list[_index].VarValue = EditorGUILayout.Vector3Field( "", (Vector3)list[_index].VarValue, _layout );  break;
			case VarTypeID._Vector4:list[_index].VarValue = EditorGUILayout.Vector4Field( "", (Vector4)list[_index].VarValue, _layout ); break;
			case VarTypeID._string:	list[_index].VarValue = EditorGUILayout.TextField( "", (string)list[_index].VarValue ); break;
			}
			list[_index].Memo = EditorGUILayout.TextArea( list[_index].Memo, _layout );
			EditorGUILayout.EndHorizontal();

			// 選択されている場合の何かしらの処理.
			if ( list[_index].IsSelected )
            {
				EditorGUILayout.BeginHorizontal();

				if ( GUILayout.Button( "Delete", GUILayout.Width(70f) ) )
					list.RemoveAt(_index);

				if ( GUILayout.Button( "MoveUp", GUILayout.Width(70f) ) && 0 < _index )
                {
					VarContainer temp = list[_index];
					list.RemoveAt(_index);
					list.Insert( _index - 1, temp );
				}

				if (  GUILayout.Button( "MoveDown", GUILayout.Width(100f) ) && _index < list.Count - 1 )
                {
					VarContainer temp = list[_index];
					list.RemoveAt(_index);
					list.Insert( _index + 1, temp );
				}
				EditorGUILayout.EndHorizontal();
			}
		}


        //------------------------------------------------------------------------------
		// Drag & Dropで渡されたオブジェクトからVarContainerの配列を取得.
		VarContainer[] AnalysisDDJsonObj(
				DragAndDropJsonObj _obj )
        {
			if ( _obj == null || _obj.ValInfo == null ) return null;

			Func< string, int, float > _f = ( string _s, int _num ) => {
				_s = _s.Trim( '(', ')', ' ' );
				string[] _tempStr = _s.Split(',');
				return ( _num < _tempStr.Length ) ? float.Parse( _tempStr[_num] ) : 0f;
			};

			const int _typeIdx = 0;
			const int _nameIdx = 1;
			const int _valIdx = 2;
			const int _memoIdx = 3;
			List<VarContainer> _ret = new List<VarContainer>();
			for ( int i = 0; i < _obj.ValInfo.Length; ++i )
            {
				string[] _str = _obj.ValInfo[i].Split(':');

				VarContainer container = null;
				switch ( _str[_typeIdx] )
                {
				case "bool":
					container =  new VarContainer( _str[_nameIdx], VarTypeID._bool, ( _str.Length < _memoIdx ) ? "" : _str[_memoIdx] );
					container.VarValue = ( _str[_valIdx] == "True" ) ? true : false;					
					break;

				case "int":
					container = new VarContainer( _str[_nameIdx], VarTypeID._int, ( _str.Length < _memoIdx ) ? "" : _str[_memoIdx] );
					container.VarValue = int.Parse( _str[_valIdx] );
					break;

				case "long":
					container = new VarContainer( _str[_nameIdx], VarTypeID._long, ( _str.Length < _memoIdx ) ? "" : _str[_memoIdx] );
					container.VarValue = long.Parse( _str[_valIdx] );
					break;

				case "float":
					container = new VarContainer( _str[_nameIdx], VarTypeID._float, ( _str.Length < _memoIdx ) ? "" : _str[_memoIdx] );
					container.VarValue = float.Parse( _str[_valIdx] );
					break;

				case "double":
					container = new VarContainer( _str[_nameIdx], VarTypeID._double, ( _str.Length < _memoIdx ) ? "" : _str[_memoIdx] );
					container.VarValue = double.Parse( _str[_valIdx] );
					break;

				case "Vector2":
					container = new VarContainer( _str[_nameIdx], VarTypeID._Vector2, ( _str.Length < _memoIdx ) ? "" : _str[_memoIdx] );
					container.VarValue = new Vector2( _f( _str[_valIdx], 0 ), _f( _str[_valIdx], 1 ) );
					break;

				case "Vector3":
					container = new VarContainer( _str[_nameIdx], VarTypeID._Vector3, ( _str.Length < _memoIdx ) ? "" : _str[_memoIdx] );
					container.VarValue = new Vector3( _f( _str[_valIdx], 0 ), _f( _str[_valIdx], 1 ), _f( _str[_valIdx], 2 ) );
					break;

				case "Vector4":
					container = new VarContainer( _str[_nameIdx], VarTypeID._Vector4, ( _str.Length < _memoIdx ) ? "" : _str[_memoIdx] );
					container.VarValue = new Vector4( _f( _str[_valIdx], 0 ), _f( _str[_valIdx], 1 ), _f( _str[_valIdx], 2 ), _f( _str[_valIdx], 3 ) );
					break;

				default: break;
				}
				_ret.Add(container);
			}
			return _ret.ToArray();
		}



    }
}

