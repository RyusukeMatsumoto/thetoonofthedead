﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AppFW.Editor.JsonFileEditor
{
	//==============================================================================
	//------------------------------------------------------------------------------
	// JsonFileEditor,Jsonファイル出力部.
	public partial class JsonFileEditor : EditorWindow
    {

		//------------------------------------------------------------------------------
		// Jsonテキストを出力.
		string CreateJsonText(
				string _fileName,
				VarContainer[] _list )
        {
			StringBuilder _sb = new StringBuilder("{\n");
			StringBuilder _valInfo = new StringBuilder(",\n\n\n\"valInfo\" : [ \n");
			for ( int i = 0; i < _list.Length; ++i )
            {
				_sb.Append( this.MoldingValue( list[i] ) );
				_valInfo.Append( this.GetJsonValInfo( list[i] ) );
				if ( i < _list.Length - 1 )
                {
					_sb.Append(",\n");
					_valInfo.Append(",\n");
				}
			}
			_valInfo.Append("\n\t]\n");
			_sb.Append( _valInfo.ToString() + "\n\n}\n" );
			return _sb.ToString();
		}


		//------------------------------------------------------------------------------
		// jsonファイルの変数情報取得,最後尾データに組み込むのに使用する.
		string GetJsonValInfo(
				VarContainer _elem )
        {
			return "\t\t\"" + _elem.ID.ToString().Remove( 0, 1 ) + ":" + _elem.Name + ":" + _elem.VarValue.ToString() + ":" + _elem.Memo + "\"";
		}


		//------------------------------------------------------------------------------
		// コンテナ内部の要素をJsonに対応する形に成形.
		string MoldingValue(
				VarContainer _vc )
        {
			string _dq = "\"";
			string _sep = " : ";
			string _tab = "\t";
			string _ret = string.Empty;
			switch ( _vc.ID )
            {
			case VarTypeID._bool:
			case VarTypeID._string:
				_ret += _tab + _dq + _vc.Name + _dq + _sep + _dq + _vc.VarValue.ToString() + _dq;
				break;

			case VarTypeID._int:
			case VarTypeID._long:
			case VarTypeID._float:
			case VarTypeID._double:
				 _ret += _tab + _dq + _vc.Name + _dq + _sep + _vc.VarValue.ToString();
				break;

			case VarTypeID._Vector2:
				Vector2 _v2 = ( Vector2 )_vc.VarValue;
				_ret += _tab + _dq + _vc.Name + _dq + _sep + "{ \n" + 
					_tab + _tab + _dq + "x" + _dq + _sep + _v2.x.ToString() + ",\n" +
					_tab + _tab + _dq + "y" + _dq + _sep + _v2.y.ToString() + "\n" + _tab + "}";
				break;

			case VarTypeID._Vector3:
				Vector3 v3 = ( Vector3 )_vc.VarValue;
				_ret += _tab + _dq + _vc.Name + _dq + _sep + "{ \n" + 
					_tab + _tab + _dq + "x" + _dq + _sep + v3.x.ToString() + ",\n" +
					_tab + _tab + _dq + "y" + _dq + _sep + v3.y.ToString() + ",\n" +
					_tab + _tab + _dq + "z" + _dq + _sep + v3.z.ToString() + "\n" + _tab + "}";
			break;

			case VarTypeID._Vector4:
				Vector4 v4 = ( Vector4 )_vc.VarValue;
				_ret += _tab + _dq + _vc.Name + _dq + _sep + "{ \n" + 
					_tab + _tab + _dq + "x" + _dq + _sep + v4.x.ToString() + ",\n" +
					_tab + _tab + _dq + "y" + _dq + _sep + v4.y.ToString() + ",\n" +
					_tab + _tab + _dq + "z" + _dq + _sep + v4.z.ToString() + ",\n" +
					_tab + _tab + _dq + "w" + _dq + _sep + v4.w.ToString() + "\n" + _tab + "}";					
				break;

			}
			return _ret;
		}


	}
}